﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace TestFramework.General
{
    public delegate IWebElement Condition(IWebElement element);
    internal class GF
    {
        protected static IWebDriver Driver { get => Drivers.dr; }

        public static IWebElement WaitElement(IWebDriver driver, By selector, int timeSeconds, Condition cnd)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeSeconds));
            return wait.Until((x) =>
            {
                IWebElement element = driver.FindElement(selector);
                return cnd(element);
            });
        }

        public static IWebElement ExistDisplayedEnabled(IWebElement element)
        {
            return (element != null && element.Displayed && element.Enabled) ? element : null;
        }

        public static IWebElement ExistDisplayed(IWebElement element)
        {
            return (element != null && element.Displayed) ? element : null;
        }

        public static IWebElement Exist(IWebElement element)
        {
            return (element != null) ? element : null;
        }

        public static Func<IWebDriver, IWebElement> ElementIsClickable(By locator)
        {
            return driver =>
            {
                var element = driver.FindElement(locator);
                return (element != null && element.Displayed && element.Enabled) ? element : null;
            };
        }

        public static IWebElement WaitElementIsExist(IWebDriver driver, By selector, int timeSeconds)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeSeconds));
            return wait.Until((x) =>
            {
                IWebElement element = driver.FindElement(selector);
                return element;
            });
        }

        public static void ClickOn(IWebElement element, string description = "",int timeToWait=8)
        {
            WaitForClickabilityOfElement(element, timeToWait);
            element.Click();
        }

        public static void JavaScriptClick(IWebElement element, string description = "")
        {
            IJavaScriptExecutor executor = (IJavaScriptExecutor)Driver;
            executor.ExecuteScript("arguments[0].click();", element);
        }

        public static void CheckAction(bool check, string description = "", int timeToWait = 10)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeToWait));

            try
            {
                wait.Until((d) =>
                {
                    return check == true;
                });
            }
            catch (StaleElementReferenceException)
            { }
        }

        public static void WaitForClickabilityOfElement(IWebElement element, long timeToWait = 10)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeToWait));
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
        }


        public static void SubmitBy(IWebElement element, string description = "")
        {
            element.Submit();
        }

        public static String GetTextOfElement(IWebElement element, string description = "")
        {
            return element.Text;
        }

        public static void SendKeys(IWebElement element, String keyword = "", string description = "")
        {
            element.Click();
            element.Clear();
            element.SendKeys(keyword);
        }

        public static void SendKeysImitation(IWebElement element, String keyword, string description = "")
        {
            char[] a = keyword.ToCharArray();
            element.Clear();
            foreach (var el in a)
            {
                element.SendKeys(el.ToString());
                Thread.Sleep(100);
            }
        }

        public static void SendKeysClickEnter(IWebElement element, String keyword, string description)
        {
            element.Click();
            element.SendKeys(keyword);
            element.SendKeys(Keys.Enter);
        }

        public static void SendKeysEnter(IWebElement element, String keyword, string description)
        {

            element.Clear();
            element.SendKeys(keyword);
            element.SendKeys(Keys.Enter);
        }

        public static void NavigateByJS(IWebElement element)
        {
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
        }

        public static void SwitchTo()
        {
            Driver.SwitchTo().DefaultContent();
        }

        public static void MoveToElementAndClick(IWebElement element, string description = "", int horizontal = 0, int vertical = 0)
        {
            Actions actions = new Actions(Driver);
            actions.MoveToElement(element, horizontal, vertical).Build().Perform();
            actions.Click().Build().Perform();
        }

        public static void MoveToElement(IWebElement element, string description = "")
        {
            Actions actions = new Actions(Driver);
            actions.MoveToElement(element).Build().Perform();
        }

        public static bool CheckElementNotVisible(IWebElement element)
        {

            for (int i = 0; i < 10; i++)
            {
                if (!element.Displayed) { return true; }
                Thread.Sleep(500);
            }
            return false;
        }

        public static void Back(string description = "")
        {
            Driver.Navigate().Back();
        }

        public static bool CheckElementNotExist(List<IWebElement> elements)
        {
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    if (!elements.Any()) { return true; }
                    Thread.Sleep(500);
                }
                return false;
            }
            catch (ArgumentNullException)
            {
                return true;
            }
        }


        public static void WaitForPageLoadComplete(double timeToWait = 10)
        {
            IWait<IWebDriver> wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeToWait));
            wait.Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
        }

        public static void WaitForVisibility(By locator, string descr = "", double timeToWait = 10)
        {
            IWait<IWebDriver> wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeToWait));
            wait.Until(ExpectedConditions.ElementIsVisible(locator));
        }

        public static bool CheckCorrectText(By el, string str, int iter = 10)
        {
            for (int i = 0; i < 20; i++)
            {
                IWebElement element = Driver.FindElement(el);
                if (element.Text == str) { return true; }
                Thread.Sleep(300);
            }
            return false;
        }

        public static bool CheckDisplayed(By el, string description = "")
        {
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    IWebElement element = Driver.FindElement(el);

                    if (element.Displayed) { return true; }
                    Thread.Sleep(500);
                }
                catch (NoSuchElementException)
                {
                    return false;
                }
            }
            return false;
        }

        public static bool CheckNotDisplayed(By el, string description = "")
        {
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    IWebElement element = Driver.FindElement(el);

                    if (element.Displayed) { return false; }
                    Thread.Sleep(500);
                }
                catch (NoSuchElementException)
                {
                    return true;
                }
            }
            return false;
        }

        public static IWebElement WaitUntilPopupChange(IWebDriver driver, By selector, int timeSeconds, Condition cnd)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeSeconds));
            return wait.Until((x) =>
            {
                IWebElement element = driver.FindElement(selector);
                return cnd(element);
            });
        }

        public static void NavigateToUrl(string Url, string description = "")
        {
            Driver.Navigate().GoToUrl(URLBuilder.BaseUrl + Url);
        }

        /// <summary>
        /// Почта { Отделение - [0], К Дому - [1] }
        /// Каталог {числа выбора - больший размер/количество}
        /// </summary>
        /// <param name="dropDown"></param>
        /// <param name="numberToClick"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static void DropDownClick(By dropDown, int numberToSelect, string description = "")
        {
            SelectElement oSelect = new SelectElement(Driver.FindElement(dropDown));
            oSelect.SelectByIndex(numberToSelect);
            //PageBase.WaitPageFullLoaded();
        }

/*        /// <summary>
        /// Сортировка { Бестселлер - [0], Новинки - [1], Возрастание - [2] Убывание - [3] }
        /// </summary>
        /// <param name="filterToOpen"></param>
        /// <param name="listOfFilter"></param>
        /// <param name="numberToSelect"></param>
        /// <param name="description"></param>
        public static void DropDownClickNoSelect(IWebElement filterToOpen, List<IWebElement> listOfFilter, int numberToSelect, string description = "")
        {
            filterToOpen.Click();
            MoveToElementAndClick(listOfFilter[numberToSelect]);
            //CheckAction(listOfFilter[numberToSelect].Displayed && listOfFilter[numberToSelect].Enabled);

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until((x) =>
            {
                listOfFilter[numberToSelect].Click();
                return filterToOpen.Text.Contains(listOfFilter[numberToSelect].Text);
            });

        }*/

        public static void CheckElementNotAttachedToDom(IWebElement element, string description = "", int timeSeconds = 7)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeSeconds));
            try
            {
                wait.Until(ExpectedConditions.StalenessOf(element));
            }
            catch (Exception)
            { }
        }

        public static void CheckElementNotAttachedToDom2(List<IWebElement> elements, string description = "", int timeSeconds = 7)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeSeconds));
            try
            {
                foreach (var n in elements)
                {
                    wait.Until(ExpectedConditions.StalenessOf(n));
                }
            }
            catch (Exception)
            { }
        }
    }
}