﻿using OpenQA.Selenium;
using System.Collections.Generic;
using TestFramework.Pages;

namespace TestFramework.General
{
    public class GeneralXPath : PageBase
    {
        //Quick View (NEW)
        public IWebElement QuickViewItemTitle { get => WaitElement(By.XPath("//h1[@class='product-detail__title']")); }
        public List<IWebElement> QuickViewOfItemList { get => MyWaitElements(By.XPath("//button[@class='product-card__quick-view']")); }
        //public IWebElement QuickViewOfFirstItem { get => WaitElement(By.XPath("//button[@class='product-card__quick-view']")); }
        //public IWebElement QuickViewOfSecondItem { get => WaitElement(By.XPath("(//button[@class='product-card__quick-view'])[2]")); }
        public IWebElement QuickViewViewedItem { get => WaitElement(By.XPath("//div[@class='one-news gtm-parent']//div[@class='fast-look one-cat-by']")); }

        

        //Quick view General func
        public IWebElement BuyButtonQuickView { get => WaitElement(By.XPath("//div[@class='def-button']")); }
        public List<IWebElement> ListOfColorsQuickView { get => MyWaitElements(By.XPath("//div[@class='offer-color__colors product-detail__offer-color']/a")); }
        public IWebElement ChangeColourQuickView { get => WaitElement(By.XPath("//div[@class='offer-color__colors product-detail__offer-color']/a[not(contains(@class,'active'))]")); }
        public IWebElement CloseQuickViewButton { get => WaitElement(By.XPath("//button[@class='def-modal__close']//*")); }
        public IWebElement AddToWishListButtonQuickView { get => WaitElement(By.XPath("//div[@class='product-detail__like']")); }
        public IWebElement ItemInFavQuickView { get => WaitElement(By.XPath("//div[@class='product-detail__like']//div[@class='def-like def-like_active']")); }
        public List<IWebElement> SizesQuickView { get => MyWaitElements(By.XPath("//div[@class='def-size']/div")); }
        public IWebElement SizesActiveQuickView { get => WaitElement(By.XPath("//div[@class='def-size__item def-size__item_active']")); }

        public IWebElement QuickViewPopUp{ get => WaitElement(By.XPath("//div[@class='vm--modal']")); }
        

        //перший розмір зі списку розмірів OLD QUICK VIEW
        public IWebElement BuyButtonOLDQuickView { get => WaitElement(By.XPath("//div[@class='product-popup']//div[@id='product_detail_buy']")); }
        

        public IWebElement ItemSizeQuickView { get => WaitElement(By.XPath("//div[@class='product-popup']//div[@class='one-color ']//label")); }
        public IWebElement ItemSizeSecondOfListQuickView { get => WaitElement(By.XPath("(//div[@class='one-color ']//label)[2]")); }
        public IWebElement ItemSizeThirdOfListQuickView { get => WaitElement(By.XPath("(//div[@class='one-color ']//label)[3]")); }
        public IWebElement TextofSizeItemQuickView { get => WaitElement(By.XPath("//span[@id='product_detail_size']")); }


        public IWebElement BuyButton { get => WaitElement(By.XPath("//div[@id='product_detail_buy']")); }
        public IWebElement ChangeColour { get => WaitElement(By.XPath("//div[@class='infos']//div[@class='one-color']/label[@class='']")); }
        public List<IWebElement> ListOfColors { get => MyWaitElements(By.XPath("//div[@class='infos']//div[@class='one-color']/label[@class]")); }

        //перший розмір зі списку розмірів
        public IWebElement ItemSize { get => WaitElement(By.XPath("//div[@class='one-color ']//label")); }
        public IWebElement ItemSizeSecondOfList { get => WaitElement(By.XPath("(//div[@class='one-color ']//label)[2]")); }
        public IWebElement ItemSizeThirdOfList { get => WaitElement(By.XPath("(//div[@class='one-color ']//label)[3]")); }
        public IWebElement TextofSizeItem { get => WaitElement(By.XPath("//span[@id='product_detail_size']")); }

        //Попап образи
        public IWebElement CloseObrazPopupButton { get => WaitElement(By.XPath("//div[contains(@class,'vue-popup__container')]//span[@class='vue-popup__close']")); }//*[@*]
        public IWebElement InfoAboutObraz { get => WaitElement(By.XPath("//div[contains(@class,'vue-popup__container')]//div[@class='vue-popup__content']//div[@class='quick-view__navInfo']")); }
        public List<IWebElement> ListOfPriceItemsObrazPopup { get => MyWaitElements(By.XPath("//a[contains(@class,'quick-view-card')]//span[contains(@class,'images-goods__current')]")); }     
        public IWebElement PreviousObrazButton { get => WaitElement(By.XPath("//div[@class='owl-prev']")); }
        public IWebElement NextObrazButton { get => WaitElement(By.XPath("//div[@class='owl-next']")); }
        public IWebElement TotalSummOfAmountObrazPopup { get => WaitElement(By.XPath("//div[contains(@class,'quick-view__total-amount')]/span[contains(@class,'images-goods__current')]")); }
        public IWebElement TextOfFirstItemInObrazPopup { get => WaitElement(By.XPath("//a//span[contains(@class,'quick-view-card__text')]")); }
        
        public List<IWebElement> ViewItemInObrazPopup { get => MyWaitElements(By.XPath("//div[contains(@class,'quick-view__detailed')]//span[@class='quick-view-card__top']")); }

        public List<IWebElement> PopUpObrazov { get => MyWaitElements(By.XPath("//div[@class='vue-popup__container']")); }


        

        public IWebElement ObrazBuyButton { get => WaitElement(By.XPath("//span[@class='def-button']")); } 

        public IWebElement ObrazSizesBuy { get => WaitElement(By.XPath("//span[@class='quick-view-sizes__size']")); }
        public IWebElement ObrazImg { get => WaitElement(By.XPath("//span[@class='quick-view-card__top']")); }

    }
}
