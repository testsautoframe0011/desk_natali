﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.Catalog
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Каталог"), Property("TestSuite:", "CT")]
    class TestPagination<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Dmytro Lytovchenko"), Description("Переключение на следующую страницу числом")]
        public void CT1_NextPageNumberClick()
        {
            GF.NavigateToUrl(PPattern.PlatyaUrl);
            var elBeforeText = PCatalog.ProductItemsTitlesList[0].Text;
            PCatalog.NextPaginationElement(PCatalog.PaginationElements, PCatalog.ProductItemList, 1);
            GF.CheckAction(PCatalog.ProductItemsTitlesList[0].Displayed);
            Assert.AreNotEqual(elBeforeText, PCatalog.ProductItemsTitlesList[0].Text);

        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Переключение на следующую страницу числом, открытая страница активна(обозначена)")]
        public void CT2_NextPageNumberClick()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            GF.NavigateToUrl(PPattern.SaleUrl);
            PCatalog.NextPaginationElement(PCatalog.PaginationElements, PCatalog.ProductItemList, 1);
            Assert.IsTrue(PCatalog.PaginationElements[1].GetAttribute("class").Contains("active"));
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Кнопка к следующей странице работает")]
        public void CT3_NextButtonClick()
        {
            GF.NavigateToUrl(PPattern.NovinkiUrl);
            var elBeforeText = PCatalog.ProductItemsTitlesList[0].Text;
            PCatalog.NextElementButton(PCatalog.ProductItemList,"Перехожу на старинцу стрелкой");
            Thread.Sleep(2000);
            Assert.AreNotEqual(elBeforeText, PCatalog.ProductItemsTitlesList[0].Text, "К следующей странице работает");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Кнопка к предыдущей странице работает")]
        public void CT4_PrevButtonClick()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            GF.NavigateToUrl(PPattern.SaleUrl);
            PCatalog.NextPaginationElement(PCatalog.PaginationElements, PCatalog.ProductItemList, 20);

            PCatalog.PrevElementButton(PCatalog.ProductItemList);
            Thread.Sleep(1000);
            Assert.IsTrue(PCatalog.PaginationElements[PCatalog.PaginationElements.Count - 2].GetAttribute("class").Contains("active"));
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Посомтреть еще - второй лист")]
        public void CT5_SecondListDisplayedMoreItem()
        {
            GF.NavigateToUrl(PPattern.NovinkiUrl);
            GF.ClickOn(PCatalog.LookMorepaginationButton,"Нажимаю на Показать еще");
            Thread.Sleep(3000);
            
            Assert.AreNotEqual(PCatalog.ProductItemList[0].Text, PCatalog.ProductItemList[20].Text, "Следующие товары отображены");

        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Посмотреть еще не отображается на посл странице")]
        public void CT6_ShowMorenotDisplaedlastPage()
        {
            GF.NavigateToUrl(PPattern.SaleUrl);
            PCatalog.NextPaginationElement(PCatalog.PaginationElements, PCatalog.ProductItemList, 20);
            Assert.IsTrue(!GF.CheckDisplayed(PCatalog.LookMorepaginationButtonSelector),"Кнопка Посмотреть еще не отображается");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Посмотреть еще на второй странице")]
        public void CT6_NumberInputActive()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            GF.ClickOn(PPattern.BazovyyOfisButton);
            PCatalog.NextElementButton(PCatalog.ProductItemList, "Перехожу на старинцу стрелкой");
            var elBefore = PCatalog.ProductItemsTitlesList.Count;
            GF.ClickOn(PCatalog.LookMorepaginationButton, "Нажимаю на Показать еще");
            GF.CheckAction(PCatalog.PaginationElements[1].GetAttribute("class").Contains("active"));//Thread.Sleep(2000);
            var elAfter = PCatalog.ProductItemsTitlesList.Count;
            Assert.AreNotEqual(elBefore, elAfter, "Следующие товары отображены");
        }
    }
}
