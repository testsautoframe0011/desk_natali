﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework.General;

namespace TestFramework.Tests.Catalog
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Каталог"), Property("TestSuite:", "CT")]
    class TestCatalog<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Vanya Demenko"), Description("Переход на детальную по цвету")]
        public void CT1_ChangeColourOfItemInCatalogPage()
        {
            GF.ClickOn(PPattern.SaleButton, "натискаю на Sale");
            GF.MoveToElement(PCatalog.ProductItemFewColors[0], "рухаюся до товару");
            var title = GF.GetTextOfElement(PCatalog.ProductItemsTitlesList[0]);
            GF.ClickOn(PCatalog.ChangeColourButtonCatalog, "Перехожу на товар другого цвета");

            GF.CheckElementNotAttachedToDom2(PCatalog.ProductItemList);

            Assert.AreNotEqual(PItem.NameOfItemInPageItem.Text, title, "колір успішно змінений");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Отображение размеров при hover эффекте")]
        public void CT2_DisplaySizesForHoverEffect()
        {
            GF.MoveToElement(PPattern.SaleButton, "прямую до Sale");
            GF.ClickOn(PPattern.TrikotazhSaleButton, "натискаю на Трикотаж");
            GF.MoveToElement(PCatalog.ProductItemList[0], "навівся на товар");
            Assert.IsTrue(PCatalog.SizesHoverEffectList[0].Displayed, "Розміри висвітлено");
        }

        [Test]
        [Author("Vanya Demenko, Dmytro Lytovchenko"), Description("Проверка корректности отображения размеров при hover эффекте")]
        public void CT3_CorrectnessOfDisplayingSizesByHover()
        {
            GF.ClickOn(PPattern.NovinkiButton, "Перехожу в Новинки");
            GF.MoveToElement(PCatalog.ProductItemList[0], "навівся на товар");
            GF.CheckAction(PCatalog.HoverSize.Displayed,"Жду отображения размеров при хувере");
            List<string> strHoverList = PItem.MakeNewList(PCatalog.SizesHoverEffectList);//записую розміри в лист, при hover
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.CheckAction(GXP.ItemSize.Displayed, "");
            List<string> strList = PItem.MakeNewList(PItem.SizesPageItemList);//записую розміри в лист
            Assert.IsTrue(PItem.ComparingTwoLists(strHoverList, strList), "Розміри висвітлено");
        }
    }
}
