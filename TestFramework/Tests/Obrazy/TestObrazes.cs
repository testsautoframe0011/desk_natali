﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.General;

namespace TestFramework.Tests.Obrazy
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Образы"), Property("TestSuite:", "OB")]
    class TestObrazes<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Vanya Demenko"), Description("Переход на страницу товара через попап образа")]
        public void OB1_GoToItemPageFromObrazPopup()
        {
            GF.MoveToElement(PPattern.NashyTrendyButton, "прямую до Наши тренды");
            GF.ClickOn(PPattern.OfficeСollection, "нажимаю на Весна 21");
            GF.CheckAction(POficeCollect.ViewFirstObrazVesna21Button.Displayed, "жду загрузки трендов",30);
            GF.MoveToElement(POficeCollect.ViewFirstObrazVesna21Button,"Навождусь на баннер");
            GF.ClickOn(POficeCollect.ViewFirstObrazVesna21Button, "відкриваю попап з образом");
            GF.CheckAction(GXP.TextOfFirstItemInObrazPopup.Displayed, "жду загрузки товара коллекций", 30);
            var title = GF.GetTextOfElement(GXP.TextOfFirstItemInObrazPopup);
            GF.ClickOn(GXP.ViewItemInObrazPopup[0], "натискаю на товар з образу");
            Assert.AreEqual(GF.GetTextOfElement(PItem.NameOfItemInPageItem).Trim(), title.Trim(), "Успішно перейшли на сторінку товара");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Открыть следующий образ в попапе")]
        public void OB2_OpenNextObrazInPopupRightArrow()
        {
            GF.MoveToElement(PPattern.NashyTrendyButton, "прямую до Наши тренды");
            GF.ClickOn(PPattern.OfficeСollection, "нажимаю на Весна 21");
            GF.CheckAction(POficeCollect.ViewFirstObrazVesna21Button.Displayed, "жду загрузки трендов");
            GF.ClickOn(POficeCollect.ViewFirstObrazVesna21Button, "відкриваю перший образ");
            GF.CheckAction(GXP.TextOfFirstItemInObrazPopup.Displayed, "жду загрузки товара коллекций", 30);
            var info = GF.GetTextOfElement(GXP.TextOfFirstItemInObrazPopup);
            GF.ClickOn(GXP.NextObrazButton, "переходжу до наступного образу");
            Assert.AreNotEqual(GF.GetTextOfElement(GXP.TextOfFirstItemInObrazPopup).Trim(), info.Trim(), "Попап успішно закритий");
        }


        [Test]
        [Author("Vanya Demenko, Dmytro Lytovchenko"), Description("Открыть предыдущий образ в попапе")]
        public void OB3_OpenPreviousObrazInPopup()
        {
            GF.MoveToElement(PPattern.NashyTrendyButton, "прямую до Наши тренды");
            GF.ClickOn(PPattern.OfficeСollection, "нажимаю на Весна 21");
            GF.CheckAction(POficeCollect.ViewMiddleObrazVesna21Button.Displayed, "жду загрузки трендов");
            GF.MoveToElement(POficeCollect.ViewMiddleObrazVesna21Button, "двигаюсь к попАпу образа");
            GF.ClickOn(POficeCollect.ViewMiddleObrazVesna21Button, "відкриваю попап образом");
            var info = GF.GetTextOfElement(GXP.TextOfFirstItemInObrazPopup);
            GF.ClickOn(GXP.PreviousObrazButton, "переходжу до наступного образу");
            Assert.AreNotEqual(GF.GetTextOfElement(GXP.TextOfFirstItemInObrazPopup).Trim(), info.Trim(), "Попап успішно закритий");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Закрытие попапа c образом")]
        public void OB4_CloseObrazPopup()
        {            
            GF.MoveToElement(PPattern.NashyTrendyButton, "прямую до Наши тренды");
            GF.ClickOn(PPattern.OfficeСollection, "нажимаю на Весна 21");
            GF.CheckAction(POficeCollect.ViewFirstObrazVesna21Button.Displayed, "Жду загрузки трендов");
            GF.MoveToElement(POficeCollect.ViewMiddleObrazVesna21Button, "двигаюсь к попАпу образа");
            GF.ClickOn(POficeCollect.ViewFirstObrazVesna21Button,"");;
            GF.ClickOn(GXP.CloseObrazPopupButton, "закриваю образ");
            Thread.Sleep(1000);
            List<IWebElement> popUp = GXP.PopUpObrazov;
            Assert.IsTrue(GF.CheckElementNotExist(popUp), "Попап успішно закритий");
        }

        [Test]
        [Author("Vanya Demenko, Dmytro Lytovchenko"), Description("Проверка корректности расчета суммы в попапе с образом")]
        public void OB5_CorrectnessOfCalculationOfAmountObrazPopup()
        {
            GF.MoveToElement(PPattern.NashyTrendyButton, "Направляюсь в Наши тренды");
            GF.ClickOn(PPattern.OfficeСollection, "Нажимаю на Весна 21");
            GF.CheckAction(POficeCollect.ViewFirstObrazVesna21Button.Displayed, "Жду загрузки трендов");
            GF.MoveToElement(POficeCollect.ViewFirstObrazVesna21Button, "двигаюсь к попАпу образа");
            GF.WaitForClickabilityOfElement(POficeCollect.ViewFirstObrazVesna21Button); 
            GF.ClickOn(POficeCollect.ViewMiddleObrazVesna21Button, "Открываю образ");
            GF.CheckAction(GXP.TextOfFirstItemInObrazPopup.Displayed, "жду загрузки товара коллекций", 30);
            int summOfitems = PStayWarm.SummOfPricesOfObrazItems(GXP.ListOfPriceItemsObrazPopup);
            Assert.AreEqual(PStayWarm.GetNumberPrice(GXP.TotalSummOfAmountObrazPopup), summOfitems, "суму підраховано правильно");
        }
    }
}
