﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.FilterQuickViewSearch
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Поиск"), Property("TestSuite:", "S")]
    class TestSearch<TBrowser> : TestBase<TBrowser>
    {
        private readonly string SearchText = "платье";
        private readonly string SearchErrorMessage = "К сожалению, не удалось ничего найти по вашему запросу";

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка работоспособности поиска товаров по сайту по категории")]
        public void S1_SearchItems()
        {
            GF.ClickOn(PPattern.SearchButton, "натискаю на пошук");
            GF.SendKeys(PPattern.SearchInput, SearchText, "Вводжу текст платье");
            GF.ClickOn(PPattern.GoSearchButton, "Выполняю поиск");
            GF.CheckAction(PCatalog.ProductItemList[0].Displayed, "Ожидаю загрузки товаров");
            Assert.IsTrue(PCatalog.SearchResultCorrectRequest(PCatalog.ProductItemsTitlesList, SearchText), "Заданные товары найдены");;
        }

        [Test]
        [Author("Vanya Demenko"), Description("Проверка работоспособности поиска товаров по сайту по названию")]
        public void S2_SearchItemByTitle()
        {
            GF.MoveToElement(PPattern.NashyTrendyButton, "прямую до Наши тренды");
            GF.ClickOn(PPattern.NashiTkaniButton, "натискаю на Наши ткани");
            GF.ClickOn(PNashiTkany.FirstItemNashiTkani, "натискаю на товар з першого слайдеру");
            var title = GF.GetTextOfElement(PItem.NameOfItemInPageItem);
            GF.ClickOn(PPattern.SearchButton, "натискаю на пошук");
            GF.SendKeys(PPattern.SearchInput, title, "вводжу назву товара");
            GF.ClickOn(PPattern.GoSearchButton, "здійснюю пошук");
            Assert.IsTrue(GF.GetTextOfElement(PCatalog.ProductItemsTitlesList[0]).Contains(title), "Знайдено потрібний товар");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Проверка работоспособности поиска товаров по сайту по артиклу")]
        public void S3_SearchItemByArcticle()
        {
            GF.ClickOn(PPattern.BazovyyOfisButton, "натискаю на Базовий офіс");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            var article = PItem.GetArticleOfItem(PItem.ArticleOfItemInPageItem);
            GF.ClickOn(PPattern.SearchButton, "натискаю на пошук");
            GF.SendKeys(PPattern.SearchInput, article, "вводжу артикул");
            GF.ClickOn(PPattern.GoSearchButton, "здійснюю пошук");
            Assert.IsTrue(PCatalog.ProductItemList[0].Displayed, "Знайдено потрібний товар");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Проверка работоспособности поиска товаров по сайту при неверном названии")]
        public void S4_SearchItemsByWrongText()
        {
            GF.ClickOn(PPattern.SearchButton, "натискаю на пошук");
            GF.SendKeys(PPattern.SearchInput, "мобилка", "вводжу невірний текст");
            GF.ClickOn(PPattern.GoSearchButton, "здійснюю пошук");
            Assert.IsTrue(PCatalog.SearchErrorMessage.Text.Contains(SearchErrorMessage), "Помилку висвітлено");
        }
    }
}
