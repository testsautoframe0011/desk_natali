﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.FilterQuickViewSearch
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Быстрый просмотр"), Property("TestSuite:", "FW")]
    class TestQuickViewPopup<TBrowser> : TestBase<TBrowser>
    {
        private readonly string SearchText = "платье";

        [Test]
        [Author("Vanya Demenko"), Description("Переход на старницу товара с другим цветом Быстрый Просмотр")]
        public void FW1_ChangeColourOfItemInQuickViewPopup()
        {
            GF.ClickOn(PPattern.BazovyyOfisButton, "прямую до Базового офису");
            GF.MoveToElement(PCatalog.ProductItemList[2], "рухаюся до товару");
            GF.ClickOn(GXP.QuickViewOfItemList[2], "натискаю на Быстрый Просмотр");
            var title = GF.GetTextOfElement(GXP.QuickViewItemTitle).Trim();
            GF.ClickOn(GXP.ChangeColourQuickView, "Перехожу на станицу товара другого цвета");
            Assert.AreNotEqual(GF.GetTextOfElement(PItem.NameOfItemInPageItem).Trim(), title, "Перешли на страницу товара другого цвета");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Проверка корректности отображения размеров на странице поиска попап Быстрый просмотр")]
        public void FW2_CorrectnessOfDisplayingSizesInQuickViewPopup()
        {
            GF.ClickOn(PPattern.SearchButton, "натискаю на пошук");
            GF.SendKeys(PPattern.SearchInput, SearchText, "вводжу текст платье");
            GF.ClickOn(PPattern.GoSearchButton, "здійснюю пошук");
            GF.MoveToElement(PCatalog.ProductItemList[0], "навівся на товар");
            List<string> strL = PItem.MakeNewList(PCatalog.SizesHoverEffectList);//записую розміри в лист, при hover
            GF.ClickOn(GXP.QuickViewOfItemList[0], "натискаю на Быстрый Просмотр");
            List<string> strL1 = PItem.MakeNewList(GXP.SizesQuickView);//записую розміри в лист з попапа быстрый просмотр
            Assert.IsTrue(PItem.ComparingTwoListsSorted(strL, strL1), "Розміри висвітлено");
        }
        #region - TODO (temporarily disabled)

        /*        [Test]
                [Author("Vanya Demenko"), Description("Переход на страницу товара с листа желаний попап Быстрый просмотр")]
                public void FW3Vuejsnetvprof_GoToItemPageFromQuickViewPopup()
                {
                    PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
                    PProf.DeleteAllItemsFromWishListAuth("видаляю всі товари з листа бажань");
                    GF.ClickOn(PPattern.BazovyyOfisButton, "прямую до Базового офісу");
                    //GF.ClickOn(PPattern.BazovyeBryukiButton, "натискаю на базові брюки");
                    GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
                    GF.ClickOn(PItem.AddToWishListButtonPegeItem, "додаю до листа бажань");
                    GF.CheckAction(PItem.ItemIsAddedToWishListPegeItem.Displayed, "чекаю додавання елемента до листа");
                    GF.ClickOn(PPattern.ProfileButton, "переходжу в профіль");
                    GF.ClickOn(PProf.WishListButton, "натискаю на лист бажань");
                    GF.MoveToElement(PProf.FirstItemInWishList, "рухуюся до товару");
                    GF.ClickOn(PCatalog.QuickViewOfFirstItemWishList, "натискаю на Быстрый Просмотр");
                    var title = GF.GetTextOfElement(PCatalog.ProductItemTitleQuickView);
                    GF.ClickOn(PCatalog.GoToItemPageQuickViewButton, "натискаю на Перейти на страницу товара");
                    Assert.AreEqual(GF.GetTextOfElement(PItem.NameOfItemInPageItem), title, "Успішно перейшли на сторінку товара");
                }*/
        #endregion
    }
}
