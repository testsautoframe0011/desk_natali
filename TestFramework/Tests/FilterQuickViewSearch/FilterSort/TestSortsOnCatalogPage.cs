using NUnit.Framework;
using TestFramework.Pages;
using TestFramework.Tests;
using System.Threading;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using TestFramework.General;
using NUnit.Framework.Internal;
using System.Collections.Generic;

namespace TestFramework.Tests.FilterQuickViewSearch.FilterSort
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("����������"), Property("TestSuite:", "ST")]
    class TestSortsOnCatalogPage<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Vanya Demenko"), Description("�������� ���������� �������� �� �����������")]
        public void ST1_SortLowestPrice()
        {
            GF.MoveToElement(PPattern.CatalogButton, "������ �� ��������");
            GF.ClickOn(PPattern.PlatyaButton, "�������� �� ������");
            GF.CheckAction(PCatalog.ProductItemList[1].Displayed);
            //filter
            var old = PCatalog.ProductItemList[0];
            GF.ClickOn(PCatalog.CatalogSortSelectRight);
            GF.ClickOn(PCatalog.UbivanieSelectRight);
            GF.CheckElementNotAttachedToDom(old);
           // GF.CheckAction(!NewCat.ProductItemList[0].Text.Contains(old));

            Assert.IsTrue(PCatalog.SortPrice(PCatalog.ResultsBySortProducts, true), "������� ����������� ����");
        }

        [Test]
        [Author("Vanya Demenko"), Description("�������� ���������� �������� �� ��������")]
        public void ST2_SortHighestPrice()
        {
            GF.MoveToElement(PPattern.CatalogButton, "������ �� ��������");
            GF.ClickOn(PPattern.KostyumyButton, "�������� �� �������");
            GF.CheckAction(PCatalog.ProductItemList[1].Displayed);
            //filter
            var old = PCatalog.ProductItemList[0].Text;
            GF.ClickOn(PCatalog.CatalogSortSelectRight);
            GF.ClickOn(PCatalog.VozrastanieSelectRight);
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            GF.CheckAction(!PCatalog.ProductItemList[0].Text.Contains(old));
            //Thread.Sleep(2000);//wait for loading    
            Assert.IsTrue(PCatalog.SortPrice(PCatalog.ResultsBySortProducts, false), "������� ����������� ����");
        }

        [Test]
        [Author("Vanya Demenko"), Description("��� ������������ �� ������� �����������")]
        public void ST4_SortAfterFilter()
        {
            GF.MoveToElement(PPattern.CatalogButton, "������ �� ��������");
            GF.ClickOn(PPattern.PlatyaButton, "�������� �� �������");
            GF.CheckAction(PCatalog.ProductItemList[1].Displayed);

            //filter
            GF.ClickOn(PCatalog.RedColourInFilterLeftMenu, "������ �������� ����");
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            GF.CheckAction(PCatalog.DeselectFilterrButton[0].Displayed, "Գ���� ������������");
            var filtersort = PCatalog.FiltredPrice;
            Thread.Sleep(2000);

            GF.MoveToElement(PCatalog.h1Catalog); 
            GF.ClickOn(PCatalog.CatalogSortSelectRight);
            GF.ClickOn(PCatalog.UbivanieSelectRight);

            Thread.Sleep(2000);
            GF.CheckAction(PCatalog.SortPrice(PCatalog.ResultsBySortProducts, true));

            Assert.IsTrue(GF.CheckDisplayed(filtersort), "������� ����������� ����");        
        }

        [Test]
        [Author("Vanya Demenko"), Description("��� ���������� �� ��������� ��� ��������� ��������� ")]
        public void ST5_SortAfterFilterOff()
        {
            GF.MoveToElement(PPattern.CatalogButton, "������ �� ��������");
            GF.ClickOn(PPattern.PlatyaButton, "�������� �� �������");
            GF.CheckAction(PCatalog.ProductItemList[1].Displayed);

            //filter
            GF.ClickOn(PCatalog.RedColourInFilterLeftMenu, "������ �������� ����");
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            GF.CheckAction(PCatalog.DeselectFilterrButton[0].Displayed, "Գ���� ������������");

            //filter
            GF.MoveToElement(PCatalog.h1Catalog);
            GF.ClickOn(PCatalog.CatalogSortSelectRight);
            GF.ClickOn(PCatalog.VozrastanieSelectRight);
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            GF.CheckAction(PCatalog.SortPrice(PCatalog.ResultsBySortProducts, false));


            //filter
            var filtersort = PCatalog.SortPrice(PCatalog.ResultsBySortProducts, false);
            GF.MoveToElement(PCatalog.h1Catalog);
            GF.ClickOn(PCatalog.DeselectFilterrButton[0]);
            Thread.Sleep(2000);

            var filtersort2 = PCatalog.SortPrice(PCatalog.ResultsBySortProducts, false);

            Assert.IsTrue(filtersort.Equals(true) && filtersort2.Equals(true), "������� ����������� ����");
        }
    }
}