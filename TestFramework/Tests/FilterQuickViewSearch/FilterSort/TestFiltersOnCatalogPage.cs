﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.General;

namespace TestFramework.Tests.FilterQuickViewSearch.FilterSort
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Страница товара"), Property("TestSuite:", "F")]
    class TestFiltersOnCatalogPage<TBrowser> : TestBase<TBrowser>
    {
        private readonly string RedColour = "Красный";
        private readonly string BlackColour = "Черный";

        private readonly string InputMinPriceFilter = "1000";
        private readonly string InputMaxPriceFilter = "2000";

        [Test]
        [Author("Vanya Demenko"), Description("Проверка работы фильтра: Цвет (Левое боковое меню фильтров)")]
        public void F1_ColourFilterOnCatalogPageLeftMenu()
        {
            GF.NavigateToUrl(PCatalog.PlatyaUrl);
            GF.MoveToElement(PCatalog.ProductItemList[0], "обераю червоний колір");
            GF.ClickOn(PCatalog.RedColourInFilterLeftMenu, "обераю червоний колір");

            //filters
            var old = PCatalog.ProductItemList[0].Text;
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            GF.CheckAction(!PCatalog.ProductItemList[0].Text.Contains(old));

            GF.CheckAction(PCatalog.FilterHasBeenAppliedTop.Displayed, "фільтр застосувався");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            Assert.IsTrue(GF.GetTextOfElement(PItem.ColourOfItem).Contains(RedColour), "Товар потрібного кольору");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Проверка работы фильтра: Цвет (Верхнее меню фильтров)")]
        public void F2_ColourFilterOnCatalogPageTopMenu()
        {
            GF.ClickOn(PPattern.ObuvButton, "прямую до обувь и сумки");

            //filters
            GF.MoveToElement(PCatalog.ColoursFilterTopMenu, "відкриваю фільтр: Колір (верхнє меню)");
            GF.ClickOn(PCatalog.BlackColourInFilterTopMenu, "обераю чорний колір");

            //var old = PCatalog.ProductItemList[0].Text;
            //GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            //GF.CheckAction(!PCatalog.ProductItemList[0].Text.Contains(old));

            GF.CheckAction(PCatalog.FilterHasBeenAppliedTop.Displayed, "фільтр застосувався");
            GF.JavaScriptClick(PCatalog.ProductItemsTitlesList[0], "натискаю на перший товар на сторінці");
            Assert.AreEqual(GF.GetTextOfElement(PItem.ColourOfItem), BlackColour, "Товар потрібного кольору");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Проверка работы фильтра: Размер (Левое боковое меню фильтров)")]
        public void F3_SizeFilterOnCatalogPageLeftMenu()
        {
            GF.MoveToElement(PPattern.CatalogButton, "переходжу до Каталога");
            GF.ClickOn(PCatalog.BluzyITunikiButton, "натискаю на Блузы и туники");

            var sizeOfFilter = GF.GetTextOfElement(PCatalog.FirstSizeInFilterLeftMenu);
            GF.ClickOn(PCatalog.SizeInFilterList[0], "обераю перший розмір зі списку доступних розмірів");

            GF.CheckAction(PCatalog.FilterHasBeenAppliedTop.Displayed, "фільтр застосувався");
            Thread.Sleep(2000);
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            Assert.AreEqual(GF.GetTextOfElement(GXP.ItemSize), sizeOfFilter, "Товар потрібного розміру");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Проверка работы фильтра: Размер (Верхнее меню фильтров)")]
        public void F4_SizeFilterOnCatalogPageTopMenu()
        {
            GF.ClickOn(PPattern.ObuvButton, "прямую до обувь и сумки");

            GF.ClickOn(PCatalog.SizesFilterTopMenu, "відкриваю фільтр: Розмір (верхнє меню)");
            var old = PCatalog.ProductItemList[3].Text;

            var sizeOfFilter = GF.GetTextOfElement(PCatalog.FirstSizeInFilterTopMenu);
            GF.ClickOn(PCatalog.FirstSizeInFilterTopMenu, "обераю перший розмір зі списку доступних розмірів");

            //filters
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[3]);
            GF.CheckAction(!PCatalog.ProductItemList[0].Text.Contains(old));

            GF.CheckAction(PCatalog.FilterHasBeenAppliedTop.Displayed, "фільтр застосувався");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            Assert.AreEqual(GF.GetTextOfElement(GXP.ItemSize), sizeOfFilter, "Товар потрібного розміру");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Проверка работы фильтра: Размеры eu/ua (Левое боковое меню фильтров)")]
        public void F5_TypeOfSizesFilterOnCatalogPageLeftMenu()
        {
            GF.MoveToElement(PPattern.CatalogButton, "прямую до каталога");
            GF.ClickOn(PPattern.TopyButton, "натискаю на Топы");
            GF.MoveToElement(PCatalog.ProductItemList[0], "навівся на товар");
            GF.CheckAction(PCatalog.HoverSize.Displayed, "");
            List<string> strL = PItem.MakeNewList(PCatalog.SizesHoverEffectList);//записую розміри в лист, при hover

            GF.ClickOn(PCatalog.ChangeTypeOfSizesButton, "змінюю тип розмірів");
            GF.MoveToElement(PCatalog.ProductItemList[0], "навівся на товар");

            GF.CheckAction(PCatalog.SizesHoverEffectList[0].Displayed);
            List<string> strL1 = PItem.MakeNewList(PCatalog.SizesHoverEffectList);//записую розміри в лист, при hover
            Assert.IsFalse(PItem.ComparingTwoLists(strL, strL1), "Тип розмірів змінено");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Проверка работы фильтра: Цена")] //ползунок цены
        public void F6_PriceFilterInputCatalog()
        {
            GF.MoveToElement(PPattern.CatalogButton, "прямую до каталога");
            GF.ClickOn(PCatalog.PlatyaButton,"Перехожу в Платья");
            GF.ClickOn(PCatalog.OpenMoreFilterButton,"Раскрываю дополнительные фильтры");
            GF.ClickOn(PCatalog.OpenPriceFilterButton, "Раскрываю фильтр цены");          
            GF.SendKeysClickEnter(PCatalog.PriceMinFilter, InputMinPriceFilter, "Минимальная цена 1000");
            GF.CheckAction(PCatalog.DeselectFilterrButton[0].Displayed);
            var a = PCatalog.ProductItemList[0];
            GF.SendKeysClickEnter(PCatalog.PriceMaxFilter, InputMaxPriceFilter, "Максимальная цена 2000");
            GF.CheckElementNotAttachedToDom(a);
            GF.CheckAction(PCatalog.DeselectFilterrButton[0].Displayed && PCatalog.DeselectFilterrButton[0].Text.Contains(InputMaxPriceFilter) && PCatalog.DeselectFilterrButton[0].Text.Contains(InputMinPriceFilter));       
            Assert.IsTrue(PCatalog.CheckPriceFilterINT(PCatalog.ProductItemListPrice, InputMinPriceFilter, InputMaxPriceFilter), "Фільтр працює вірно");
        }


        [Test]
        [Author("Vanya Demenko"), Description("Возможность сбросить все фильтры на странице каталога")] 
        public void F8_ResetAllFiltersInputsOnCatalogPage()
        {
            GF.MoveToElement(PPattern.CatalogButton, "прямую до Каталога");
            GF.ClickOn(PPattern.PlatyaButton, "натискаю на Платья");
            GF.ClickOn(PCatalog.RedColourInFilterLeftMenu, "обераю червоний колір");
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            GF.CheckAction(PCatalog.DeselectFilterrButton[0].Displayed, "Фільтр застосувався");
            var oneFilter = PCatalog.DeselectFilterrButton.Count;

            GF.ClickOn(PCatalog.SizeInFilterList[3], "обераю 4 розмір зі списку доступних розмірів");
            GF.CheckAction(PCatalog.FilterSizeHasBeenAppliedTop.Displayed);

            var title = GF.GetTextOfElement(PCatalog.ProductItemList[0]);
            GF.ClickOn(PCatalog.ResetAllFiltersButton, "натискаю на Сбросить фильтр");
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            Assert.AreNotEqual(GF.GetTextOfElement(PCatalog.ProductItemList[0]), title, "Фільтр працює вірно");
        }


        [Test]
        [Author("Vanya Demenko"), Description("отключить один фильтр из 2")]
        public void F9_ResetOneOfTwoFiltersOnCatalogPage()
        {
            GF.MoveToElement(PPattern.CatalogButton, "прямую до Каталога");
            GF.ClickOn(PPattern.PlatyaButton, "натискаю на Платья");

            //filter
            GF.ClickOn(PCatalog.RedColourInFilterLeftMenu, "обераю червоний колір");            
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            GF.CheckAction(PCatalog.DeselectFilterrButton[0].Displayed, "Фільтр застосувався");

            //filter
            GF.ClickOn(PCatalog.SizeInFilterList[2], "обераю 3 розмір зі списку доступних розмірів");
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            GF.CheckAction(PCatalog.FilterHasBeenAppliedTop.Displayed, "фільтр застосувався");

            var title = GF.GetTextOfElement(PCatalog.ProductItemsTitlesList[0]);
            GF.ClickOn(PCatalog.DeselectFilterrButton[1], "Отменяю выбор одного из фильтров");
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            Assert.AreNotEqual(GF.GetTextOfElement(PCatalog.ProductItemsTitlesList[0]), title, "Фільтр працює вірно");
        }
    }
}
