﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.AutorizationProfile.Autorization
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Авторизация"), Property("TestSuite:", "A")]
    class TestAuthorization<TBrowser> : TestBase<TBrowser>
    {
        private readonly string ExpectedAuthorizationUrl = "personal";

        [Test]
        [Author("Vanya Demenko, Dmytro Lytovchenko"), Description("Авторизация клиента")]
        public void A1_AuthorizationUser()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизовується");
            GF.ClickOn(PPattern.ProfileButton, "перейти в профіль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "жду меню профиля");
            Assert.IsTrue(Driver.Url.Contains(ExpectedAuthorizationUrl), "користувач успішно авторизувався");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Авторизация клиента с неправильным логином")]
        public void A2_AuthorizationUserWithWrongLogin()
        {
            GF.ClickOn(PPattern.ProfileButton, "перейти в профіль");
            GF.SendKeys(PAthrzt.EmailInput, "aniartqa@gmail.com", "вводимо логін");
            GF.SendKeys(PAthrzt.PasswordInput, "32424243423", "вводимо хибний пароль");
            GF.ClickOn(PAthrzt.SubmitButton, "натискаємо Войти");
            Assert.IsTrue(PAthrzt.AuthErrorMessage.Displayed, "користувач не авторизувався");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Авторизация клиента с неправильным паролем")]
        public void A3_AuthorizationUserWithWrongPassword()
        {
            GF.ClickOn(PPattern.ProfileButton, "перейти в профіль");
            GF.SendKeys(PAthrzt.EmailInput, "ivantestrwerw@gmail.com", "вводимо хибний логін");
            GF.SendKeys(PAthrzt.PasswordInput, "aniartqagmailcom", "вводимо пароль");
            GF.ClickOn(PAthrzt.SubmitButton, "натискаємо Войти");
            Assert.IsTrue(PAthrzt.AuthErrorMessage.Displayed, "користувач не авторизувався");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Авторизация клиента через Facebook")]
        public void A4_AuthorizationUserByFacebook()
        {
            GF.ClickOn(PPattern.ProfileButton, "Нажимаю на профиль");
            GF.ClickOn(PAthrzt.FacebookButton, "Выбираю авторизацию через Facebook");
            PAthrzt.FacebookAuth(User.UserFacebook, "Выполняю авторизацию");
            GF.ClickOn(PPattern.ProfileButton, "Нажимаю на кнопку профиля");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            GF.ClickOn(PProf.PersonalDataButton, "Перехожу в личные данные");
            Assert.IsTrue(PProf.ProfileEmail.GetAttribute("value").Equals(User.UserFacebook.Email), "Клиент авторизирован");
        }
    }
}
