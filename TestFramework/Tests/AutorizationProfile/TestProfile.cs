﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.AutorizationProfile.Profile
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Профиль"), Property("TestSuite:", "P")]
    class TestProfile<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Vanya Demenko"), Description("Выход из аккаунта")]
        public void P1_LogOutFromAccount()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизовується");
            GF.ClickOn(PPattern.ProfileButton, "перейти в профіль");
            GF.CheckAction(PProf.ProfileMenu.Displayed);
            GF.ClickOn(PProf.LogOutButton, "вихід з профілю");
            Thread.Sleep(2000);
            GF.ClickOn(PPattern.ProfileButton, "перейти в профіль");
            Assert.IsTrue(PAthrzt.AuthorizationPopup.Displayed, "користувач вийшов, попап для авторизації відкрився");
        }


        [Test]
        [Author("Vanya Demenko"), Description("Переход на страницу товара через лист желания профиль")]
        public void P2_GoToItemPageFromFishListProfile()
        {

            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PProf.DeleteAllItemsFromWishListAuth("видаляю всі товари з листа бажань");
            GF.MoveToElement(PPattern.ObuvButton, "прямую до обувь и сумки");
            GF.ClickOn(PPattern.TufliButton, "натискаю на Полуботинки");
            GF.ClickOn(PCatalog.AddToWishListButtonOfFirstItem, "додаю до листа бажань");
            GF.ClickOn(PPattern.ProfileButton, "переходжу на профіль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "натискаю на лист бажань");
            var title = GF.GetTextOfElement(PProf.TitleOfFirstItemWishList);
            GF.ClickOn(PProf.FirstItemInWishList, "переходжу на сторінку товара");
            Assert.AreEqual(GF.GetTextOfElement(PItem.NameOfItemInPageItem), title, "Успішно перейшли на сторінку товара, що знаходиться в листу бажань");
        }


        [Test]
        [Author("Vanya Demenko"), Description("Возможность изменить личные данные в профиле")]
        public void P3_ChangePersonalInformationProfile()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизовується");
            GF.ClickOn(PPattern.ProfileButton, "перейти в профіль");
            GF.ClickOn(PProf.PersonalDataButton, "натискаю на лист бажань особисті дані");
            var name = PProf.NameInput.GetAttribute("value");
            var surname = PProf.LastNameInput.GetAttribute("value");
            GF.SendKeys(PProf.NameInput, surname, "змінію ім'я");
            GF.SendKeys(PProf.LastNameInput, name, "змінію призвіще");
            GF.ClickOn(PProf.SavePersonalInfo, "зберігаю зміни");
            Assert.IsTrue(PProf.SuccessMessage.Displayed, "повідомлення про успішні зміни висвітлено");
        }
    }
}
