﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.Item
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Страница товара"), Property("TestSuite:", "PI")]
    class TestPageItem<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Vanya Demenko"), Description("Смена цвета товара на странице товара")]
        public void PI1_ChangeColourOfItemInItemPage()
        {
            GF.MoveToElement(PPattern.CatalogButton, "прямую до каталога");
            GF.ClickOn(PPattern.VerkhnyayaOdezhdaButton, "натискаю на Верхній одяг");
            GF.CheckAction(PCatalog.ProductItemList[0].Displayed, "");
            PCatalog.FindAbilityChangeColor("Выполняю поиск товара с возможностью сменить цвет");
            var title = GF.GetTextOfElement(PItem.NameOfItemInPageItem).Trim();
            GF.ClickOn(GXP.ChangeColour, "змінюю колір товара");
            GF.CheckAction(PItem.NameOfItemInPageItem.Displayed, "Жду загрузки товара");
            Assert.AreNotEqual(GF.GetTextOfElement(PItem.NameOfItemInPageItem).Trim(), title, "колір успішно змінений");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Смена типа размеров на странице товара UA/EU")]
        public void PI2_ChangeTypeOfItemSizesInItemPage()
        {
            GF.ClickOn(PPattern.SaleButton, "Перехожу в Sale");
            GF.ClickOn(PCatalog.ProductItemList[0], "Выбираю первый товар");

            GF.CheckAction(PItem.SizesPageItemList[0].Displayed);
            List<string> strL = PItem.MakeNewList(PItem.SizesPageItemList);//записую розміри в лист
            GF.ClickOn(PItem.ChangeTypeOfSizesButton, "змінюю тип розмірів");

            GF.CheckAction(PItem.TypesOfSizes.GetAttribute("class").Contains("selected"));
            List<string> strL1 = PItem.MakeNewList(PItem.SizesPageItemList);//записую нові розміри в лист
            Assert.IsFalse(PItem.ComparingTwoListsSorted(strL, strL1), "Тип розмірів змінено");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Проверка наличия таблицы размеров")]
        public void PI3_CheckSizeTableInPageItem()
        {
            GF.MoveToElement(PPattern.CatalogButton, "прямую до каталога");
            GF.ClickOn(PPattern.GolfySvitshotyButton, "натискаю на костюми");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(PItem.SizeTableButton, "натискаю на Таблицю розмірів");
            Assert.IsTrue(PItem.SizeTablePopup.Displayed, "Таблицю розмірів висвітлено");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Просмотр функционала наличия товара в магазинах")]
        public void PI4_ViewItemAvailabilityInStore()
        {
            GF.ClickOn(PPattern.NovinkiButton, "нажимаю на Новинки");
            GF.ClickOn(PCatalog.ProductItemList[0], "нажимаю на первый товар на странице");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.ClickOn(PItem.AvailabilityInStore, "нажимаю на Наличие в магазинах");
            GF.CheckAction(PItem.AvailabilityInStoreMessage.Displayed);           
            Assert.IsTrue(PItem.CheckTheDifferenceInStoreAddresses(), "Адреса магазинов отображаются");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Проверка функционала Быстрый просмотр на детальной товара (Слайдер просмотренные товары)")]
        public void PI5_CheckQuickViewInPageItem()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            GF.MoveToElement(PPattern.BazovyyOfisButton, "прямую до базового офісу");
            GF.ClickOn(PPattern.BazovyeBluzyTopyButton, "натискаю на базові блузи і топи");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.MoveToElement(PItem.ViewedProductsSliderItem, "рухаюся до першого товару в слайдері переглянуті товари");
            GF.ClickOn(PItem.ViewedProductsSliderItemQuickView, "натискаю на Быстрый Просмотр");
            Assert.IsTrue(PCatalog.ProductItemTitleQuickView.Displayed, "Попап быстрый просмотр відкрито");
        }

        #region - TODO (temporarily disabled)

        //[Test]
        //[Author("Dmytro Lytovchenko"), Description("Проверка функционала Слайдер изображений товара")]
        //public void PI6_CheckSliderImgOfCurrentProduct()
        //{
        //    GF.MoveToElement(PPattern.CatalogButton, "Направляюсь к Каталогу");
        //    GF.ClickOn(PPattern.GolfySvitshotyButton, "Нажимаю на Гольфы и Свитшоты");
        //    GF.ClickOn(PCatalog.ProductItemList[1], "Нажимаю на первый товар на странице");
        //    Assert.IsTrue(PItem.CheckSlider(PItem.ProductSlider, PItem.MainImgList, "Проверка отображения выбраного изображения слайдера"), "Слайдер работает");

        //    Console.WriteLine("NB-798");
        //}
        #endregion

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка функционала изменений изображений на xl")]
        public void PI7_CheckAbilityToToggleSizes()
        {
            GF.NavigateToUrl(PItem.ChangeToXLUrl);
            Assert.IsTrue(PItem.CheckMainImgAfterToggleSize(), "Изменяю изображение на xl и проверяю его отображение");
        }

        #region - TODO (temporarily disabled)
        //[Test]
        //[Author("Dmytro Lytovchenko"), Description("Проверка функционала слайдера изображений xl")]
        //public void PI8_CheckSlideXLSize()
        //{
        //    GF.MoveToElement(PPattern.CatalogButton, "Направляюсь к Каталогу");
        //    GF.ClickOn(PPattern.GolfySvitshotyButton, "Нажимаю на Гольфы и Свитшоты");
        //    PCatalog.FindAbilityChangeSize("Поиск товара с возможностью сменить размер");
        //    //GF.ClickOn(PItem.ChangeSize, "Смена размера на xl");
        //    GF.CheckAction(PItem.CheckMainImgAfterToggleSize(), "Изменяю изображение на xl и проверяю его отображение");
        //    Assert.IsTrue(PItem.CheckSlider(PItem.ProductSlider, PItem.MainImgList, "Проверка отображения XL изображений слайдера"), "Слайдер работает");
        //}
        #endregion

        #region - TODO (temporarily disabled)
        //[Test]
        //[Author("Dmytro Lytovchenko"), Description("Проверка функционала слайдера при смены с xl на xs")]
        //public void PI9_CheckSliderAfterToggleXlToXs()
        //{
        //    GF.MoveToElement(PPattern.CatalogButton, "Направляюсь к Каталогу");
        //    GF.ClickOn(PPattern.GolfySvitshotyButton, "Нажимаю на Гольфы и Свитшоты");
        //    PCatalog.FindAbilityChangeSize("Поиск товара с возможностью сменить размер");
        //    GF.CheckAction(PItem.CheckMainImgAfterToggleSize(), "Смена изображений на XL");
        //    //PItem.WaitPageFullLoaded();
        //    Assert.IsTrue(PItem.CheckSlider(PItem.ProductSlider, PItem.MainImgList, "Проверяю слайдер после смены изображений с Хl на XS"), "Слайдер работает");
        //}
        #endregion

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка функционала слайдера в режиме фуллскрин")]
        public void PI10_CheckFullScreenImgSlider()
        {
            GF.ClickOn(PPattern.BazovyyOfisButton, "Нажимаю на Базовый офис");
            GF.ClickOn(PCatalog.ProductItemList[1], "Нажимаю на первый товар на странице");
            GF.ClickOn(PItem.FullSizeButton, "Включаю режим фулскрин");
            GF.CheckAction(PItem.FullImg.Displayed, "Жду отображения фуллскрин изображений");
            Assert.IsTrue(PItem.CheckFullScreenSlider(PItem.FullScreenImgSlider, PItem.FullScreenImg, "Проверка слайдера"), "Слайдер в режиме фуллскрин работает");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка функционала выхода из режима фуллскрин")]
        public void PI11_CheckFullScreenQuitButton()
        {
            GF.MoveToElement(PPattern.BazovyyOfisButton, "Направляюсь к Базовому офису");
            GF.ClickOn(PPattern.BazovyeBluzyTopyButton, "Нажимаю на Базовый офис");
            GF.ClickOn(PCatalog.ProductItemList[1], "Нажимаю на первый товар на странице");
            GF.ClickOn(PItem.FullSizeButton, "Включаю фуллскрин режим");
            GF.CheckAction(PItem.FullImg.Displayed, "Жду отображения фуллскрин изображений");
            GF.ClickOn(PItem.QuitFullScreenMode, "Выхожу с фулскрин режима");
            Assert.IsTrue(PItem.NameOfItemInPageItem.Displayed, "Выход выполнен");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка функционала зума в режиме фуллскрин")]
        public void PI12_CheckFullScreenZoom()
        {
            GF.MoveToElement(PPattern.BazovyyOfisButton, "Направляюсь к Базовому офису");
            GF.ClickOn(PPattern.BazovyeBluzyTopyButton, "Нажимаю на Базовый офис");
            GF.ClickOn(PCatalog.ProductItemList[1], "Нажимаю на первый товар на странице");
            GF.ClickOn(PItem.FullSizeButton, "Включаю фуллскрин режим");
            GF.CheckAction(PItem.FullImg.Displayed, "Жду отображения фуллскрин изображений");
            Assert.IsTrue(PItem.CheckFullScreenZoomImgDisplayed("Для каждой картинки проверяю зум"), "Функционал зум работает");
        }
    }
}
