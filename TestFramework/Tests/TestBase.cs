﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TestFramework.General;
using TestFramework.Pages;
using TestFramework.Pages.PagesNashyTrendy;

namespace TestFramework.Tests
{

    public class TestBase<TBrowser>
    {
        public IWebDriver Driver { get => Drivers.dr; }
        //public string BaseUrl {get => Drivers.BaseUrl;} 

        [SetUp]
        public void CreateDriver()
        {
            if (typeof(TBrowser) == typeof(ChromeDriver)) { new Drivers("ch"); }

            if (typeof(TBrowser) == typeof(FirefoxDriver)) { new Drivers("fr"); }

            if (typeof(TBrowser) == typeof(string)) { new Drivers("ch-hd"); }

            Driver.Navigate().GoToUrl(Config.BaseUrl);
        }

        [TearDown]
        public void TeardownTest()
        {
            Driver.Close();
            Driver.Quit();
        }


        public PageBase PPattern = new PCatalog();
        public GeneralXPath GXP = new GeneralXPath();
        public PCatalog PCatalog = new PCatalog();
        public User PAthrzt = new User();
        public PageItem PItem = new PageItem();
        public PageCart PCart = new PageCart();
        public PageProfile PProf = new PageProfile();
        public PageCheckOut PCheck = new PageCheckOut();
        public PageShops PShop = new PageShops();



        //Nashi Trendy
        public PageNashiTkani PNashiTkany = new PageNashiTkani();
        public PageStayWarm PStayWarm = new PageStayWarm();
        public OfficeCollection POficeCollect = new OfficeCollection();



    }
}
