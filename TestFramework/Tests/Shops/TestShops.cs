﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests
{

    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Магазины")]
    class TestShops<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Dmytro Lytovchenko"), Description("Города из списка соответствую городам с информацией о магазинах")]
        public void SP1_ListOfCityContainsCorrectCity()
        {
            GF.ClickOn(PPattern.OurShopsButton, "Нажимаю на магазины");
            GF.ClickOn(PShop.DropDownCityButton, "Раскрываю список городов");
            var cityListDropDown = PShop.MakeNewList(PShop.DropDownCityList);
            var cityList = PShop.MakeNewList(PShop.CityListMoreButtons);
            Assert.IsTrue(PShop.ComparingTwoListsSorted(cityListDropDown, cityList));         
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Выбранный город из списка открывает информацию о выбранном городе")]
        public void SP2_ChosenCityByListSelected()
        {
            GF.ClickOn(PPattern.OurShopsButton, "Нажимаю на магазины");
            GF.ClickOn(PShop.DropDownCityButton, "Раскрываю список городов");
            var selectedCity = PShop.CityListMoreButtons[2].Text;
            GF.ClickOn(PShop.DropDownCityList[2], "Выбираю третий город");
            GF.CheckAction(PShop.DropDownCityButton.Text.Contains(selectedCity));
            Assert.AreEqual(PShop.SelectedCityInfo[0].Displayed, PShop.SelectedCityInfo[0].Text.Contains(selectedCity),"Выбранный город открыт, город верный");           
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Карта отображается для каждого выбранного магазина")]
        public void SP3_ChosenCityByListSelected()
        {
            GF.ClickOn(PPattern.OurShopsButton, "Нажимаю на магазины");
            GF.ClickOn(PShop.CityListMoreButtons[0], "Открываю магазины в Киев");
            Assert.IsTrue(PShop.MapChangedCitySelect(), "Карта отображается");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Текущий день недели выделен в Магазинах")]
        public void SP4_ChosenCityByListSelected()
        {
            GF.ClickOn(PPattern.OurShopsButton, "Нажимаю на магазины");
            GF.ClickOn(PShop.CityListMoreButtons[0], "Открываю магазины в Киев");
            Assert.IsTrue(PShop.CheckDay(), "День верный");
        }
    }
}
