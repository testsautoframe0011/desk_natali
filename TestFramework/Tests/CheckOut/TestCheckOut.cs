﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.CheckOut
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Чекаут"), Property("TestSuite:", "CH")]
    class TestCheckOut<TBrowser> : TestBase<TBrowser>
    {
        private readonly string EmailFacebook = "facebookqaaniart@gmail.com";

        private readonly string EmptyCartMessage = "В корзине пусто";

        private readonly string Street = "Аниартовская";
        private readonly string Home = "4/6";
        private readonly string Apartment = "5";

        private readonly string OfficeDelivery = "50";
        private readonly string HomeDelivery = "80";

        [Test]
        [Author("Vanya Demenko"), Description("Авторизация на странице чекаута")]
        public void CH1_AuthorizationCheckoutPage()
        {
            GF.MoveToElement(PPattern.SaleButton, "прямую до Sale");
            GF.ClickOn(PPattern.ZhaketyZhiletySaleButton, "натискаю на Жакеты и жилеты");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");

            GF.ClickOn(PPattern.GoToCartButton, "в попапі натискаю на кнопку Купить");

            GF.SendKeys(PCheck.LoginUserInput, User.UserFirst.Email, "вводимо логін");
            GF.SendKeys(PCheck.PasswordUserInput, User.UserFirst.Password, "вводимо пароль");
            GF.ClickOn(PCheck.AuthSubmitButton, "авторизуємося");
            Assert.IsTrue(PCheck.FinishCheckoutButton.Displayed, "успішна авторизація на сторінці чекаута");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Авторизация на странице чекаута через Facebook"),]
        public void CH2_AuthorizeFacebookCheckoutPage()
        {
            GF.NavigateToUrl(PCatalog.BotinkiSumkiUrl, "Перехожу на страницу Полуботинки");
            GF.MoveToElement(PCatalog.ProductItemList[0], "Направляюсь к товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[0], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            GF.CheckAction(GXP.SizesActiveQuickView.Displayed, "Цвет выбран");

            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");
            GF.ClickOn(PPattern.CartButton, "Открываю попАп Корзины");

            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);

            GF.NavigateToUrl(PCheck.Url, "Перехожу на страницу Чекаута");
            GF.ClickOn(PCheck.FacebookCheckoutAutr, "Нажимаю кнопку авторизации Facebook");
            PAthrzt.FacebookAuth(User.UserFacebook, "Выполняю авторизацию");
            Assert.IsTrue(PCheck.EmailUserInput.GetAttribute("value").Equals(EmailFacebook), "Авторизация выполнена");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Проверка корректности расчёта доставки на чекауте, доставка бесплатная")]
        public void CH3_FreeDeliveryCheckoutPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            GF.ClickOn(PPattern.NovinkiButton, "натискаю на базові платья");
            //filter
            var old = PCatalog.ProductItemList[0].Text;
            GF.ClickOn(PCatalog.CatalogSortSelectRight);
            GF.ClickOn(PCatalog.UbivanieSelectRight);
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            GF.CheckAction(!PCatalog.ProductItemList[0].Text.Contains(old));

            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[1]);
            GF.CheckAction(PCatalog.ProductItemList[1].Displayed);

            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");

            GF.ClickOn(PPattern.CartCloseButton);

            GF.ClickOn(GXP.ItemSizeSecondOfList, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");

            GF.ClickOn(PPattern.GoToCartButton, "в попапі натискаю на кнопку Оформить покупку");

            Assert.IsTrue(PCheck.FreeDeliveryMessageCheckoutOformlenie.Displayed, "Повідомлення про безкоштовну доставку висвітлено");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Проверка корректности расчёта доставки на чекауте, доставка платная в отделение")]
        public void CH4_PaidOfficeDeliveryCheckoutPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            GF.ClickOn(PPattern.NovinkiButton, "натискаю на Новинки");
            //filter
            var old = PCatalog.ProductItemList[0].Text;
            GF.ClickOn(PCatalog.CatalogSortSelectRight);
            GF.ClickOn(PCatalog.VozrastanieSelectRight);
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            GF.CheckAction(!PCatalog.ProductItemList[0].Text.Contains(old));

            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            GF.CheckAction(PCatalog.ProductItemList[0].Displayed);

            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");

            GF.NavigateToUrl(PCheck.Url, "Перехожу на страницу Чекаута");

            GF.DropDownClick(PCheck.DropDownWayOfDelivery, 0, "Доставка в отделение");
            Thread.Sleep(1000);
            Assert.AreEqual(PCheck.PaidDeliveryMessageCheckoutOfomlenie.Displayed, PCheck.PaidDeliveryMessageCheckoutOfomlenie.Text.Contains(OfficeDelivery), "Повідомлення про доставку висвітлено");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Удаление всех кроме одного товара на чекауте, пользователь не авторизован")]
        public void CH5_DeletePartOfItemFromCartCheckoutPage()
        {
            GF.MoveToElement(PPattern.BazovyyOfisButton, "направляюсь к Базовый офис");
            GF.ClickOn(PPattern.BazovyeBluzyTopyButton, "нажимаю на Блузы и туники");
            GF.MoveToElement(PCatalog.ProductItemList[1], "направляюсь к товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[1], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            GF.CheckAction(GXP.SizesActiveQuickView.Displayed, "Цвет выбран");
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");

            GF.MoveToElement(PCatalog.ProductItemList[4], "направляюсь ко второму товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[4], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            GF.CheckAction(GXP.SizesActiveQuickView.Displayed, "Цвет выбран");
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");
            GF.ClickOn(PPattern.CartButton, "Открываю попАп Корзины");

            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);

            GF.NavigateToUrl(PCheck.Url);
            GF.CheckAction(PCheck.ItemCheckOutFeild.Displayed, "жду меню чекаута");
            var beforeDeleting = PCheck.ProductsInCheckout.Count;
            PCheck.DeleteElementsCheckout("удаляю все товары на чекауте, кроме", 1);
            var afterDeleting = PCheck.ProductsInCheckout.Count;
            Assert.AreNotEqual(afterDeleting, beforeDeleting, "Количество товаров на чекауте уменьшилась");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Удаление одного товара на чекауте, пользователь авторизован")]
        public void CH6_DeleteOneItemFromCartCheckoutPageAuthorizedUser()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "пользователь авторизировалсся");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            GF.ClickOn(PPattern.NovinkiButton, "нажимаю на Новинки");
            GF.MoveToElement(PCatalog.ProductItemList[1], "направляюсь к товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[1], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            GF.CheckAction(GXP.SizesActiveQuickView.Displayed, "Цвет выбран");
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");
            GF.ClickOn(PPattern.CartButton, "Открываю попАп Корзины");

            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);


            GF.NavigateToUrl(PCheck.Url, "Перехожу на страницу Чекаута");

            PCheck.DeleteElementsCheckout("удаляю все товары на чекауте");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "жду отображения меню");
            GF.ClickOn(PProf.CartProfileButton, "нажимаю на подпункт корзина");
            Assert.IsTrue(GF.CheckCorrectText(PProf.ProfileMessageOfEmptyCart, EmptyCartMessage), "Корзина пуста");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Удаление двух товаров на чекауте, пользователь авторизован")]
        public void CH7_DeleteTwoItemFromCartCheckoutPageAuthorizedUser()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "пользователь авторизировалсся");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            GF.ClickOn(PPattern.NovinkiButton, "нажимаю на Новинки");
            GF.MoveToElement(PCatalog.ProductItemList[1], "направляюсь к товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[1], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            GF.CheckAction(GXP.SizesActiveQuickView.Displayed, "Цвет выбран");
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");

            GF.ClickOn(PPattern.SaleButton, "нажимаю на Sale");
            GF.MoveToElement(PCatalog.ProductItemList[0], "направляюсь к товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[0], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            GF.CheckAction(GXP.SizesActiveQuickView.Displayed, "Цвет выбран");
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");

            GF.ClickOn(PPattern.CartButton, "Открываю попАп Корзины");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);


            GF.NavigateToUrl(PCheck.Url, "Перехожу на страницу Чекаута");

            GF.CheckAction(PCheck.DeleteItemFromCheckOutButton.Displayed, "жду меню чекаута");
            PCheck.DeleteElementsCheckout("удаляю все товары на чекауте");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "жду отображения меню");
            GF.ClickOn(PProf.CartProfileButton, "нажимаю на подпункт корзина");
            Assert.IsTrue(GF.CheckCorrectText(PProf.ProfileMessageOfEmptyCart, EmptyCartMessage), "Корзина пуста");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Почтовые адреса меняются в соответствии с городом")]
        public void CH8_PostalAddressesChangeAccordingToCity()
        {
            GF.ClickOn(PPattern.NovinkiButton, "нажимаю на Новинки");
            GF.MoveToElement(PCatalog.ProductItemList[1], "направляюсь к товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[1], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            GF.CheckAction(GXP.SizesActiveQuickView.Displayed, "Цвет выбран");
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");

            GF.ClickOn(PPattern.CartButton, "Открываю попАп Корзины");

            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа");

            GF.NavigateToUrl(PCheck.Url, "Перехожу на страницу Чекаута");

            GF.CheckAction(PCheck.FirstBuyButton.Displayed, "Жду загрузки страницы чекаута");
            GF.ClickOn(PCheck.FirstBuyButton, "Нажимаю на кнопку Покупаю впервые");
            GF.DropDownClick(PCheck.DropDownWayOfDelivery, 0, "Доставка в отделение");

            GF.SendKeys(PCheck.InputCity, "Львов", "Ввожу город");
            Thread.Sleep(1000);
            GF.ClickOn(PCheck.ChoosePostOfficeButton, "Открываю список отделений");
            List<string> firstCityPost = PCheck.DropDown(PCheck.DropDownPost);
            GF.SendKeys(PCheck.InputCity, "", "Очищаю поле ввода");
            Thread.Sleep(1000);
            GF.SendKeys(PCheck.InputCity, "ЧЕРКАССЫ", "Ввожу город");
            Thread.Sleep(1000);
            GF.MoveToElementAndClick(PCheck.ShowDeliveryList, "Открываю список отделений");
            List<string> secondtCityPost = PCheck.DropDown(PCheck.DropDownPost);
            Assert.IsFalse(PCheck.ComparingTwoListsBeElements(firstCityPost, secondtCityPost), "Отделения изменились");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка функционал заказа товара до двери ")]
        public void CH9_EnterHomeAddress()
        {
            GF.ClickOn(PPattern.SaleButton, "нажимаю на Распродажу");
            GF.MoveToElement(PCatalog.ProductItemList[0], "направляюсь к товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[0], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            GF.CheckAction(GXP.SizesActiveQuickView.Displayed, "Цвет выбран");
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");

            GF.ClickOn(PPattern.CartButton, "Открываю попАп Корзины");

            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа");

            GF.NavigateToUrl(PCheck.Url, "Перехожу на страницу Чекаута");

            GF.CheckAction(PCheck.FirstBuyButton.Displayed, "Жду загрузки страницы чекаута");
            GF.SendKeys(PCheck.LoginUserInput, User.UserFirst.Email, "Ввожу логин");
            GF.SendKeys(PCheck.PasswordUserInput, User.UserFirst.Password, "Ввожу пароль");
            GF.ClickOn(PCheck.AuthSubmitButton, "Авторизация");

            GF.CheckElementNotAttachedToDom(PCheck.AuthSubmitButton);
            GF.CheckAction(PCheck.AuthOrderForm.Displayed);

            GF.DropDownClick(PCheck.DropDownWayOfDelivery, 1, "Доставка к дому");
            PCheck.FillInputFields(Street, Home, Apartment);
            GF.ClickOn(PCheck.CheckoutToCartButton, "Перехожу в корзину");
            GF.ClickOn(PCart.OrderCheckoutButton, "Перехожу в Оформление заказа");
            Assert.IsTrue(PCheck.TextWasInput(Street, Home, Apartment), "Заказ до двери доступен");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Выбранное отделение почты отображается")]
        public void CH10_SelectedPostOfficeAdressApplyed()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "пользователь авторизировалсся");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");

            GF.ClickOn(PPattern.NovinkiButton, "нажимаю на Новинки");
            GF.MoveToElement(PCatalog.ProductItemList[1], "направляюсь к товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[1], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            GF.CheckAction(GXP.SizesActiveQuickView.Displayed, "Цвет выбран");
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");

            GF.ClickOn(PPattern.CartButton, "Открываю попАп Корзины");

            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа");

            GF.NavigateToUrl(PCheck.Url, "Перехожу на страницу Чекаута");

            GF.CheckAction(PCheck.PriceOfFirstItem.Displayed, "Жду загрузки страницы чекаута");
            GF.DropDownClick(PCheck.DropDownWayOfDelivery, 0, "Доставка в отделение");
            Thread.Sleep(2000);
            GF.SendKeys(PCheck.InputCity, "", "Очищаю поле ввода");
            Thread.Sleep(1000);
            GF.SendKeys(PCheck.InputCity, "ЧЕРКАССЫ", "Ввожу город");
            Thread.Sleep(1000);
            Assert.IsTrue(PCheck.PostIsDisplayed(), "Выбранный адрес почты отображается");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка функционала кнопок способов оплаты")]
        public void CH11_CheckboxPaymentOptions()
        {
            GF.MoveToElement(PPattern.CatalogButton, "направляюсь к Каталогу");
            GF.ClickOn(PPattern.AksessuaryButton, "нажимаю на Аксессуары");
            GF.MoveToElement(PCatalog.ProductItemList[0], "направляюсь к товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[0], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            GF.CheckAction(GXP.SizesActiveQuickView.Displayed, "Цвет выбран");
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");

            GF.ClickOn(PPattern.CartButton, "Открываю попАп Корзины");

            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа");

            GF.NavigateToUrl(PCheck.Url, "Перехожу на страницу Чекаута");

            GF.CheckAction(PCheck.FirstBuyButton.Displayed, "Жду загрузки страницы чекаута");
            GF.ClickOn(PCheck.FirstBuyButton, "Нажимаю на кнопку Покупаю впервые");

            Assert.IsTrue(PCheck.ChangePayment(false, "переключаю на оплату картой"), "RadiоButton оплаты меняются в зависимости от выбора");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка корректности расчёта суммы на Чекауте")]
        public void CH12_CheckSummOfTwoItemsCheckOutPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "пользователь авторизировалсся");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");

            GF.NavigateToUrl(PPattern.NovinkiUrl);
            GF.MoveToElement(PCatalog.ProductItemList[1], "направляюсь к товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[1], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            GF.CheckAction(GXP.SizesActiveQuickView.Displayed, "Цвет выбран");
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");

            GF.MoveToElement(PCatalog.ProductItemList[2], "направляюсь к товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[2], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            GF.CheckAction(GXP.SizesActiveQuickView.Displayed, "Цвет выбран");
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");

            GF.ClickOn(PPattern.CartButton, "Открываю попАп Корзины");

            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа");

            GF.NavigateToUrl(PCheck.Url, "Перехожу на страницу Чекаута");

            GF.CheckAction(PCheck.ItemCheckOutFeild.Displayed, "Жду отображение чекаута");
            var final = (PCheck.GetNumberPrice(PCheck.PriceOfFirstItem) + PCheck.GetNumberPrice(PCheck.PriceOfSecondItem));
            Assert.AreEqual(final, PCheck.GetNumberPrice(PCheck.GeneralPrice), "Подсчет верный");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Проверка корректности расчёта доставки на чекауте, доставка платная к дому")]
        public void CH13_PaidHomeDeliveryCheckoutPage()
        {
            GF.NavigateToUrl(PPattern.SaleUrl);

            //filter
            var old = PCatalog.ProductItemList[0].Text;
            GF.ClickOn(PCatalog.CatalogSortSelectRight);
            GF.ClickOn(PCatalog.VozrastanieSelectRight);
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            GF.CheckAction(!PCatalog.ProductItemList[0].Text.Contains(old));

            GF.ClickOn(PCatalog.ProductItemList[1], "Выбираю первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа");

            GF.NavigateToUrl(PCheck.Url, "Перехожу на страницу Чекаута");

            GF.CheckAction(PCheck.PriceOfFirstItem.Displayed,"Жду загрузку чекаута");
            GF.ClickOn(PCheck.FirstBuyButton, "Нажимаю покупаю впервые");
            GF.DropDownClick(PCheck.DropDownWayOfDelivery, 1, "Выбираю досавку ");
            Thread.Sleep(2000);
            Assert.AreEqual(PCheck.PaidDeliveryMessageCheckoutOfomlenie.Displayed, PCheck.PaidDeliveryMessageCheckoutOfomlenie.Text.Contains(HomeDelivery), "Сообщение о доставка отобразилось");
        }

        /*[Test]
        [Category("Dmytro Lytovchenko"), Description("Удаление одного товара на чекауте, этап Оформление заказа, пользователь не авторизован")]
        public void DeleteOneItemFromCartCheckoutPage()
        {
            GF.MoveToElement(PPattern.ObuvSumkiButton, "направляюсь к Обувь и сумки");
            GF.ClickOn(PPattern.SumkiButton, "нажимаю на Сумки");
            GF.MoveToElement(NewCat.ProductItemList[1], "направляюсь к товару");
            GF.ClickOn(GXP.QuickViewOfItemList[0], "нажимаю на Быстрый просмотр первого товара");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
            GF.ClickOn(PPattern.MakeOrderButton, "в попапе нажимаю на кнопку Оформить покупку");
            PCheck.DeleteElementsCheckout("удаляю все товары на чекауте");
            Assert.IsTrue(PProf.UnregistredUserMessage.Displayed, "Товар удален");
        }

        [Test]
        [Category("Dmytro Lytovchenko"), Description("Удаление двух товаров на чекауте, этап Оформление заказа, пользователь не авторизован")]
        public void DeleteTwoItemFromCartCheckoutPage()
        {
            GF.MoveToElement(PPattern.CatalogButton, "направляюсь к Каталогу");
            GF.ClickOn(PPattern.PlatyaButton, "нажимаю на Платья");
            GF.MoveToElement(NewCat.ProductItemList[1], "направляюсь к товару");
            GF.ClickOn(GXP.QuickViewOfItemList[0], "нажимаю на Быстрый просмотр первого товара");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
            GF.ClickOn(GXP.ItemSizeSecondOfList, "выбираю другой размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
            GF.ClickOn(PPattern.MakeOrderButton, "в попапе нажимаю на кнопку Оформить покупку");
            GF.CheckAction(PCheck.DeleteItemFromCheckOutButton.Displayed, "жду меню чекаута");
            PCheck.DeleteElementsCheckout("удаляю все товары на чекауте");
            Assert.IsTrue(PProf.UnregistredUserMessage.Displayed, "Товар удален");
        }


        [Test]
        [Category("Dmytro Lytovchenko"), Description("Удаление всех кроме одного товара на чекауте, этап Оформление заказа, пользователь не авторизован")]
        public void DeletePartOfItemFromCartCheckoutPage()
        {
            GF.MoveToElement(PPattern.CatalogButton, "направляюсь к Каталогу");
            GF.ClickOn(PPattern.PlatyaButton, "нажимаю на Платья");
            GF.MoveToElement(NewCat.ProductItemList[1], "направляюсь к товару");
            GF.ClickOn(GXP.QuickViewOfItemList[0], "нажимаю на Быстрый просмотр первого товара");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
            GF.ClickOn(GXP.ItemSizeSecondOfList, "выбираю другой размер");
            GF.ClickOn(GXP.BuyButton, "нажимаю кнопку Добавить в корзину");
            GF.ClickOn(PPattern.MakeOrderButton, "в попапе нажимаю на кнопку Оформить покупку");
            GF.CheckAction(PCheck.DeleteItemFromCheckOutButton.Displayed, "жду меню чекаута");
            PCheck.DeleteElementsCheckout("удаляю все товары на чекауте");
            Assert.IsTrue(PProf.CartPrifileTitleZero.Text.Contains(EmptyCartMessage), "Товар удален");
        }*/
    }
}