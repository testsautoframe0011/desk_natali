﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.Cart.CartPopup
{

    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Корзина"), Property("TestSuite:", "С")]
    class TestCartPopupItemAmount<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление двух одинаковых товаров в попап корзины, изменение числа в корзине")]
        public void С1_AddCartPopupTwoIdenticalCartAmountChange()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизовується");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            GF.ClickOn(PPattern.NovinkiButton, "Нажимаю на Новые поступления");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);
            GF.ClickOn(PPattern.CartCloseButton);
            GF.ClickOn(GXP.ItemSizeSecondOfList, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);
            Assert.IsTrue(PPattern.GetQuantityCartPopup("2"), "Иконка корзины отображает наличие 2х товаров");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в попап корзины один товар, изменения числа в корзине ")]
        public void С2_AddCartPopupTwoIdenticalCartAmountChange()
        {
            GF.ClickOn(PPattern.SaleButton, "Нажимаю на Sale");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);
            Assert.IsTrue(PPattern.GetQuantityCartPopup("1"), "Иконка корзины отображает наличие добавленного товара");
        }

/*        [Test]
        [Author("Dmytro Lytovchenko"), Description("Увеличение количества товаров в Корзине, число в попапе увеличилось")]
        public void С3_IncreaseNumberCheckoutCartAmountChange()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизовується");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            GF.ClickOn(PPattern.NovinkiButton, "Нажимаю на Новые поступления");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);
            GF.ClickOn(PPattern.GoToCartButton, "В попапе нажимаю Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton, "Перехожу на этап Корзина");
            GF.ClickOn(PCart.ChangeAmountOfItem, "Открываю список количества товаров");
            //var beforeQuantity = PPattern.CartAmount.Text.Trim();
            GF.DropDownClick(PCart.DropDownQuantity, 1, "Изменяю количество до 2х товаров");
            //GF.CheckAction(!GF.CheckCorrectText(PPattern.CartAmountSelector, beforeQuantity),"Жду",);            
            Assert.IsTrue(PPattern.GetQuantityCartPopup("2"), "Счетчик корзины увеличил свое значение");
        }*/

/*        [Test]
        [Author("Dmytro Lytovchenko"), Description("Уменьшение количества товаров в Корзине, число в попапе уменьшилось")]
        public void С4_ReduceNumberCheckoutCartAmountChange()
        {
            GF.NavigateToUrl(PCatalog.PlatyaUrl, "Перехожу на Платья");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);
            GF.ClickOn(PPattern.GoToCartButton, "В попапе нажимаю Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton, "Перехожу на этап Корзина");
            GF.ClickOn(PCart.ChangeAmountOfItem, "Открываю список количества товаров");
            //var beforeQuantity = PPattern.CartAmount.Text.Trim();
            GF.DropDownClick(PCart.DropDownQuantity, 1, "Увеличиваю количество до 2х товаров");
            bool twoPopup = PPattern.CartAmount.Text.Contains("2");
            Thread.Sleep(1500);
            GF.DropDownClick(PCart.DropDownQuantity, 0, "Уменьшаю количество до 1х товаров");
            bool onePopup = PPattern.CartAmount.Text.Contains("1");
            Thread.Sleep(1500);
            //GF.CheckAction(!GF.CheckCorrectText(PPattern.CartAmountSelector, beforeQuantity),"Жду",);            
            Assert.AreEqual(twoPopup,onePopup, "Счетчик корзины уменьшил свое значение");

            //Assert.IsTrue(GF.CheckAction(PPattern.GetQuantityCartPopup("1") && currentPopup), "Счетчик корзины уменьшил свое значение");
        }*/

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Удаление двух одинаковых товаров, страница профиля, число в попапе пропало")]
        public void С5_DeleteIdenticalProfileCartAmountChange()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Авторизация");
            PCart.DeleteAllItemsFromCartAuth("Очистка корзины");
            GF.NavigateToUrl(PCatalog.NovinkiUrl, "Перехожу в Новинки");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);
            GF.ClickOn(PPattern.CartCloseButton);
            GF.ClickOn(GXP.ItemSizeSecondOfList, "Выбираю другой размер");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed,"Жду отображение попапа корзины");
            GF.NavigateToUrl(PProf.Url, "Перехожу в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду отображение меню Профиля");
            GF.ClickOn(PProf.CartProfileButton, "В профиле нажимаю на корзину");
            GF.ClickOn(PProf.DeleteFromCartProfileButton, "Удаляю товары с корзины");

            Thread.Sleep(2000);

            Assert.IsTrue(GF.CheckElementNotExist(PPattern.CartAmountEmpty), "Счетчик попапа сбросился");
        }


        //Depricated
/*        [Test]
        [Author("Dmytro Lytovchenko"), Description("Изменение числа попапа - удаление 1 из 2 чекаут")]
        public void С6_DeleteCheckoutOneCartAmountChange()
        {
            GF.NavigateToUrl(PCatalog.SaleUrl, "Перехожу на Sale");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.Back("Возвращаюсь назад");
            GF.ClickOn(PCatalog.ProductItemList[1], "Нажимаю на второй товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.NavigateToUrl(PCheck.Url, "Перехожу на чекаут");
            GF.CheckAction(PCheck.DeleteItemFromCheckOutButton.Displayed, "Жду меню чекаута");
            PCheck.DeleteElementsCheckout("Удаляю все товары на чекауте, кроме", 1);
            Thread.Sleep(500);
            Assert.IsTrue(PPattern.GetQuantityCartPopup("1"), "Иконка корзины отображает наличие 1го  товаров");
        }*/

/*        [Test]
        [Author("Dmytro Lytovchenko"), Description("Изменение числа попапа - удаление 1 из 2 корзина")]
        public void С7_DeleteOneBasketCartAmountChange()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Авторизация");
            PCart.DeleteAllItemsFromCartAuth("Очистка корзины");
            GF.NavigateToUrl(PCatalog.BotinkiSumkiUrl, "Перехожу на Ботинки и Сумки");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.NavigateToUrl(PCatalog.PlatyaUrl, "Перехожу в Полуботинки");
            GF.ClickOn(PCatalog.ProductItemList[1], "Нажимаю на второй товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.NavigateToUrl(PCart.Url, "Перехожу в Корзину");
            GF.CheckAction(PCart.DeleteItemButton.Displayed);
            GF.ClickOn(PCart.DeleteItemButton, "Удаляю один товар");
            Assert.IsTrue(PPattern.GetQuantityCartPopup("1"), "Иконка корзины отображает наличие 1го  товаров");
        }*/
/*
        [Test]
        [Author("Dmytro Lytovchenko"), Description("Изменение числа попапа - очистка два одинаковых")]
        public void С8_ClearBasketCartAmountChange()
        {
            GF.NavigateToUrl(PCatalog.BazoviyPlatyaUrl, "Перехожу в Платья");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(GXP.OpenedCartPopUp.Displayed);
            GF.NavigateToUrl(PCart.Url, "Пережу в Корзину");
            GF.CheckAction(PCart.ItemsInCart.Displayed);
            GF.DropDownClick(PCart.DropDownQuantity, 1, "Увеличиваю количество до 2х товаров");
            Thread.Sleep(2000);
            PCart.DeleteAllItemsFromCartAuth("Очистка корзины");
            GF.CheckAction(PCart.ReestablishItemButton.Displayed);
            Assert.IsTrue(GF.CheckElementNotExist(PPattern.CartAmountEmpty), " Числа попап изменилось");
        }*/
/*
        [Test]
        [Author("Dmytro Lytovchenko"), Description("Восстановление товара - попап изменился")]
        public void С9_RestoreBasketCartAmountChange()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Авторизация");
            PCart.DeleteAllItemsFromCartAuth("Очистка корзины");
            GF.NavigateToUrl(PCatalog.SaleUrl, "Перехожу в Распродажи");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.NavigateToUrl(PCheck.NovinkiUrl, "Перехожу в Новинки");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.NavigateToUrl(PCart.Url, "Перехожу в Корзину");
            GF.ClickOn(PCart.DeleteItemButton, "Удаляю товар");
            Thread.Sleep(2000);
            GF.CheckAction(PCart.ReestablishItemButton.Displayed, "Жду отображения кнопки восстановления");
            var before = PPattern.CartAmount.Text;
            GF.ClickOn(PCart.ReestablishItemButton, "Восстанавливаю товар");
            Thread.Sleep(2000);
            var after = PPattern.CartAmount.Text;
            Assert.AreNotEqual(before, after, "Числа попап изменилось");
        }*/

        [Test]
        [Author("Dmytro Lytovchenko"), Description("После возвращения на пердыдущую страницу попап не изменяется")]
        public void С10_PopupSaveValueAfterBack()
        {
            GF.NavigateToUrl(PCheck.BazJacketeUrl,"Базовые жакеты");
            GF.ClickOn(PCatalog.ProductItemList[1], "Нажимаю на первый товар");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Нажимаю кнопку Купить");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);
            GF.Back("Возвращаюсь назад");
            GF.CheckAction(PCatalog.ProductItemList[1].Displayed);            
            Assert.IsTrue(PPattern.GetQuantityCartPopup("1"), "Иконка корзины отображает наличие добавленного товара, при нажатии Назад");
        }
    }
}
