﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.Cart
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Корзина"), Property("TestSuite:", "С")]
    class TestDeleteFromCart<TBrowser> : TestBase<TBrowser>
    {
        private readonly string TextOfEmptyCartMessage = "В корзине пусто";


        [Test]
        [Author("Vanya Demenko"), Description("Удаление товара из попапа корзины")]
        public void С1_DeleteItemFromCart()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизовується");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            GF.MoveToElement(PPattern.CatalogButton, "прямую до каталога");
            GF.ClickOn(PPattern.YubkiButton, "натискаю на Юбки");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            GF.WaitForClickabilityOfElement(PPattern.DeleteItemFromPopupCartButton);
            GF.ClickOn(PPattern.DeleteItemFromPopupCartButton, "видаляю товар з попапа корзини");
            Thread.Sleep(1000);
            GF.ClickOn(PPattern.CartButton, "Открываю малую корзину");
            Assert.IsTrue(PPattern.EmptyMiniCart.Displayed, "Товар видалений з попапа корзини");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Удаление товара из страницы корзины")]
        public void С2_DeleteItemFromCartPage()
        {
            GF.MoveToElement(PPattern.CatalogButton, "прямую до каталога");
            GF.ClickOn(PPattern.KostyumyButton, "натискаю на костюми");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            GF.ClickOn(PPattern.GoToCartButton, "в попапі натискаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton, "Перехожу на этап Корзина");
            GF.ClickOn(PCart.DeleteItemButton, "видаляю товар з корзини");
            Assert.IsTrue(PCart.DeletedtemInCartPage.Displayed, "Товар видалений з корзини");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Удаление товара из корзины в профиле")]
        public void С3_DeleteItemFromCartPageProfile()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизовується");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            GF.MoveToElement(PPattern.CatalogButton, "прямую до каталога");
            GF.ClickOn(PPattern.BluzyITunikiButton, "натискаю на Блузы/туники");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            GF.CheckAction(PPattern.FirstElementInCartPopup.Displayed, "Чекаю, щоб елемент додався до корзини");

            GF.ClickOn(PPattern.CartCloseButton);

            GF.ClickOn(PPattern.ProfileButton, "переходжу на профіль");
            GF.ClickOn(PProf.CartProfileButton, "натискаю на корзина");
            GF.ClickOn(PProf.DeleteFromCartProfileButton, "видаляю товар");
            GF.CheckAction(PProf.CartPrifileTitleZero.Displayed, "Жду загрузки корзины");
            Assert.IsTrue(GF.CheckCorrectText(PProf.ProfileMessageOfEmptyCart, TextOfEmptyCartMessage), "Товар удален из корзины в профиле");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Удаление всех товаров на странице Корзина")]
        public void С4_DeleteAllItemFromCartPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизовується");
            GF.ClickOn(PPattern.ObuvButton, "прямую до обувь и сумки");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            GF.CheckAction(PCart.ReestablishItemButton.Displayed);
            Assert.IsTrue(PCart.DeletedtemInCartPage.Displayed, "Товар видалений з корзини");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Восстановление удаленного товара")]
        public void С5_RecovereDeletedItemInCartPage()
        {
            GF.ClickOn(PPattern.ObuvButton, "прямую до обувь и сумки");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(PItem.ViewedProductsSliderItem, "натискаю на перший товар в слайдері переглянуті товари");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.ClickOn(PPattern.GoToCartButton, "в попапі натискаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton, "Перехожу на этап Корзина");
            GF.ClickOn(PCart.DeleteItemButton, "видаляю товар з корзини");
            GF.ClickOn(PCart.ReestablishItemButton, "відновлюю товар в корзині");
            Assert.IsTrue(PCart.ItemInCartPage.Displayed, "Товар успішно відновлено");
        }
    }
}
