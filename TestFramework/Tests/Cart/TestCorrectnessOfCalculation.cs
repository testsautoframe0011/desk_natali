﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.Cart
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Корзина"), Property("TestSuite:", "C")]
    class TestCorrectnessOfCalculation<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Vanya Demenko"), Description("Проверка корректности расчёта доставки в корзине, доставка бесплатная")]
        public void C1_CalculationOfFreeDelivery()
        {   
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            GF.ClickOn(PPattern.BazovyyOfisButton, "натискаю на Базовий офіс");
            //filter
            GF.CheckAction(PCatalog.ProductItemList[0].Displayed);
            //var old = PCatalog.ProductItemList[0].Text;
            GF.ClickOn(PCatalog.CatalogSortSelectRight);
            GF.ClickOn(PCatalog.UbivanieSelectRight);
            GF.CheckElementNotAttachedToDom(PCatalog.ProductItemList[0]);
            //GF.CheckAction(!PCatalog.ProductItemList[0].Text.Contains(old));

            GF.ClickOn(PCatalog.ProductItemList[1], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            GF.ClickOn(PPattern.CartCloseButton);
            GF.ClickOn(GXP.ItemSizeSecondOfList, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            GF.ClickOn(PPattern.GoToCartButton, "в попапі натискаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton, "Перехожу на этап Корзина");
            Assert.IsTrue(!PCart.DeliveryTextNotVisiable(), "Текст про безкоштовну доставку висвітлено");
        }


        //Depricated - в корзине теперь не отображается
/*        [Test]
        
        [Author("Vanya Demenko, Dmytro Lytovchenko"), Description("Проверка корректности расчета доставки в корзине, сообщение о бесплатной доставке не видно, доставка платная")]
        public void C2_CalculationOfPaidDelivery()
        {
            GF.ClickOn(PPattern.ObuvButton, "натискаю на Обувь и сумки");

            GF.CheckAction(PCatalog.ProductItemList[0].Displayed);
            var old = PCatalog.ProductItemList[0].Text;
            var oldEl = PCatalog.ProductItemList[0];
            GF.ClickOn(PCatalog.CatalogSortSelectRight);
            GF.ClickOn(PCatalog.VozrastanieSelectRight);
            //Thread.Sleep(1000);
            GF.CheckElementNotAttachedToDom(oldEl);
            GF.CheckAction(!PCatalog.ProductItemList[0].Text.Contains(old));

            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            GF.ClickOn(PPattern.GoToCartButton, "в попапі натискаю на кнопку Перейти в корзину");
            GF.CheckAction(PCart.CartDeliverMenu.Displayed, "Жду загрузки корзины");
            Assert.IsTrue(PCart.DeliveryTextNotVisiable(), "Текст про безкоштовну доставку не висвітлено");

            Console.WriteLine("Нет сообщения о доставке");
        }*/

        [Test]
        [Author("Vanya Demenko"), Description("Проверка корректности расчёта суммы в корзины")]
        public void C3_CheckSummOfTwoItemsInCartPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            GF.ClickOn(PPattern.NovinkiButton);
            GF.ClickOn(PCatalog.ProductItemList[0]);
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");

            GF.WaitForClickabilityOfElement(PPattern.CartCloseButton);
            GF.ClickOn(PPattern.CartCloseButton);

            GF.MoveToElement(PPattern.CatalogButton, "прямую до каталога");
            GF.ClickOn(PPattern.TrikotazhButton, "натискаю на Трикотаж");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            GF.ClickOn(PPattern.GoToCartButton, "в попапі натискаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton, "Перехожу на этап Корзина");
            var final = (PCart.GetNumberPriceINT(PCart.PriceOfFirstItem) + PCart.GetNumberPriceINT(PCart.PriceOfSecondItem)); //Додаю суми товарів
            Assert.AreEqual(final, PCart.GetNumberPriceINT(PCart.GeneralPrice), "Вірний підрахунок суми");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Проверка корректности расчёта суммы в Малой корзине")]
        public void C4_CheckSummOfTwoItemsMiniCart()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            GF.ClickOn(PPattern.NovinkiButton);
            GF.ClickOn(PCatalog.ProductItemList[0]);
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");

            GF.ClickOn(PPattern.CartCloseButton);

            GF.MoveToElement(PPattern.CatalogButton, "прямую до каталога");
            GF.ClickOn(PPattern.SaleButton, "натискаю на Sale");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");

            var final = (PCart.GetNumberPriceINT(PPattern.PriceFirstItemMiniCart) + PCart.GetNumberPriceINT(PCart.PriceSecondItemMiniCart)); //Додаю суми товарів
            Assert.AreEqual(final, PCart.GetNumberPriceINT(PPattern.GeneralPriceMiniCart), "Вірний підрахунок суми");
        }
    }
}
