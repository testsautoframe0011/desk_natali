﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.Cart
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Корзина"), Property("TestSuite:", "С")]
    class TestSizeAndAmount<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Vanya Demenko"), Description("Изменение размера товара в корзине")]
        public void С1_ChangeSizeOfItemInCartPage()
        {
            GF.ClickOn(PPattern.NovinkiButton, "натискаю на новинки");
            GF.MoveToElement(PCatalog.ProductItemList[1], "рухуюся до товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[1], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            GF.CheckAction(GXP.SizesActiveQuickView.Displayed, "Цвет выбран");
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");

            GF.ClickOn(GXP.CloseQuickViewButton);
            GF.ClickOn(PPattern.CartButton);

            GF.ClickOn(PPattern.GoToCartButton, "в попапі натискаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton, "Перехожу на этап Корзина");
            GF.CheckAction(PCart.CartDeliverMenu.Displayed, "Жду меню корзины");
            var sizeText = GF.GetTextOfElement(PCart.SizeOfItem);//записую розмір товара

            GF.DropDownClick(PCart.DropDownSize, 1, "Меняю размер товара");

            Assert.AreNotEqual(GF.GetTextOfElement(PCart.SizeOfItem), sizeText, "Розмір успішно змінений");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Проверка выбраного размера в корзине")]
        public void С2_CheckSelectedSizeInCartPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            GF.MoveToElement(PPattern.CatalogButton, "иду к Каталогу");
            GF.ClickOn(PPattern.BluzyITunikiButton, "нажимаю на Блузы и туники");
            GF.MoveToElement(PCatalog.ProductItemList[0], "направляюсь к товару");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            var sizeText = GF.GetTextOfElement(GXP.TextofSizeItem);//записую розмір товара
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            GF.ClickOn(PPattern.GoToCartButton, "в попапі натискаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton, "Перехожу на этап Корзина");
            Assert.AreEqual(GF.GetTextOfElement(PCart.SizeOfItem), sizeText, "Розмір успішно обраний");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Изменение количества товара в корзине")]
        public void С3_ChangeAmountOfItemInCartPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");

            GF.ClickOn(PPattern.BazovyyOfisButton, "прямую до базового офісу");
            GF.ClickOn(PCatalog.ProductItemList[1], "натискаю на другий товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            GF.ClickOn(PPattern.GoToCartButton, "в попапі натискаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton, "Перехожу на этап Корзина");
            GF.CheckAction(PCart.ItemInCartPage.Displayed, "Жду отображения товаров в Корзине");
            var amountText = GF.GetTextOfElement(PCart.AmountOfItem);//записую кількість товара

            GF.DropDownClick(PCart.DropDownQuantity, 1, "Меняю размер товара");
            Assert.AreNotEqual(PCart.AmountOfItem.Text, amountText, "Кількість успішно змінений");
        }
    }
}
