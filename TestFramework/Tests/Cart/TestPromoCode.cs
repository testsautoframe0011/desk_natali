﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.Cart
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Корзина"), Property("TestSuite:", "PR")]
    class TestPromoCode<TBrowser> : TestBase<TBrowser>
    {
        /// <summary>
        /// https://natalibolgar.com/bitrix/admin/sale_discount_coupons.php?lang=ru - промокоды на Натали
        /// Купон - Promo-To-Test
        /// </summary>
        readonly string PromoCodeAuthorized = "Promo-To-Test";
        readonly string PromoCodeUnAuthorized = "Promo-To-Test-UnAuthorized";
        readonly string IncorrectPromoCode = "3243423432";

        [Test]
        [Author("Vanya Demenko"), Description("Ввод неверного промо-кода на странице корзина")]
        public void PR1_WrongPromoCodeInCartPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Авторизация");
            PCart.DeleteAllItemsFromCartAuth("Удаление товаров с Корзины");
            GF.MoveToElement(PPattern.CatalogButton, "переходжу до Каталога");
            GF.ClickOn(PPattern.KostyumyButton, "натискаю на Костюми");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            GF.ClickOn(PPattern.GoToCartButton, "в попапі натискаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton, "Перехожу на этап Корзина");
            GF.JavaScriptClick(PCart.PromoCodeButton, "натискаю на промо-код");
            GF.SendKeys(PCart.PromoCodeInput, IncorrectPromoCode, "вводжу промо-код");
            GF.ClickOn(PCart.PromoCodeConfirmButton, "натискаю на ОК");
            Thread.Sleep(1000);
            Assert.IsTrue(PCart.PromoTextBasket.Text.Contains(IncorrectPromoCode), "Промо-код успішно доданий");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Ввод неверного промо-кода на чекауте")]
        public void PR2_WrongPromoCodeCheckOut()
        {
            GF.MoveToElement(PPattern.CatalogButton, "Направляюсь к Каталогу");
            GF.ClickOn(PPattern.KostyumyButton, "Выбираю Костюмы");
            GF.MoveToElement(PCatalog.ProductItemList[0], "Направляюсь к первому товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[0], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            GF.CheckAction(GXP.SizesActiveQuickView.Displayed, "Цвет выбран");
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");
            GF.ClickOn(PPattern.CartButton);

            GF.NavigateToUrl(PCheck.Url, "Перехожу на страницу Чекаута");
            GF.ClickOn(PCheck.FirstBuyButton,"Нажимаю Покупаю впервые");

            PCheck.EnterCoupon(IncorrectPromoCode, "Ввожу некорректный промокод");

            Assert.IsTrue(GF.CheckNotDisplayed(PCheck.DiscountAmountSelector), "Суммка скидки не изменилась");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Ввод верного промокода в корзине, отображение скидки на чекауте")]
        public void PR3_CorrectPromoCodeFromCartToCheckout()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Авторизация");
            PCart.DeleteAllItemsFromCartAuth("Удаление товаров с Корзины");

            GF.ClickOn(PPattern.NovinkiButton, "Нажимаю на Новинки");
            GF.MoveToElement(PCatalog.ProductItemList[1], "Направляюсь к первому товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[1], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            GF.CheckAction(GXP.SizesActiveQuickView.Displayed, "Цвет выбран");
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");
            GF.ClickOn(PPattern.CartButton, "Открываю попАп Корзины");

            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду поАпа корзины");
            GF.ClickOn(PPattern.GoToCartButton, "В попапі нажимаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton, "Перехожу на этап Корзина");
            GF.ClickOn(PCart.PromoCodeButton, "Нажимаю на промо-код");
            GF.SendKeys(PCart.PromoCodeInput, PromoCodeUnAuthorized, "Ввод промо-кода");
            GF.ClickOn(PCart.PromoCodeConfirmButton, "Нажимаю ОК");
            Thread.Sleep(2000);
            GF.CheckAction(PCart.Discount.Displayed, "Промокод задействован");
            var beforeCode = GF.GetTextOfElement(PCart.Discount, "Запись скидки");
            GF.ClickOn(PCart.MakeOrderButtonCartPage, "Нажимаю на Офрормить заказ");

            Thread.Sleep(2000);

            GF.CheckAction(PCheck.ItemCheckOutFeild.Displayed, "Жду перехода на страницу чекаута");

            Assert.IsTrue(PCheck.DiscountAmount.Text.Contains(beforeCode), "Промо-код активен на странице чекаута");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Скидка промокода влияет на общую сумму")]
        public void PR4_PromoCodeDiscountWork()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Авторизация");
            PCart.DeleteAllItemsFromCartAuth("Удаление товаров с Корзины");
            GF.NavigateToUrl(PCatalog.NovinkiUrl, "Перехожу на страницу Новинки");
            GF.MoveToElement(PCatalog.ProductItemList[1], "Направляюсь к товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[1], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            GF.CheckAction(GXP.SizesActiveQuickView.Displayed, "Цвет выбран");
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");
            GF.ClickOn(PPattern.CartButton, "Открываю попАп Корзины");

            GF.NavigateToUrl(PCart.Url, "Перехожу на страницу Корзины");
            var priceBefore = PCart.GetNumberPriceINT(PCart.GeneralPrice);
            GF.ClickOn(PCart.PromoCodeButton, "Нажимаю на промо-код");
            GF.SendKeys(PCart.PromoCodeInput, PromoCodeAuthorized, "Ввод промо-кода");
            GF.ClickOn(PCart.PromoCodeConfirmButton, "Нажимаю ОК");

            Thread.Sleep(2000);
            var discount = PCart.GetNumberPriceINT(PCart.Discount);
            var priceAfter = PCart.GetNumberPriceINT(PCart.GeneralPrice);
            Assert.AreEqual((priceBefore - discount), priceAfter, "Скидка применена");
        }
    }
}