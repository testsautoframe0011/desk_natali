﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.Cart
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Корзина"), Property("TestSuite:", "С")]
    class TestGoTo<TBrowser> : TestBase<TBrowser>
    {

        //new mini CART

/*        [Test]
        [Author("Vanya Demenko"), Description("Переход к оформлению заказа с попапа корзины, пользователь авторизирован")]
        public void С1_GoToOrderPageFromCart()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            GF.MoveToElement(PPattern.CatalogButton, "Направляюсь к Каталогу");
            GF.ClickOn(PPattern.PlatyaButton, "Выбираю Платья");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");

            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            GF.ClickOn(PPattern.MakeOrderButton, "в попапі натискаю на кнопку Оформить покупку");
            Assert.IsTrue(PCheck.ItemBasketInOrderPage.Displayed, "Успішно перейшли до сторінки замовлення");
        }*/

        [Test]
        [Author("Vanya Demenko"), Description("Переход к оформлению заказа с корзины, пользователь неавторизирован")]
        public void С2_GoToOrderPageFromCartPage()
        {
            GF.MoveToElement(PPattern.CatalogButton, "прямую до каталогу");
            GF.ClickOn(PPattern.ZhaketyIZhiletyButton, "натискаю на жакеты и жилеты");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            GF.ClickOn(PPattern.GoToCartButton, "в попапі натискаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton, "Перехожу на этап Корзина");
            GF.ClickOn(PCart.OrderCheckoutButton, "Перехожу на этап Чекаут");
            Assert.IsTrue(PCheck.ItemBasketInOrderPage.Displayed, "Успішно перейшли до сторінки замовлення");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Переход к оформлению заказа с корзины профиля, авторизирован")]
        public void С3_GoToOrderPageFromCartPageProfile()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            GF.MoveToElement(PPattern.CatalogButton, "прямую до каталогу");
            GF.ClickOn(PPattern.AksessuaryButton, "натискаю на аксессуари");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            GF.NavigateToUrl(PProf.Url, "переходжу в профіль");
            GF.ClickOn(PProf.CartProfileButton, "натискаю на корзина");
            GF.ClickOn(PProf.MakeOrderButtonCartPageProfile, "натиснути на Оформить покупку");
            GF.ClickOn(PCart.MakeOrderButtonCartPage, "натиснути перейти к оформленню");
            Assert.IsTrue(PCheck.ItemBasketInOrderPage.Displayed, "Успішно перейшли до сторінки замовлення");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Переход на страницу товара со страницы корзины")]
        public void С4_GoToItemPageFromCardPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            GF.MoveToElement(PPattern.CatalogButton, "прямую до каталогу");
            GF.ClickOn(PPattern.PlatyaButton, "натискаю на платья");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа корзины");
            GF.ClickOn(PPattern.GoToCartButton, "в попапі натискаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton, "Перехожу на этап Корзина");
            var NameofItem = GF.GetTextOfElement(PCart.TitleOfItemInCartPage);//записую назву товара
            GF.ClickOn(PCart.ItemInCartPage, "переходжу на сторінку товара");
            Assert.AreEqual(GF.GetTextOfElement(PItem.NameOfItemInPageItem), NameofItem, "Успішно перейшли на сторінку товара, що знаходиться в корзині");
        }
        
        [Test]
        [Author("Vanya Demenko"), Description("Переход на страницу товара со страницы корзины профиля")]
        public void С4_GoToItemPageFromCardPageProfile()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            GF.MoveToElement(PPattern.CatalogButton, "прямую до каталогу");
            GF.ClickOn(PPattern.TopyButton, "натискаю на топи");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            GF.CheckAction(PPattern.FirstElementInCartPopup.Displayed, "Чекаю, щоб елемент додався до корзини");
            GF.ClickOn(PPattern.CartCloseButton,"Закрываю Корзину");
            GF.ClickOn(PPattern.ProfileButton, "переходжу на профіль");
            GF.ClickOn(PProf.CartProfileButton, "натискаю на корзина");
            var NameofItem = GF.GetTextOfElement(PProf.TitleOfItemInCartPageProfile);//записую назву товара
            GF.ClickOn(PProf.TitleOfItemInCartPageProfile, "переходжу на сторінку товара");
            Assert.AreEqual((GF.GetTextOfElement(PItem.NameOfItemInPageItem)).ToUpper(), NameofItem, "Успішно перейшли на сторінку товара, що знаходиться в корзині");
        }
    }
}