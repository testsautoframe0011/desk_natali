﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.Cart
{
    
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Корзина"), Property("TestSuite:", "С")]
    class TestAddToCart<TBrowser> : TestBase<TBrowser>
    {
        private readonly string SearchText = "Юбка";
        private readonly string IncorrectSearchText = "абвг";


        [Test]
        [Author("Vanya Demenko"), Description("Добавление товара в малую корзину через страницу товара")]
        public void С1_AddToCartFromPageItem()
        {
            GF.MoveToElement(PPattern.CatalogButton, "прямую до каталога");
            GF.ClickOn(PPattern.KostyumyButton, "натискаю на костюми");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            Assert.IsTrue(PPattern.FirstElementInCartPopup.Displayed, "Товар успішно доданий в попап корзини");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Добавление товара в корзину через Быстрый Просмотр")]
        public void С2_AddToCartPageFromQuickView()
        {
            GF.ClickOn(PPattern.NovinkiButton, "натискаю на новинки");
            GF.MoveToElement(PCatalog.ProductItemList[1], "рухуюся до товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[1], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0],"Выбираю первый размер");
            Thread.Sleep(500);
            
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");
            Thread.Sleep(1000);
            GF.ClickOn(PPattern.CartButton,"открываю малую Корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed);

            GF.ClickOn(PPattern.GoToCartButton, "в попапі натискаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton,"Перехожу на этап Корзина");
            Assert.IsTrue(PCart.ItemInCartPage.Displayed, "Товар успішно доданий в корзину");
        }

        [Test]
        [TestCase]
        [Author("Vanya Demenko"), Description("Добавление товара в корзину c листа желаний через Быстрый Просмотр")]
        public void С3_AddItemToCartPageFromQuickViewInWishList()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            PProf.DeleteAllItemsFromWishListAuth("видаляю всі товари з листа бажань");
            GF.ClickOn(PPattern.BazovyyOfisButton, "натискаю на Базовий офіс");
            GF.ClickOn(PCatalog.AddToWishListButtonOfSecondItem, "додаю до листа бажань");
            GF.ClickOn(PPattern.ProfileButton, "переходжу на профіль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду загрузки меню профиля");

            GF.ClickOn(PProf.WishListButton, "натискаю на лист бажань");
            GF.MoveToElement(PProf.FirstItemInWishList, "рухуюся до товару");

            GF.ClickOn(PCatalog.QuickViewOfFirstItemWishList, "натискаю на Быстрый Просмотр");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");


            GF.ClickOn(PPattern.GoToCartButton, "в попапі натискаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton, "Перехожу на этап Корзина");
            Assert.IsTrue(PCart.ItemInCartPage.Displayed, "Товар успішно доданий в корзину");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Добавление товара в корзину профиля через страница поиска Быстрый Просмотр")]
        public void С4_AddItemToCartPageProfileFromSearchPageByQuickView()
        {
            Console.WriteLine("Товар не добавляется в ЛИСТ ЖЕЛАНИЙ через КАТАЛОГ, БП, в НОВОМ КАТАЛОГЕ");
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            GF.ClickOn(PPattern.SearchButton, "натискаю на пошук");
            GF.SendKeys(PPattern.SearchInput, SearchText, "ввожу текст Юбки");
            GF.ClickOn(PPattern.GoSearchButton, "здійснюю пошук");
            GF.MoveToElement(PCatalog.ProductItemList[0], "рухуюся до товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[0], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            Thread.Sleep(300);
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");
            Thread.Sleep(2000);

            GF.ClickOn(PPattern.ProfileButton, "переходжу на профіль");
            GF.ClickOn(PProf.CartProfileButton, "натискаю на корзина");
            Assert.IsTrue(PProf.CartPrifileItem.Displayed, "Товар успішно доданий в корзину профіля");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Добавление товара в корзину профиля через страницу товара перейдя в нее через слайдер (просмотренные товары)")]
        public void С5_AddItemToCartPageProfileFromPageItemBySlider()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            GF.ClickOn(PPattern.ObuvButton, "прямую до обувь и сумки");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.CheckAction(PItem.ViewedProductsSliderItem.Displayed, "Жду отображение блока Просмотренные товары ");
            GF.MoveToElement(PItem.ViewedProductsSliderItem, "иду к перший товар в слайдері переглянуті товари");

            //OLDqv
            GF.ClickOn(GXP.QuickViewViewedItem, "Нажимаю на Быстрый Просмотр просмотреных товаров");
            GF.ClickOn(GXP.ItemSizeQuickView, "Нажимаю на размер");
            GF.ClickOn(GXP.BuyButtonOLDQuickView, "Нажимаю кнопку Купить Q");
            Thread.Sleep(1000);
            GF.ClickOn(PPattern.CartCloseButton, "Закрываю малую корзину");

            GF.ClickOn(PPattern.ProfileButton, "переходжу на профіль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду загрузку профиля", 15);
            GF.ClickOn(PProf.CartProfileButton, "натискаю на корзина");
            Assert.IsTrue(PProf.CartPrifileItem.Displayed, "Товар успішно доданий в корзину профіля");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Добавление товара в попап корзины через образ")]
        public void С6_AddItemToCartFromObraz()
        {
            GF.NavigateToUrl(POficeCollect.Url, "Перехожу к коллекциям");
            GF.CheckAction(POficeCollect.ViewFirstObrazVesna21Button.Displayed, "жду загрузки трендов", 20);
            GF.WaitForClickabilityOfElement(POficeCollect.ViewFirstObrazVesna21Button);
            Thread.Sleep(2000);
            GF.ClickOn(POficeCollect.ViewFirstObrazVesna21Button, "відкриваю перший образ");
            GF.ClickOn(GXP.ViewItemInObrazPopup[0], "натискаю на товар з образу");
            GF.ClickOn(GXP.ItemSize, "обираю розмір");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            Assert.IsTrue(PPattern.FirstElementInCartPopup.Displayed, "Товар успішно доданий в попап корзини");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в попап корзины через StickyBar")]
        public void С7_AddItemToCartFromStickyBar()
        {
            GF.MoveToElement(PPattern.CatalogButton, "направляюсь к Каталогу");
            GF.ClickOn(PPattern.GolfySvitshotyButton, "нажимаю на Гольфы и Свитшоты");
            GF.ClickOn(PCatalog.ProductItemList[1], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.ItemSize, "выбираю размер");
            GF.MoveToElement(PItem.NataliFooterLogo, "направляюсь к кнопке Оставить отзыв");
            GF.WaitForVisibility(PItem.NataliFooterLogoSelector, "Жду появления отзывов");
            GF.CheckAction(PItem.AddItemFromStickyBarButton.Displayed, "Жду отображение StickyBar");
            GF.ClickOn(PItem.AddItemFromStickyBarButton, "Нажимаю добавить в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа корзины");
            Assert.IsTrue(PPattern.FirstElementInCartPopup.Displayed, "Товар добавлен в корзину");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в корзину через Дополнить образ")]
        public void С8_AddItemToCartPageFromСomplementTheImage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Авторизация");
            PCart.DeleteAllItemsFromCartAuth("Удаление товаров с корзины");
            GF.MoveToElement(PPattern.CatalogButton, "Направляюсь к Каталогу");
            GF.ClickOn(PPattern.TopyButton, "Выбираю Топы");
            GF.CheckAction(PCatalog.ProductItemList[1].Displayed, "Жду отображение товаров");
            PCatalog.FindAbilityToСomplementImage();
            GF.ClickOn(PItem.СomplementTheImage, "Открываю дополнить образ");
            GF.ClickOn(GXP.TextOfFirstItemInObrazPopup, "Открываю первый товар из образа");
            GF.ClickOn(GXP.ItemSize, "Выбираю размер");
            GF.ClickOn(GXP.BuyButton, "Добавляю в корзину");
            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.ClickOn(PPattern.GoToCartButton, "В попАпе нажимаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton, "Перехожу на этап Корзина");
            Assert.IsTrue(PCart.TitleOfItemInCartPage.Displayed, "Товар добавлен в корзину");
        }



        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в Корзину через Новые поступления, при неудачном поиске")]
        public void C9_AddItemCartByNewArrivals()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "авторизация пользователя");
            PCart.DeleteAllItemsFromCartAuth("Удаление товаров с корзины");
            GF.ClickOn(PPattern.SearchButton, "Нажимаю на поиск");
            GF.SendKeys(PPattern.SearchInput, IncorrectSearchText, "Ввожу неправильный запрос");
            GF.ClickOn(PPattern.GoSearchButton, "выполняю поиск");
            GF.CheckAction(PPattern.NewArrivalsImg.Displayed, "ожидаю Новые Поступления");
            GF.MoveToElement(PCatalog.ProductItemList[0], "Направляюсь к товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[0], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.SizesQuickView[0], "Выбираю первый размер");
            Thread.Sleep(300);
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");
            Thread.Sleep(2000);

            GF.ClickOn(PPattern.ProfileButton, "переходжу в профиль",15);
            GF.CheckAction(PProf.ProfileMenu.Displayed, "жду меню профиля");
            GF.ClickOn(PProf.CartProfileButton, "нажимаю на корзина");
            Assert.IsTrue(PProf.CartPrifileItem.Displayed, "Товар добавлен в лист желаний");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в Корзину через попАп Образов")]
        public void C10_AddItemCartByObraz()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "авторизация пользователя");
            PCart.DeleteAllItemsFromCartAuth("Удаление товаров с корзины");
            GF.MoveToElement(PPattern.CatalogButton, "Направляюсь к Каталогу");
            GF.ClickOn(PPattern.TopyButton, "Выбираю Топы");
            GF.CheckAction(PCatalog.ProductItemList[1].Displayed, "Жду отображение товаров");
            PCatalog.FindAbilityToСomplementImage();
            GF.ClickOn(PItem.СomplementTheImage, "Открываю дополнить образ");


            GF.MoveToElement(GXP.ObrazImg, "Навожусь на образы");
            GF.MoveToElement(GXP.ObrazBuyButton,"Навожусь на кнопку КУпить");
            GF.ClickOn(GXP.ObrazSizesBuy, "Добавляю товар с образа в Коризну");
            GF.ClickOn(GXP.CloseObrazPopupButton, "Закрываю попап");
            GF.ClickOn(PPattern.CartButton, "Открываю малую Корзину");


            GF.CheckAction(PPattern.OpenedCartPopUp.Displayed, "Жду отображение попАпа Корзины");
            GF.ClickOn(PPattern.GoToCartButton, "В попАпе нажимаю на кнопку Перейти в корзину");
            GF.ClickOn(PCheck.CheckoutToCartButton,"Перехожу в Корзину");
            Assert.IsTrue(PCart.ItemInCartPage.Displayed, "Товар добавлен в корзину");
        }
    }
}
