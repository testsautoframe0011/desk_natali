﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.Cart
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Корзина"), Property("TestSuite:", "С")]
    class TestErrorSizeAddingToCart<TBrowser> : TestBase<TBrowser>
    {
        private readonly string ErrorMessageSizeNotChoose = "Не выбран";
        private readonly string ErrorMessageSizeNotChoosePAgeItem = "Не выбран. Выберите, пожалуйста, размер";
        private readonly string SearchText = "Юбка";



        [Test]
        [Author("Vanya Demenko"), Description("Невозможность добавление товара в корзину без выбранного размера на странице товара")]
        public void С1_AddToCartFromPageItemWithOutSize()
        {
            GF.MoveToElement(PPattern.CatalogButton, "прямую до каталога");
            GF.ClickOn(PPattern.KostyumyButton, "натискаю на костюми");
            GF.ClickOn(PCatalog.ProductItemList[0], "натискаю на перший товар на сторінці");
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            Thread.Sleep(1500);
            Assert.AreEqual(GF.GetTextOfElement(PItem.Errore_Size_Message), ErrorMessageSizeNotChoosePAgeItem, "Неможливо додати товар без обраного розміру");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Невозможность добавление товара в корзину без выбранного размера через Быстрый Просмотр")]
        public void С2_AddToCartFromQuickViewWithOutSize()
        {
            GF.ClickOn(PPattern.NovinkiButton, "натискаю на новинки");
            GF.MoveToElement(PCatalog.ProductItemList[1], "рухуюся до товару");
            GF.ClickOn(GXP.QuickViewOfItemList[1], "натискаю на Быстрый Просмотр");
            GF.WaitForClickabilityOfElement(GXP.BuyButtonQuickView, 10);
            GF.ClickOn(GXP.BuyButtonQuickView, "натискаю кнопку Купити");
            Assert.AreEqual(GF.GetTextOfElement(PCatalog.ErroreSizeMessage), ErrorMessageSizeNotChoose, "неможливо додати товар без обраного розміру");
        }

        [Test]
        [Author("Vanya Demenko, Dmytro Lytovchenko"), Description("Невозможность добавление товара в корзину без выбранного размера через Быстрый Просмотр лист желаний")]
        public void С3_AddToCartFromQuickViewInWishListWithOutSize()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизовується");
            PCart.DeleteAllItemsFromCartAuth("видаляю всі товари з корзини");
            PProf.DeleteAllItemsFromWishListAuth("видаляю всі товари з листа бажань");
            GF.ClickOn(PPattern.BazovyyOfisButton, "натискаю на Базовий офіс");
            GF.ClickOn(PCatalog.AddToWishListButtonOfFirstItem, "додаю до листа бажань");
            GF.ClickOn(PPattern.ProfileButton, "переходжу на профіль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "натискаю на лист бажань");
            GF.MoveToElement(PProf.FirstItemInWishList, "рухуюся до товару");
            GF.ClickOn(PCatalog.QuickViewOfFirstItemWishList, "натискаю на Быстрый Просмотр");
            GF.WaitForClickabilityOfElement(GXP.BuyButton, 10);
            GF.ClickOn(GXP.BuyButton, "натискаю кнопку Купити");
            Assert.IsTrue(GF.GetTextOfElement(PItem.Errore_Size_Message).Contains(ErrorMessageSizeNotChoose), "неможливо додати товар без обраного розміру");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Невозможность добавление товара в корзину без выбранного размера через Быстрый Просмотр страница поиска")]
        public void С4_AddToCartFromSearchPageByQuickViewWithOutSize()
        {
            GF.ClickOn(PPattern.SearchButton, "натискаю на пошук");
            GF.SendKeys(PPattern.SearchInput, SearchText, "ввожу текст");
            GF.ClickOn(PPattern.GoSearchButton, "здійснюю пошук");
            GF.MoveToElement(PCatalog.ProductItemList[0], "рухуюся до товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[0], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.BuyButtonQuickView, "Нажимаю кнопку Купить Q");
            Assert.IsTrue(PCatalog.ErroreSizeMessage.Displayed, "неможливо додати товар без обраного розміру");
        }
    }
}
