﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.WishList
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Лист Желаний"), Property("TestSuite:", "WL")]
    class TestDeleteFromWishList<TBrowser> : TestBase<TBrowser>
    {
        [Test]
        [Author("Vanya Demenko"), Description("Удаление товара из листа желаний")]
        public void WL1_DeleteItemFromWishList()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PProf.DeleteAllItemsFromWishListAuth("видаляю всі товари з листа бажань");
            GF.MoveToElement(PPattern.SaleButton, "прямую до Sale");
            GF.ClickOn(PPattern.KostyumySaleButton, "натискаю на Костюмы");
            GF.ClickOn(PCatalog.ProductItemList[1], "натискаю на перший товар на сторінці");
            GF.ClickOn(PItem.AddToWishListButtonPegeItem, "додаю до листа бажань");
            GF.CheckAction(PItem.ItemIsAddedToWishListPegeItem.Displayed, "чекаю додавання елемента до листа",12);
            GF.ClickOn(PPattern.ProfileButton, "переходжу на профіль");
            GF.ClickOn(PProf.WishListButton, "натискаю на лист бажань");
            GF.ClickOn(PProf.DeleteFromWishListButton, "видаляю товар з листа бажань");
            Assert.IsTrue(GF.CheckElementNotVisible(PProf.FirstItemInWishList), "Товар успішно видалений з листа бажань");
        }
         
        [Test]
        [Author("Dmytro Lytovchenko"), Description("Удаление товара из листа желаний через каталог")]
        public void WL2_DeleteItemFromWishListFromCatalog()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "авторизация");
            PProf.DeleteAllItemsFromWishListAuth("удаляю все товары с листа желаний");
            GF.ClickOn(PPattern.NovinkiButton, "перехожу в Новинки");
            GF.ClickOn(PCatalog.AddToWishListButtonOfFirstItem, "добавляю первый товар в избранное");
            GF.CheckAction(PCatalog.ItemIsAddedToWishList.Displayed, "жду добавления товара в лист желаний");
            GF.ClickOn(PPattern.ProfileButton, "переходжу в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "нажимаю на лист желаний");
            GF.CheckAction(PProf.DeleteFromWishListButton.Displayed, "проверяю отображения товара в листе желаний");
            GF.ClickOn(PPattern.NovinkiButton, "перехожу в Новинки");
            GF.ClickOn(PCatalog.ItemIsAddedToWishList, "удаляю первый товар из избранного"); 
            GF.ClickOn(PPattern.ProfileButton, "переходжу в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "нажимаю на лист желаний");
            Assert.IsTrue((PProf.EmptyWishListText.Displayed), "Товар успішно видалений з листа бажань");
        }
    }
}
