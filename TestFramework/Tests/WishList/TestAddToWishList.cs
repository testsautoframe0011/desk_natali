﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;

namespace TestFramework.Tests.WishList
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    //[TestFixture(typeof(FirefoxDriver))]
    [Category("Лист Желаний"), Property("TestSuite:", "WL")]
    class TestAddToWishList<TBrowser> : TestBase<TBrowser>
    {
        private readonly string SearchText = "юбка";
        private readonly string IncorrectSearchText = "абвг";
        private readonly int AmountItemsWishlist = 3;

        [Test]
        [Author("Vanya Demenko"), Description("Добавление товара в лист желаний через страницу товара")]
        public void WL1_AddItemToWishListFromPageItem() 
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PProf.DeleteAllItemsFromWishListAuth("видаляю всі товари з листа бажань");
            GF.MoveToElement(PPattern.NashyTrendyButton, "прямую до Наши тренды");
            GF.ClickOn(PPattern.OfficeСollection, "нажимаю на Весна 21");
            GF.CheckAction(POficeCollect.ViewFirstObrazVesna21Button.Displayed, "жду загрузки трендов");
            Thread.Sleep(2000);
            GF.ClickOn(POficeCollect.ViewFirstObrazVesna21Button, "відкриваю перший образ");
            GF.CheckAction(GXP.ViewItemInObrazPopup[0].Displayed, "жду загрузки попАпа трендов");
            GF.ClickOn(GXP.ViewItemInObrazPopup[0], "натискаю на товар з образу");
            GF.ClickOn(PItem.AddToWishListButtonPegeItem, "додаю до листа бажань");
            GF.CheckAction(PItem.ItemIsAddedToWishListPegeItem.Displayed, "чекаю додавання елемента до листа");
            GF.ClickOn(PPattern.ProfileButton, "переходжу на профіль");
            GF.CheckAction(PProf.WishListButton.Displayed, "Жду отображения меню профиля", 15);
            GF.ClickOn(PProf.WishListButton, "натискаю на лист бажань");
            Assert.IsTrue(PProf.FirstItemInWishList.Displayed, "Товар успішно доданий до листа бажань");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Добавление товара в лист желаний через каталог товаров")]
        public void WL2_AddItemToWishListByCatalogPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PProf.DeleteAllItemsFromWishListAuth("видаляю всі товари з листа бажань");
            GF.MoveToElement(PPattern.SaleButton, "прямую до Sale");
            GF.ClickOn(PPattern.PlatyaSaleButton, "натискаю на Платья");
            GF.ClickOn(PCatalog.AddToWishListButtonOfFirstItem, "додаю до листа бажань");
            GF.ClickOn(PPattern.ProfileButton, "переходжу на профіль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "натискаю на лист бажань");
            Assert.IsTrue(PProf.FirstItemInWishList.Displayed, "Товар успішно доданий до листа бажань");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Добавление товара в лист желаний через Быстрый Просмотр")]
        public void WL3_AddItemToWishListByQuickView()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PProf.DeleteAllItemsFromWishListAuth("видаляю всі товари з листа бажань");
            GF.MoveToElement(PPattern.CatalogButton, "прямую до каталога");
            GF.ClickOn(PPattern.AksessuaryButton, "натискаю на Аксессуары ");
            GF.MoveToElement(PCatalog.ProductItemList[0], "рухуюся до товару");

            //qv
            GF.ClickOn(GXP.QuickViewOfItemList[0], "Нажимаю на Быстрый Просмотр");
            GF.ClickOn(GXP.AddToWishListButtonQuickView, "Нажимаю кнопку добавить в  Лист желаний");
            GF.CheckAction(GXP.ItemInFavQuickView.Displayed, "Товар добавлен в Лист желаний");
            GF.ClickOn(GXP.CloseQuickViewButton, "Закрываю Быстрый просмотр");

            GF.ClickOn(PPattern.ProfileButton, "переходжу на профіль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "натискаю на лист бажань");
            Assert.IsTrue(PProf.FirstItemInWishList.Displayed, "Товар успішно доданий до листа бажань");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Добавление нескольких товаров в лист желаний")]
        public void WL4_AddSeveralItemsToWishListByCatalogPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PProf.DeleteAllItemsFromWishListAuth("видаляю всі товари з листа бажань");
            GF.MoveToElement(PPattern.CatalogButton, "переходжу до Каталога");
            GF.ClickOn(PCatalog.PlatyaButton, "натискаю на Платья");
            GF.ClickOn(PCatalog.AddToWishListButtonOfFirstItem, "додаю до листа бажань перший товар");
            GF.ClickOn(PCatalog.AddToWishListButtonOfSecondItem, "додаю до листа бажань другий товар");
            GF.ClickOn(PCatalog.AddToWishListButtonOfThirdItem, "додаю до листа бажань третій товар");
            GF.ClickOn(PPattern.ProfileButton, "переходжу на профіль");
            GF.CheckAction(PProf.WishListButton.Displayed, "Жду отображения меню профиля", 15);
            GF.ClickOn(PProf.WishListButton, "натискаю на лист бажань");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "жду меню профиля");
            Assert.AreEqual((PProf.ItemsInWishList).Count, AmountItemsWishlist, "Товари успішно доданині до листа бажань");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Добавление товара в лист желаний через страницу поиска")]
        public void WL5_AddItemToWishListBySearchPage()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PProf.DeleteAllItemsFromWishListAuth("видаляю всі товари з листа бажань");
            GF.ClickOn(PPattern.SearchButton, "натискаю на пошук");
            GF.SendKeys(PPattern.SearchInput, SearchText, "ввожу текст Юбки");
            GF.ClickOn(PPattern.GoSearchButton, "здійснюю пошук");
            GF.ClickOn(PCatalog.AddToWishListButtonOfFirstItem, "додаю до листа бажань");
            GF.ClickOn(PPattern.ProfileButton, "переходжу на профіль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "натискаю на лист бажань");
            Assert.IsTrue(PProf.FirstItemInWishList.Displayed, "Товар успішно доданий до листа бажань");
        }

        [Test]
        [Author("Vanya Demenko"), Description("Добавление товара в лист желаний через слайдер Просмотренные товары")]
        public void WL6_AddItemToWishListBySliderViewedItems()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "користувач авторизувався");
            PProf.DeleteAllItemsFromWishListAuth("видаляю всі товари з листа бажань");
            GF.MoveToElement(PPattern.SaleButton, "прямую до Sale");
            GF.ClickOn(PPattern.ObuvSaleButton, "натискаю на Обувь");
            GF.ClickOn(PCatalog.ProductItemList[1], "натискаю на перший товар на сторінці");
            GF.ClickOn(PItem.ViewedProductsSliderItemAddToFavorite, "додаю до листа бажань товар із слайдеру");
            GF.ClickOn(PPattern.ProfileButton, "переходжу на профіль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "натискаю на лист бажань");
            Assert.IsTrue(PProf.FirstItemInWishList.Displayed, "Товар успішно доданий до листа бажань");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в лист желаний через Новые поступления, при неудачном поиске")]
        public void WL7_AddItemToWishListByNewArrivals()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "авторизация пользователя");
            PProf.DeleteAllItemsFromWishListAuth("Удаляю все товары с листа желаний");
            GF.ClickOn(PPattern.SearchButton, "Нажимаю на поиск");
            GF.SendKeys(PPattern.SearchInput, IncorrectSearchText, "Ввожу неправильный запрос");
            GF.ClickOn(PPattern.GoSearchButton, "выполняю поиск");
            GF.CheckAction(PPattern.NewArrivalsImg.Displayed,"ожидаю Новые Поступления");
            GF.ClickOn(PPattern.NewArrivalsWishlistButton, "добавляю в лист желаний первый товар из Новых поступлений");
            GF.ClickOn(PPattern.ProfileButton, "переходжу в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "нажимаю на лист желаний");
            Assert.IsTrue(PProf.FirstItemInWishList.Displayed, "Товар добавлен в лист желаний");
        }

        [Test]
        [Author("Dmytro Lytovchenko"), Description("Добавление товара в лист желаний через слайдер Вас может заинтересовать")]
        public void WL8_AddItemToWishListBySliderRecomended()
        {
            PAthrzt.AuthorizationUser(User.UserFirst, "Авторизация");
            PProf.DeleteAllItemsFromWishListAuth("Удаляю все товары с Листа желаний");
            GF.ClickOn(PPattern.BazovyyOfisButton, "Нажимаю к Базовый офис");
            GF.ClickOn(PCatalog.ProductItemList[0], "Нажимаю на первый товар на странице");
            GF.MoveToElement(PItem.RecomendedSliderItemAddToFavorite, "Направляюсь к слайдеру");
            GF.ClickOn(PItem.RecomendedSliderItemAddToFavorite, "Добавляю товар со слайдера Вас может заинтересовать");
            GF.ClickOn(PPattern.ProfileButton, "Перехожу в профиль");
            GF.CheckAction(PProf.ProfileMenu.Displayed, "Жду меню профиля");
            GF.ClickOn(PProf.WishListButton, "Нажимаю на Лист желаний");
            Assert.IsTrue(PProf.FirstItemInWishList.Displayed, "Товар добавлен в лист желаний");
        }
    }
}
