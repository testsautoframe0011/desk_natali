﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework.General;

namespace TestFramework.Pages
{
    public class PageCart : PageBase
    {
        public PageCart() { Url = "order/basket/"; }

        readonly PageProfile profile = new PageProfile();

        public IWebElement OrderCheckoutButton { get => WaitElement(By.XPath("//div[@class='order-steps']/div[2]")); }


        /*//ItemInCart//*/


        public IWebElement ItemInCartPage { get => WaitElement(By.XPath("(//a[@class='basket-item-image-link'])[last()]")); }
        public IWebElement ItemsInCart { get => WaitElement(By.XPath("//tr[@class='basket-items-list-item-container']")); }
        public IWebElement TitleOfItemInCartPage { get => WaitElement(By.XPath("//span[@data-entity='basket-item-name'][last()]")); }

        //DeleteAndReestablish
        public IWebElement DeleteAllItemsButton { get => WaitElement(By.XPath("//a[contains(text(),'Очистить корзину')]")); }
        public IWebElement DeletedtemInCartPage { get => WaitElement(By.XPath("//div[@class='basket-items-list-item-removed-container']")); }
        public IWebElement DeleteItemButton { get => WaitElement(By.XPath("//span[@class='basket-item-actions-remove']")); }
        public IWebElement ReestablishItemButton { get => WaitElement(By.XPath("//a[@data-entity='basket-item-restore-button']")); }

        //Size and Amount
        public IWebElement AmountOfItem { get => WaitElement(By.XPath("//div[contains(@class,'amount')]//div[@class='jq-selectbox__select-text']")); }
        public IWebElement AnotherAmountOfItem { get => WaitElement(By.XPath("//div[contains(@class,'amount')]//li[2]")); }
        public IWebElement AnotherSizeOfItem { get => WaitElement(By.XPath("//div[contains(@class,'sku')]//li[2]")); }
        public IWebElement ChangeAmountOfItem { get => WaitElement(By.XPath("//div[contains(@class,'amount')]//div[@class='jq-selectbox__trigger']")); }
        public IWebElement ChangeSizeOfItem { get => WaitElement(By.XPath("//div[contains(@class,'sku')]//div[@class='jq-selectbox__trigger']")); }
        public IWebElement SizeOfItem { get => WaitElement(By.XPath("(//div[contains(@class,'sku')]//div[@class='jq-selectbox__select-text'])[last()]")); }

        public By DropDownSize = By.XPath("//select[@data-entity='basket-item-sku-select'][1]");       
        public By DropDownQuantity = By.XPath("//div[@data-entity='basket-item-quantity-block']//select[@data-entity='basket-item-sku-select']");

        //DeliverySumPromocode
        public IWebElement DeliveryText { get => WaitElement(By.XPath("(//div[@class='sum-row sum-row__block '])[last()]")); }
        public IWebElement MakeOrderButtonCartPage { get => WaitElement(By.XPath("//button[contains(text(),'Перейти к оформлению')]")); }
        public IWebElement TextOfFreeDelivery { get => WaitElement(By.XPath("//div[contains(@class,'sum-row sum-row__block')]")); }
        public IWebElement PaidDeliveryMessageCheckoutBasket { get => WaitElement(By.XPath("//div[@class='basket-checkout-section-inner']//div[@class='sum-price-name' and contains(text(),'Доставка')]/following-sibling::div")); }
        public IWebElement FreeDeliveryMessageCheckoutBasket { get => WaitElement(By.XPath("//div[@class='basket-checkout-section-inner']//div[@class='sum-price-num'][contains(text(),'Бесплатная доставка')]")); }
        public IWebElement CartDeliverMenu { get => WaitElement(By.XPath("//div[@class='wrapper-sticky']//div[@class='basket-checkout-container']"), 5); }

        //Promo-code
        public IWebElement PromoCodeButton { get => WaitElement(By.XPath("//div[contains(@class,'basket-checkout-block')]//span[@class='show-promo']")); }
        public IWebElement PromoCodeConfirmButton { get => WaitElement(By.XPath("//div[@class='form-group']//span[@class='basket-coupon-block-coupon-btn']")); }
        public IWebElement PromoCodeInput { get => WaitElement(By.XPath("//input[@data-entity='basket-coupon-input']")); }
        public IWebElement PromoCodeItem { get => WaitElement(By.XPath("//div[@class='basket-coupon-alert text-danger']")); }
        public IWebElement Discount { get => WaitElement(By.XPath("//div[@class='sum-row'][2]//div[@class='sum-price-num']")); }
        public IWebElement PromoTextBasket { get => WaitElement(By.XPath("//div[@id='basket-total']//div[@class='basket-coupon-alert-section']//span[@class='basket-coupon-text']/strong")); }
        public List<IWebElement> PromoElementsList { get => MyWaitElements(By.XPath("//div[@id='basket-total']//div[@class='basket-coupon-alert-section']//*")); }
   
        //PriceOfTotal
        public IWebElement GeneralPrice { get => WaitElement(By.XPath("(//div[@data-entity='basket-total-price'])[last()]")); }
        public IWebElement PriceOfFirstItem { get => WaitElement(By.XPath("(//span[@class='basket-item-price-current-text'])[2]")); }
        public IWebElement PriceOfSecondItem { get => WaitElement(By.XPath("(//span[@class='basket-item-price-current-text'])[last()]")); }


        public void DeleteAllItemsFromCartAuth(string description)
        {

            if (!Driver.Url.Contains(URLBuilder.BaseUrl + Url))
            {
                Driver.Navigate().GoToUrl(URLBuilder.BaseUrl + Url);

                Console.WriteLine(Driver.Url);
            }
            Thread.Sleep(2000);
            if (Driver.Url.Contains(URLBuilder.BaseUrl + Url))
            {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
                IWebElement element = null;
                GF.MoveToElement(FooterMenu);
                wait.Until<IWebElement>((d) =>
                {
                    try
                    {
                        IWebElement elem = ItemsInCart;
                        element = DeleteAllItemsButton;
                        return element;
                    }
                    catch (Exception)
                    {
                        IWebElement elementFall = profile.ProfileMenu; //d.FindElement(By.XPath("//div[@class='lk-page']")); 
                        element = null;
                        return element;
                        //return elementFall;
                    }
                });
                if (element != null) element.Click();
            }

        }


        // Exception коли повідомлення, про безкоштовну доставку не висвітлено, бо ціна менше 1500
        public bool DeliveryTextNotVisiable()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement element = null;
            wait.Until<IWebElement>((d) =>
            {
                try
                {
                    element = TextOfFreeDelivery;
                    return element;
                }
                catch (Exception)
                {
                    IWebElement elementFall = ItemsInCart;
                    element = null;
                    elementFall = null;
                    return elementFall;
                }
            });
            if (element != null) return false;
            else return true;
        }

        /// <summary>
        /// Для размеров и количества в Корзине.
        /// </summary>
        /// <param name="dropDown"></param>
        /// <param name="quantity"></param>
        /// <param name="description"></param>
        /// <returns></returns>
/*        public IWebElement SelectSize(By dropDown, int quantity = 1, string description = "")
        {
            SelectElement oSelect = new SelectElement(Driver.FindElement(dropDown));
            IList<IWebElement> elementCount = oSelect.Options;

            if (elementCount.Count < 2)
            {
                Console.WriteLine("Count < 2");
                return null;
            }
            return elementCount.ElementAt(quantity);
        }*/
    }
}
