﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework.General;

namespace TestFramework.Pages
{
    public partial class PageShops : PageBase
    {
        //* DropDown *//
        public IWebElement DropDownCityButton { get => WaitElement(By.XPath("//div[@class='multi-tit cities-list']")); }

        public List<IWebElement> DropDownCityList { get => MyWaitElements(By.XPath("//ul//li//span[@data-city_value]")); }

        //* ShopsInfo *//
        public List<IWebElement> CityListMoreButtons { get => MyWaitElements(By.XPath("//div[contains(@class,'shops-container')]//span[@class='city-name']")); }////span[contains(@class,'shop-city')]/span[1]

        public List<IWebElement> SelectedCityInfo { get => MyWaitElements(By.XPath("//div[contains(@id,'collapse') and @class='panel-collapse collapse in']/div[contains(@class,'row shop-info')]")); }

        public IWebElement ActiveDayInfo { get => WaitElement(By.XPath("//div[@class='panel-collapse collapse in']/div[contains(@class,'row shop-info')][position()<2]//tbody//tr[@class='active']/td[1]")); }
        public List<IWebElement> DayInfoList { get => MyWaitElements(By.XPath("//div[@class='panel-collapse collapse in']/div[contains(@class,'row shop-info')][position()<2]//tbody//tr")); }


        //* MAP *//
        public IWebElement MapBlock { get => WaitElement(By.XPath("(//div[@id='map'])[1]//*")); }

    }

    public partial class PageShops : PageBase
    {

        public bool CheckDay()
        {

            var culture = new CultureInfo("ru-Ru");
            var day = culture.DateTimeFormat.GetDayName(DateTime.Today.DayOfWeek);

            Dictionary<string, string> listofdays = new Dictionary<string, string>()
              {
                 {"Пн","понедельник"},
                 {"Вт","вторник"},
                 {"Ср","среда"},
                 {"Чт","четверг"},
                 {"Пт","пятница"},
                 {"Сб","суббота"},
                 {"Вс","воскресенье"}

            };

            var siteDay = listofdays[ActiveDayInfo.Text];

            return day == siteDay;

        }


        public bool MapChangedCitySelect()
        {
            var ShopMap = MapBlock;

            foreach (var shops in SelectedCityInfo)
            {

                GF.ClickOn(shops);
                var ShopMapSel = MapBlock;
                try
                {
                    if (ShopMap.Displayed)
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is NoSuchElementException && ex is StaleElementReferenceException)
                    { ShopMap = ShopMapSel; }
                }
            }

            return true;
        }
    }
}

