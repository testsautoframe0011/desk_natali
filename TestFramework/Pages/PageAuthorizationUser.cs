﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TestFramework.General;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;



namespace TestFramework.Pages
{
    public partial class User : PageBase
    {

        public static User UserFirst = new User("aniartqa@gmail.com", "qaaniartgmailcom");
        public static User UserFacebook = new User("facebookqaaniart@gmail.com", "facebookqa");



        public User() { Url = ""; }

        public IWebElement AuthErrorMessage { get => WaitElement(By.XPath("//span[contains(text(),'Неверный логин или пароль')]")); }
        public IWebElement AuthorizationPopup { get => WaitElement(By.XPath("(//div[@class='modal-body'])[1]")); }
        public IWebElement EmailInput { get => WaitElement(By.XPath("//input[@name='AUTH-LOGIN']")); }
        public IWebElement PasswordInput { get => WaitElement(By.XPath("(//input[@name='AUTH-PASSWORD'])[1]")); }
        public IWebElement SignInByGoogleButton { get => WaitElement(By.XPath("//form[@id='auth_login']//a[@class='google']")); }
        public IWebElement SubmitButton { get => WaitElement(By.XPath("//input[@id='auth_submit']")); }
        public IWebElement GooglePlusButton { get => WaitElement(By.XPath("//div[@class='social-auth']//a[@class='google']")); }
        public IWebElement FacebookButton { get => WaitElement(By.XPath("//div[@class='social-auth']//a[@class='facebook']")); }

        public IWebElement GoogleMailinput { get => WaitElement(By.XPath("//input[@type='email']")); }
        public IWebElement GoogleDalee { get => WaitElement(By.XPath("//div[@id='identifierNext']")); }

        public IWebElement FacebookEmailInput { get => WaitElement(By.XPath("//input[@id='email']")); }
        public IWebElement FacebookPassInput { get => WaitElement(By.XPath("//input[@id='pass']")); }
        public IWebElement FacebookEnterButton { get => WaitElement(By.XPath("//button[@id='loginbutton']")); }



        public void AuthorizationUser(User user, string description)
        {
            ProfileButton.Click();
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("(//div[@class='modal-body'])[1]")));
            EmailInput.SendKeys(user.Email);
            PasswordInput.SendKeys(user.Password);
            SubmitButton.Click();
            Driver.Navigate().Refresh();
            WaitPageFullLoaded();
        }

        public void FacebookAuth(User user, string description="")
        {
            String MainWindow = Driver.CurrentWindowHandle;
		
            IReadOnlyCollection<string> GooglePlusWindows = Driver.WindowHandles;

            foreach (var c in GooglePlusWindows)
            {
                if (c == MainWindow)
                {
                    continue;
                }
                else
                {
                    Driver.SwitchTo().Window(c);
                    GF.SendKeys(FacebookEmailInput, UserFacebook.Email);
                    GF.SendKeys(FacebookPassInput, UserFacebook.Password);
                    GF.ClickOn(FacebookEnterButton);
                    WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
                    wait.Until((d) =>
                    {
                        GooglePlusWindows = Driver.WindowHandles;
                        return GooglePlusWindows.Count == 1;
                    });

                    Driver.SwitchTo().Window(MainWindow);
                }
            }
        }
    }

    public partial class  User
    {
        public readonly string Email;
        public readonly string Password;

        public User(string email, string password)
        {
            Email = email;
            Password = password;
        }
    }
}
