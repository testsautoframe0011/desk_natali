﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using TestFramework.General;

namespace TestFramework.Pages
{
    public class PageItem : PageBase
    {
        public PageItem() { Url = ""; }

        public readonly string ChangeToXLUrl = "/catalog/zhakety_i_zhilety/teplyy_zhaket_iz_bukle-21171-bu-30.html";

        public IWebElement AddToWishListButtonPegeItem { get => WaitElement(By.XPath("//a[@class='fav ']")); }

        public IWebElement ItemIsAddedToWishListPegeItem { get => WaitElement(By.XPath("//a[contains(@class,'fav  in-favorite')]")); }
        public IWebElement ArticleOfItemInPageItem { get => WaitElement(By.XPath("//div[@class='product-descr']//div[@class='art']")); }

        
        public IWebElement ChangeTypeOfSizesButton { get => WaitElement(By.XPath("//ul[@class='size-set']/li[not(@class='selected')]")); }
        public IWebElement ColourOfItem { get => WaitElement(By.XPath("//div[@class='infos']//div[@class='prod-color-name']/span[last()]")); }

        public IWebElement Errore_Size_Message { get => WaitElement(By.XPath("//span[@id='product_detail_size']")); }
        public IWebElement NameOfItemInPageItem { get => WaitElement(By.XPath("//div[@class='product-descr']//*[contains(@class,'h-1') or contains(@class,'h1')]")); }
        public List<IWebElement> SizesPageItemList { get => MyWaitElements(By.XPath("//div[@class='one-color ']//span")); }
        public IWebElement SizeTableButton { get => WaitElement(By.XPath("//div[@class='prod-table']/a")); }
        public IWebElement SizeTablePopup { get => WaitElement(By.XPath("//div[@id='size-table']")); }
        public IWebElement СomplementTheImage { get => WaitElement(By.XPath("//div[@class='owl-stage-outer']//div[@class='images-goods__link']//img")); }

        public By СomplementTheStyle = By.XPath("//div[@class='owl-stage-outer']//div[@class='images-goods__link']//img");

        public IWebElement TypesOfSizes { get => WaitElement(By.XPath("//ul[@class='size-set']/li[@data-code='ua']")); }


        //Availability in store (наявність в магазині)

        public IWebElement AvailabilityInStoreMessage { get => WaitElement(By.XPath("//div[@class='stores-find-city_description']")); }
        

        public IWebElement AvailabilityInStore { get => WaitElement(By.XPath("//div[@class='prod-more']/a")); }
        public IWebElement ShopsText { get => WaitElement(By.XPath("//div[@class='prod-detail-city-block']")); }
        public IList<IWebElement> ListOfCity { get => MyWaitElements(By.XPath("//div[@class='town-shop-sel-in']//a")); }

        //Viewed Slider
        public IWebElement AddSliderItemToWishList { get => WaitElement(By.XPath("//a[@class='one-cat-fav ']")); }
        public IWebElement ViewedProductsSliderItem { get => WaitElement(By.XPath("//section[@id='viewed-items']//div[@class='owl-item active']")); }
        public IWebElement ViewedProductsSliderItemQuickView { get => WaitElement(By.XPath("//section[@id='viewed-items']//div[@class='owl-item active']//div[@class='fast-look one-cat-by']")); }
        public IWebElement ViewedProductsSliderItemAddToFavorite { get => WaitElement(By.XPath("//div[contains(@class,'product-detail-viewed floated')]//div[contains(@class,'owl-item active')]//*[@class='favorite' or @id='favorite']")); }
        public IWebElement RecomendedSliderItemAddToFavorite { get => WaitElement(By.XPath("//div[contains(@id,'recomended')]//div[contains(@class,'owl-item active')]//*[@class='favorite' or @id='favorite']")); }
     
        //Sticky Bar
        public IWebElement AddItemFromStickyBarButton { get => WaitElement(By.XPath("//div[contains(@class,'sticky-bar shown')]//div[contains(@class,'sticky-add-bag')]")); }

        public IWebElement NataliFooterLogo { get => WaitElement(By.XPath("//div[@class='foot-bot']//div[@class='container']//div[@class='copy']")); }
        public By NataliFooterLogoSelector = By.XPath("//div[@class='foot-bot']//div[@class='container']//div[@class='copy']");
        

        

        //Current Product Slider 
        public List<IWebElement> ProductSlider { get => MyWaitElements(By.XPath("//div[@class='prod-car-vert']//div[@class='bx-viewport']//div//a[@data-slide-index]//img[not(@alt='видео')]")); }
        public IWebElement MainImg { get => WaitElement(By.XPath("//div[@class='bx-wrapper']//div[@class='bx-viewport']//img[@xoriginal and @aria-hidden='false']")); }
        public List<IWebElement> MainImgList { get => MyWaitElements(By.XPath("//div[@class='bx-wrapper']//div[@class='bx-viewport']//img[@xoriginal and @aria-hidden]")); }

        public IWebElement FirstSliderElement { get => WaitElement(By.XPath("//div[@class='prod-car-vert']//div[@class='bx-viewport']//div//a[@data-slide-index and @class='act']")); }



        //Video Sizes full screen img
        public List<IWebElement> ResultsBySortProductsListSale { get => MyWaitElements(By.XPath("//div[@class='prod-card-controls']/div[@class]")); }
        public IWebElement ChangeSize { get => WaitElement(By.XPath("//div[@class='product-size product-size-js product-size-desktop']/div[1]")); }

        public IWebElement ChangeSizeToXL { get => WaitElement(By.XPath("//div[@class='prod-card-controls']//div[@class='product-size__item']")); }


        // Full screen
        public List<IWebElement> FullScreenImgSlider { get => MyWaitElements(By.XPath("//div[@class='full-screen-previrew']//div[@class='full-screen-previrew-in']//img")); }
        public List<IWebElement> FullScreenImg { get => MyWaitElements(By.XPath("//div[@id='full-screen-view']//div[@class='full-screen-big-in']//following-sibling::span//img")); }
        public IWebElement FullSizeButton { get => WaitElement(By.XPath("//div[@class='prod-card-controls']//div[contains(@class,'circle zoom')]")); }
        public IWebElement FullImg { get => WaitElement(By.XPath("//div[@id='full-screen-view']//div[@class='full-screen-big-in']//following-sibling::span//img")); }
        public IWebElement QuitFullScreenMode { get => WaitElement(By.XPath("//div[@id='full-screen-view']/div[contains(@class, 'close')]")); }
        public IWebElement FullScreenZoom { get => WaitElement(By.XPath("//div[@id='full-screen-view']//div[@class='full-screen-zoom']/img")); }


        #region - TODO (temporarily disabled)
        //public bool CheckSlider(List<IWebElement> listSlider, List<IWebElement> MainImgList, string description = "")
        //{
        //    string slider;
        //    //string img;

        //    for (int i = 0; i < listSlider.Count; i++)
        //    {
        //        if (listSlider.ElementAt(i).Displayed && listSlider.ElementAt(i).Enabled)
        //        {
        //            GF.ClickOn(listSlider.ElementAt(i));

        //            Thread.Sleep(1000);

        //            slider = listSlider.ElementAt(i).GetAttribute("src");
        //            var slider_final = Regex.Match(slider, @".*\/", RegexOptions.Singleline).Value;
        //            //if (list[i].GetAttribute(attributeName: "alt") != "")
        //            //{
        //            foreach (var n in MainImgList)
        //            {
        //                if (n.Enabled && n.Displayed)
        //                {
        //                    string img;

        //                    img = n.GetAttribute("scr");
        //                    var img_final = Regex.Match(img, @".*\/", RegexOptions.Singleline).Value;

        //                    if (slider_final != img_final)
        //                    {
        //                        return false;
        //                    }
        //                    continue;
        //                }
        //            }
        //        }
        //        continue;
        //    }
        //    return true;
        //}
        #endregion

        public bool CheckFullScreenSlider(List<IWebElement> list, List<IWebElement> listOfmain, string description = "")
        {
            string slider;
            string img;

            for (int i = 0; i < list.Count; i++)
            {
                GF.ClickOn(FullScreenImgSlider.ElementAt(i));
                Thread.Sleep(300);

                slider = list[i].GetAttribute("src");

                img = listOfmain[i].GetAttribute("src");


                var slider_final = Regex.Match(slider, @".*\/", RegexOptions.Singleline).Value;
                var img_final = Regex.Match(img, @".*\/", RegexOptions.Singleline).Value;

                if (slider_final != img_final)
                {
                    return false;
                }
                continue;
            }
            return true;
        }

        public bool CheckFullScreenZoomImgDisplayed(string description = "")
        {
            string img_zoom;
            string img;
            
            for (int i = 0; i < FullScreenImgSlider.Count; i++)
            {

                FullScreenImgSlider[i].Click();
                Thread.Sleep(300);
                if (FullScreenImgSlider[i].GetAttribute("alt") != "")
                {
                    img = FullScreenImg[i].GetAttribute("alt");
                    FullScreenImg[i].Click();
                    Thread.Sleep(300);
                    img_zoom = FullScreenZoom.GetAttribute("alt");

                    if (!FullScreenZoom.Displayed && img != img_zoom)
                    {
                        return false;
                    }
                    else
                    {
                        FullScreenZoom.Click();
                        continue;
                    }
                }
                else
                {
                    img = FullScreenImg[i].GetAttribute("src");
                    FullScreenImg[i].Click();

                    Thread.Sleep(300);
                    img_zoom = FullScreenZoom.GetAttribute("src");
                    var img_zoom_final = Regex.Match(img_zoom, @".*\/", RegexOptions.Singleline).Value;
                    var img_final = Regex.Match(img, @".*\/", RegexOptions.Singleline).Value;

                    if (!FullScreenZoom.Displayed && img_final != img_zoom_final)
                    {
                        return false;
                    }
                    else
                    {
                        FullScreenZoom.Click();
                        continue;
                    }
                }
            }
            return true;
        }


        public bool CheckMainImgAfterToggleSize(string description = "")
        {
            string before;
            string after;

            before = MainImg.GetAttribute("src");
            GF.MoveToElement(ChangeSize);
            GF.ClickOn(ChangeSize);
            GF.ClickOn(ChangeSizeToXL);
            Thread.Sleep(500);
            GF.MoveToElement(ArticleOfItemInPageItem);
            Thread.Sleep(500);
            after = MainImg.GetAttribute("src");

            var before_final = Regex.Match(before, @".*\/", RegexOptions.Singleline).Value;
            var after_final = Regex.Match(after, @".*\/", RegexOptions.Singleline).Value;

            if (before_final == after_final)
            {
                return false;
            }
            return true;

        }

        public string GetArticleOfItem(IWebElement elementNumber)
        {
            String elem = elementNumber.Text;
            var split = elem.Split(" ");
            string number;
            number = split[1] + " " + split[2];
            return number;
        }

        public bool CheckTheDifferenceInStoreAddresses()
        {
            int count = 0;

            string noProduct = "Данного размера нет в наличии в этом городе.";
            string number = "Телефон";

            for (int i = 0; i < ListOfCity.Count; i++)
            {
                Thread.Sleep(1000);
                GF.ClickOn(ListOfCity[i]);
                Thread.Sleep(1000);
                if (ShopsText.Text.Contains(noProduct))
                {
                    count++;
                }
                else
                {
                    if (ShopsText.Text.Contains(number))
                    {
                        return true;
                    }
                    else return false;
                }
            }

            if (count == 4)
            {
                return false;
            }
            else return true;
        }
    }
}
