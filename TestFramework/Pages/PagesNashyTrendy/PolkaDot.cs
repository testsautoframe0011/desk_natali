﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestFramework.Pages.PagesNashyTrendy
{
    public class PolkaDot : PageBase
    {
        public PolkaDot() { Url = "nashy_trendy/polka-dot/"; }
        public IWebElement ViewFirstObrazVesna21Button { get => WaitElement(By.XPath("//div[@class='top-banner']//following-sibling::span")); }
        public IWebElement ViewMiddleObrazVesna21Button { get => WaitElement(By.XPath("(//div[@class='top-banner']//following-sibling::span)[6]")); }
    }
}
