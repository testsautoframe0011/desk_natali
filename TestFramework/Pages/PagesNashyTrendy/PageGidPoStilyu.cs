﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestFramework.Pages.PagesNashyTrendy
{
    public class PageGidPoStilyu : PageBase
    {
        public PageGidPoStilyu() { Url = "nashy_trendy/gid-po-stilyu/"; }

        public IWebElement ViewSecondObrazGidButton { get => WaitElement(By.XPath("//div[(@class='banners desktop')]//div[@class='bot-banner'][1]//span[@class='half-banner']//span[2]/a"), 10); }
        public IWebElement ViewLastObrazGidButton { get => WaitElement(By.XPath("(//div[contains(@*,'banners desktop')]//span[@class='half-banner'])[last()]"), 10); }
    }
}
//div[contains(@*,'banners desktop')]//div[@class="bot-banner"]//span[@class='half-banner'][