﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestFramework.Pages.PagesNashyTrendy
{
    public class Page365Dney : PageBase    
    {
        public Page365Dney() { Url = "nashy_trendy/365-collection/"; }

        public IWebElement ViewFirstObraz365DneyObrazButton { get => WaitElement(By.XPath("(//div[contains(@*,'banners')]//div[@class='top-banner'][2])"), 10); }
        
    }
}
