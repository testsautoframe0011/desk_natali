﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestFramework.Pages.PagesNashyTrendy
{
    public class PageStayWarm : PageBase
    {
        public PageStayWarm() { Url = "nashy_trendy/30-gotovykh-obrazov-dlya-moroznykh-dney/"; }

        //public IWebElement ViewFirstObrazStayWarmButton { get => WaitElement(By.XPath("//div[contains(@*,'banners desktop')]//div[@class='bot-banner']//span[@class='half-banner']//div[1]"), 10); }//(//div[@class='bot-banner'])[1]//a[@class='set-btn']
        public IWebElement ViewFirstObrazStayWarmButton { get => WaitElement(By.XPath("//div[contains(@*,'banners desktop')]//div[@class='bot-banner']//span[@class='half-banner']//a//div//img")); }//div[contains(@*,'banners desktop')]//div[@class='bot-banner']//span[@class='half-banner']//div[1]"), 10); }//(//div[@class='bot-banner'])[1]//a[@class='set-btn']
        //public IWebElement ViewFirstObrazStayWarmButton { get => WaitElement(By.XPath("(//div[@class='cont full-width-cont']//div[@class='bot-banner'][last()]//a[@class='set-bt'])[4]"), 10); } //(//div[@class='bot-banner'])[last()]//a[@class='set-bt']


        public IWebElement ViewMiddleObrazStayWarmButton { get => WaitElement(By.XPath("(//div[@class='cont full-width-cont']//div[@class='bot-banner'][last()]//a[@class='set-bt'])[4]"), 10); } //(//div[@class='bot-banner'])[last()]//a[@class='set-bt']
    }
}
