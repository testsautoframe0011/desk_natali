﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestFramework.Pages.PagesNashyTrendy
{
    public class OfficeCollection : PageBase
    {
        public OfficeCollection() { Url = "/nashy_trendy/office-new-collection/"; }

        public IWebElement ViewFirstObrazVesna21Button { get => WaitElement(By.XPath("(//div[contains(@class,'top-banner')]//following-sibling::img)[position()>1]")); }
        public IWebElement ViewMiddleObrazVesna21Button { get => WaitElement(By.XPath("(//div[contains(@class,'top-banner')]//following-sibling::img)[position()>5]")); }
    }
}
