﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestFramework.Pages.PagesNashyTrendy
{
    public class PageNashiTkani : PageBase
    {
        public PageNashiTkani() { Url = "nashy_trendy/nashi-osnovnye-tkani/"; }

        public IWebElement FirstItemNashiTkani { get => WaitElement(By.XPath("//div[@class='owl-item active']"), 10); }
    }
}
