﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestFramework.Pages.PagesNashyTrendy
{
    public class PageBlackWhiteOffice : PageBase
    {
        public PageBlackWhiteOffice() { Url = "nashy_trendy/gid-po-stilyu/"; }
        
        public IWebElement ViewFirstObrazBlackWhiteOfficeButton { get => WaitElement(By.XPath("(//div[contains(@*,'banners desktop')]//div[@class='bot-banner']//span[@class='half-banner'])[1]"), 10); }//div[@class='bot-banner'][1]/span[@class='half-banner'][1]       
    }
}
