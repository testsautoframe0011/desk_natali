﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework.General;

namespace TestFramework.Pages
{
    public partial class PCatalog : PageBase
    {
        public PCatalog() { Url = ""; }

        public List<IWebElement> ProductItemList { get => MyWaitElements(By.XPath("//article[contains(@class,'product-card')]")); }
        public List<IWebElement> ProductItemListPrice { get => MyWaitElements(By.XPath("//article[contains(@class,'product-card')]//div[@class='product-card__price-block']/span[last()]")); }

        public List<IWebElement> ProductItemFewColors { get => MyWaitElements(By.XPath("//article[contains(@class,'product-card')]//div[@class='offer-color__colors product-card__colors'][count(a)>1]//preceding-sibling::a[@class='product-card__name']")); }


        public IWebElement EliteProductItem { get => WaitElement(By.XPath("//article[contains(@class,'unique-product-right')]")); }


        public IWebElement FirstItemWithColours { get => WaitElement(By.XPath("//div[contains(@class,'offer-color__colors')]/a")); }
        public IWebElement SecondItemWithColours { get => WaitElement(By.XPath("//div[contains(@class,'offer-color__colors')]/a[position()>1]")); }



        public IWebElement LastProductItem { get => WaitElement(By.XPath("(//div[contains(@class,'offer-color__colors')])[last()]")); }

        public List<IWebElement> ProductItemsTitlesList { get => MyWaitElements(By.XPath("//a[@class='product-card__name']")); }

        public IWebElement SearchErrorMessage { get => WaitElement(By.XPath("//h1[@class='catalog-top__search-empty']")); }
        public IWebElement HoverSize { get => WaitElement(By.XPath("//span[@class='product-card__name']")); } // 


        //CatalogPage main items (ці розділи знаходяться на сторінці каталог)
        public IWebElement PlatyaCatalogButton { get => WaitElement(By.XPath("//div[@class='top-banner']/a[@href='/catalog/platya/']")); }
        public IWebElement KostyumyCatalogButton { get => WaitElement(By.XPath("//span[@class='half-banner']/a[@href='/catalog/kostyumy/']")); }
        public IWebElement ZhaketyCatalogButton { get => WaitElement(By.XPath("//span[@class='half-banner']/a[@href='/catalog/zhakety_i_zhilety/']")); }
        public IWebElement BluzyTunikiCatalogButton { get => WaitElement(By.XPath("//div[contains(@class,'banner')]//a[@href='/catalog/bluzy_i_tuniki/']")); }


        //Adding to Wishlist
        public IWebElement AddToWishListButtonOfFirstItem { get => WaitElement(By.XPath("//div[@class='def-like']")); }
        public IWebElement AddToWishListButtonOfSecondItem { get => WaitElement(By.XPath("(//div[@class='def-like'])[2]")); }
        public IWebElement AddToWishListButtonOfThirdItem { get => WaitElement(By.XPath("(//div[@class='def-like'])[3]")); }
        public IWebElement AddToWishListButtonFromNewArrivals { get => WaitElement(By.XPath("//div[@id='novlety']//div[@class='owl-item active']//a[contains(@class,'one-cat-fav')]")); }
        public IWebElement DeleteFromWishListFirstItemButton { get => WaitElement(By.XPath("//div[contains(@class,'def-like_active')]")); }
        public IWebElement ItemIsAddedToWishList { get => WaitElement(By.XPath("//div[@class='def-like def-like_active']")); }

        

        public IWebElement GoToItemPageQuickViewButton { get => WaitElement(By.XPath("//div[@class='def-button']")); }
        public IWebElement ProductItemTitleQuickView { get => WaitElement(By.XPath("//div[@class='product-descr']//h1")); }
        public IWebElement QuickViewOfFirstItemWishList { get => WaitElement(By.XPath("//div[@class='my-fav']//div[contains(@class,'fast-look')]")); }

        public IWebElement ErroreSizeMessage { get => WaitElement(By.XPath("//div[@class='product-detail__size']//span[@class='size-text-error']")); }

        

        //Colours and Sizes
        public IWebElement ChangeColourButtonCatalog { get => WaitElement(By.XPath("//article[contains(@class,'product-card')]//div[@class='offer-color__colors product-card__colors'][count(a)>1]//a[@class='offer-color__color']")); }
        public IWebElement ItemWithOtherColour { get => WaitElement(By.XPath("(//div[@class='one-cat-thumb'])[1]/div[@class='product_img ']/a[2][@style='display: inline;']")); }
        public IWebElement ProductItemSizes { get => WaitElement(By.XPath("(//div[@class='one-cat-sizes-aval '])[1]")); }
        public IWebElement ProductItemTitleWithColours { get => WaitElement(By.XPath("//div[contains(@class, 'one-cat-color')]/div[contains(@class, 'product_list_size one-color')][2]/parent::div/preceding-sibling::div[contains(@class, 'one-cat-tit')]/a")); }//изменил
        public IWebElement ProductItemWithColours { get => WaitElement(By.XPath("//div[@class='one-cat-color']/div[@class='product_list_size one-color'][2]/parent::div/parent::div")); }
        public List<IWebElement> SizesHoverEffectList { get => MyWaitElements(By.XPath("//span[@class='product-card__name']//span[@class='product-card__size']")); }


        public IWebElement h1Catalog { get => WaitElement(By.XPath("(//ul[@class='menu-aside vue-menu-aside']/li[not(@class)])[1]")); }

        

        public void FindAbilityToСomplementImage(string description = "")
        {
            PageItem p = new PageItem();

            for (int i = 0; i < ProductItemList.Count; i++)
            {
                ProductItemList.ElementAt(i).Click();
                GF.CheckAction(p.NameOfItemInPageItem.Displayed);
                try
                {
                    if (!GF.CheckDisplayed(p.СomplementTheStyle))
                    {
                        Driver.Navigate().Back();
                        continue;
                    }
                    else break;
                }
                catch (Exception ex)
                {
                    if (ex is NoSuchElementException || ex is TimeoutException)
                    {
                        Driver.Navigate().Back();
                        continue;
                    }
                }
            }
        }
        public void FindAbilityChangeColor(string description = "") // можно смотреть на странице каталога на кол-о единиц товара - так быстрее
        {
            GeneralXPath gxp = new GeneralXPath();

            for (int i = 0; i < ProductItemList.Count; i++)
            {
                GF.ClickOn(ProductItemList.ElementAt(i));
                GF.CheckAction(gxp.ItemSize.Displayed);
                if (gxp.ListOfColors.Count < 2)
                {
                    Driver.Navigate().Back();
                    continue;
                }
                else break;
            }
        }

        public bool SearchResultCorrectRequest(List<IWebElement> webElements, string textOfItem, string description = "")
        {
            if (webElements == null)
            {
                return false;
            }
            foreach (var item in webElements)
            {
                if (!item.Text.ToLower().Contains(textOfItem)) { return false; }
            }
            return true;
        }
    }

    public partial class PCatalog : PageBase
    {
        //Pagination (переключення сторінок)

        public IWebElement SecondPageIsActive { get => WaitElement(By.XPath("//div[@id='catalog_pagination_num']//li[3][@class='active']")); }
        public IWebElement LookMorepaginationButton { get => WaitElement(By.XPath("//div[contains(@class,'add-more pagination')]")); }
        public By LookMorepaginationButtonSelector = By.XPath("//div[contains(@class,'add-more pagination')]");
        public IWebElement PaginationActivePage { get => WaitElement(By.XPath("//ul[@class='pagination__nav']/li[contains(@class,'active')]")); }
        public IWebElement PrevPagePaginationButton { get => WaitElement(By.XPath("//li[@class='pagination__button']//*[contains(@*,'left')]")); }
        public IWebElement NextPagePaginationButton { get => WaitElement(By.XPath("//li[@class='pagination__button']//*[contains(@*,'right')]")); }
        public List<IWebElement> PaginationElements { get => MyWaitElements(By.XPath("//ul[@class='pagination__nav']/li[not(@class='pagination__button') and not(contains(.,'...'))]")); }
        public IWebElement PaginationInput { get => WaitElement(By.XPath("//div[@id='catalog_pagination_num']//li[position()>1 and position()<last()]/span[@class='ellipse clickable']")); }


        public void NextPaginationElement(List<IWebElement> list, List<IWebElement> toChange, int elementat, string description = "")
        {
            try
            {
                var elBefore = toChange[0];

                GF.ClickOn(list[elementat]);
                GF.CheckElementNotAttachedToDom(elBefore);

            }
            catch (ArgumentOutOfRangeException)
            {
                var elBefore = toChange[0];
                GF.ClickOn(list[list.Count - 1]);
                GF.CheckElementNotAttachedToDom(elBefore);
            }
        }

        public void PrevPaginationElement(List<IWebElement> list, List<IWebElement> toChange, int amountPageReturn, string description = "")
        {
            try
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].GetAttribute("class") == "active")
                    {
                        var elBefore = toChange[0];

                        GF.ClickOn(list[i - amountPageReturn]);
                        GF.CheckElementNotAttachedToDom(elBefore);
                        break;
                    }
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentOutOfRangeException("Start from another element");
            }
        }

        public void NextElementButton(List<IWebElement> toChange, string description = "")
        {
            var elBefore = toChange[0];
            GF.MoveToElement(ProductItemList.Last());
            GF.MoveToElementAndClick(NextPagePaginationButton);
            GF.CheckElementNotAttachedToDom(elBefore);
        }

        public void PrevElementButton(List<IWebElement> toChange, string description = "")
        {
            var elBefore = toChange[0];

            GF.MoveToElementAndClick(PrevPagePaginationButton);
            GF.CheckElementNotAttachedToDom(elBefore);
        }


    }

    public partial class PCatalog : PageBase
    {
        /*//FILETRS//*/

        public List<IWebElement> ResultsBySortProducts { get => MyWaitElements(By.XPath("//div[@class='product-card__price-block']/span[last()]")); }

        public By FiltredPrice = By.XPath("//div[@class='product-card__price-block']/span[last()]");

        public IWebElement CatalogSortSelectRight { get => WaitElement(By.XPath("//div[@class='catalog-top__sort']//div[@class='select-vue-sort']")); }
        public List<IWebElement> SortSelectRightChoice { get => MyWaitElements(By.XPath("//div[@class='select-vue-sort__list']/span")); }

        public IWebElement OpenMoreFilterButton { get => WaitElement(By.XPath("//div[@class='filter-big-block__header']")); }
        public IWebElement OpenPriceFilterButton { get => WaitElement(By.XPath("//div[@class='filter-block__title' and contains(text(),'Цена')]//parent::div")); }

        public IWebElement SortFilterTopMenu { get => WaitElement(By.XPath("//div[@class='catalog-top__sort-block']")); }

        

        // Sorting 
        public IWebElement UbivanieSelectRight { get => WaitElement(By.XPath("//div[@class='select-vue-sort__list']/span[4]")); }
        public IWebElement VozrastanieSelectRight { get => WaitElement(By.XPath("//div[@class='select-vue-sort__list']/span[@class='select-vue-sort__link' and contains(text(),'Цена по возрастанию')]")); }

        //Filters
        public IWebElement FilterHasBeenAppliedTop { get => WaitElement(By.XPath("//div[@class='catalog-top__filters']//div[@class='vue-select-color__header']/span[contains(text(),'')]")); }
        public List<IWebElement> DeselectFilterrButton { get => MyWaitElements(By.XPath("//div[@class='cancel-filter__items']/span")); }

        public IWebElement FilterSizeHasBeenAppliedTop { get => WaitElement(By.XPath("//div[@class='vue-select-size__header']/span[not(contains(text(),'Размеры'))]")); }
        

        public IWebElement MoreFiltersButton { get => WaitElement(By.XPath("//li[@class='more-properties ']")); }
        public IWebElement ResetAllFiltersButton { get => WaitElement(By.XPath("//a[@class='def-button-border']")); }

        //Main Filter colour 
        public IWebElement BlackColourInFilterTopMenu { get => WaitElement(By.XPath("//div[@class='filter-color__items']//label[@class='def-color']/span[1 and following-sibling::span[text()='Черный']]")); }
        public IWebElement ColoursFilterLeftMenu { get => WaitElement(By.XPath("//div[@class='vue-select-color__header']")); }
        public IWebElement ColoursFilterTopMenu { get => WaitElement(By.XPath("//div[@class='vue-select-color']")); }
        public IWebElement RedColourInFilterLeftMenu { get => WaitElement(By.XPath("//div[@class='filter-color__items']//label[@class='def-color']/span[1 and following-sibling::span[text()='Красный']]")); }

        //Filter size
        public IWebElement ChangeTypeOfSizesButton { get => WaitElement(By.XPath("//div[@class='size-tabs__control']/span[not(contains(@class,'active'))]")); }
        public IWebElement FirstSizeInFilterLeftMenu { get => WaitElement(By.XPath("//div[@class='filter-size__items']/label[@class='filter-size__checkbox'][1]")); }

        public List<IWebElement> SizeInFilterList { get => MyWaitElements(By.XPath("//div[@class='filter-size__items']/label[@class='filter-size__checkbox']")); }

        public IWebElement FirstSizeInFilterTopMenu { get => WaitElement(By.XPath("//div[@class='vue-select-size__items']//label[1]")); }
        public IWebElement SizesFilterLeftMenu { get => WaitElement(By.XPath("//div[@id='custom_filter']//li[@data-filter_group_code='sizes']")); }
        public IWebElement SizesFilterTopMenu { get => WaitElement(By.XPath("//div[@class='vue-select-size__header']")); }
        public IWebElement FilterText { get => WaitElement(By.XPath("//span[@class='menu-name']")); }

        //Filter price
        public IWebElement PriceMinFilter { get => WaitElement(By.XPath("//div[@class='catalog-range__block-input']//input[@data-name='MIN']")); }
        public IWebElement PriceMaxFilter { get => WaitElement(By.XPath("//div[@class='catalog-range__block-input']//input[@data-name='MAX']")); }
    }
}