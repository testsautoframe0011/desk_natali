﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework.General;

namespace TestFramework.Pages
{
    public class PageCheckOut : PageBase
    {
        public PageCheckOut() { Url = "order/order/"; }


        public IWebElement CheckoutToCartButton { get => WaitElement(By.XPath("//div[@class='order-steps']/div[1]")); }

        /*//ItemFiel//*/

        //Item
        public IWebElement ItemCheckOutFeild { get => WaitElement(By.XPath("//div[@class='order-left ']//div[@class='your-order']")); }
        public IWebElement ItemBasketInOrderPage { get => WaitElement(By.XPath("//div[@class='order-basket-role']")); }
        public List<IWebElement> ProductsInCheckout { get => MyWaitElements(By.XPath("//div[@id='order-ajax-block']//div[contains(@class,'one-your-order basket-product')]")); }

        //DeleteItem
        public IWebElement DeleteItemFromCheckOutButton { get => WaitElement(By.XPath("//div[@class='bask-delete']//i[@class='close'][last()]")); }

        //PriceAndDeliveryCost

        public IWebElement FreeDeliveryMessageCheckoutOformlenie { get => WaitElement(By.XPath("//div[@class='order-left ']//div[@class='sum-price-num'][not(contains(text(),'грн'))]")); }
        public IWebElement PaidDeliveryMessageCheckoutOfomlenie { get => WaitElement(By.XPath("//div[@class='order-sum-price']/div[@class='sum-row']//div[@class='sum-price-name' and contains(text(),'Доставка')]/following-sibling::div")); }
        public IWebElement GeneralPrice { get => WaitElement(By.XPath("//div[@class='sum-row sum-total']/div[@class='sum-price-num sum-price-total']")); }
        public IWebElement PriceOfFirstItem { get => WaitElement(By.XPath("(//div[@class='one-your-order-descr']//div[@class='one-your-order-item-param'][4])[1]")); }
        public IWebElement PriceOfSecondItem { get => WaitElement(By.XPath("(//div[@class='one-your-order-descr']//div[@class='one-your-order-item-param'][4])[2]")); }

        //PromoCode

        public By DiscountAmountSelector = By.XPath("(//div[@class='sum-price-name' and contains(text(),'Промокод')])//following-sibling::div");
        public IWebElement DiscountAmount { get => WaitElement(By.XPath("(//div[@class='sum-price-name' and contains(text(),'Промокод')])//following-sibling::div")); }
        public IWebElement ShowCouponButton { get => WaitElement(By.XPath("//a[@id='show-coupon']")); }
        public IWebElement EnterCouponField { get => WaitElement(By.XPath("//div[@class='one-action-bl-in']//input")); }
        public IWebElement EnterCouponButton { get => WaitElement(By.XPath("//a[@class='action-bt']")); }
        public IWebElement PromoError { get => WaitElement(By.XPath("//div[@class='order-left ']//div[@class='err-text']")); }

        

        /*//OrderFrom//*/
        public IWebElement FinishCheckoutButton { get => WaitElement(By.XPath("//button[contains(text(),'Оформить покупку')]")); }

        //Authorization

        public IWebElement AuthSubmitButton { get => WaitElement(By.XPath("//input[@id='order-auth_submit']")); }
        public IWebElement AuthOrderForm { get => Driver.FindElement(By.XPath("//div[@class='order-form']")); }
        public IWebElement PasswordUserInput { get => WaitElement(By.XPath("//input[@name='USER_PASSWORD']")); }
        public IWebElement FacebookCheckoutAutr { get => WaitElement(By.XPath("//div[@class='order-social-auth']/a[contains(@onclick,'facebook')]//img")); }

        //UserData

        public IWebElement LoginUserInput { get => WaitElement(By.XPath("//input[@name='USER_LOGIN']")); }
        public IWebElement EmailUserInput { get => WaitElement(By.XPath("//input[@placeholder='EMAIL']")); }

        //PostalAndPayment//

        //ToOffice
        public By DropDownWayOfDelivery = By.XPath("//select[@class='order-delivery-select']");
        public By DropDownPost = By.XPath("//select[@name='ORDER_PROP_8']");

        public IWebElement FirstBuyButton { get => WaitElement(By.XPath("//div[@class='log-bt']//a[@class='first-buy']")); }
        public IWebElement InputCity { get => WaitElement(By.XPath("//input[@name='ORDER_PROP_4']")); }
        public IWebElement TextOfPost { get => WaitElement(By.XPath("(//select[@name='ORDER_PROP_8']/following-sibling::div/div)")); }
        public IWebElement ChoosePostOfficeButton { get => WaitElement(By.XPath("(//div[@class='jq-selectbox__select']//div[@class='jq-selectbox__select-text'])[4]")); }
        public IWebElement ShowDeliveryList { get => WaitElement(By.XPath("(//div[@class='one-order-form'])[4]")); }

        //ToHome
        public IWebElement InputStreet { get => WaitElement(By.XPath("//input[@name='ORDER_PROP_5']")); }
        public IWebElement InputHomeAddress { get => WaitElement(By.XPath("//input[@name='ORDER_PROP_6']")); }
        public IWebElement InpuApartment { get => WaitElement(By.XPath("//input[@name='ORDER_PROP_7']")); }

        public IWebElement CashPaymentUnactive { get => WaitElement(By.XPath("//div[@class='one-pay-radio'][@data-id='2']")); }
        public IWebElement CardPaymentUnactive { get => WaitElement(By.XPath("//div[@class='one-pay-radio'][@data-id='2']")); }


        public IWebElement CashPaymentActive { get => WaitElement(By.XPath("//div[@class='one-pay-radio checked'][@data-id='2']")); }
        public IWebElement CardPaymentActive { get => WaitElement(By.XPath("//div[@class='one-pay-radio checked'][@data-id='5']")); }


        public void FillInputFields(string Street, string Home, string Apartment, string descriptionint = "")
        {
            GF.SendKeys(InputStreet, Street);
            GF.SendKeys(InputHomeAddress, Home);
            GF.SendKeys(InpuApartment, Apartment);
        }

        public bool TextWasInput(string Street, string Home, string Apartment)
        {
            return !InputStreet.Text.Contains(Street) || !InputHomeAddress.Text.Contains(Home) || !InpuApartment.Text.Contains(Apartment);
        }

        //public void ChooseWayOfDelivery(int DeliveryTo, string descriptionint = "")
        //{
        //    SelectElement oSelect = new SelectElement(Driver.FindElement(By.XPath("//select[@class='order-delivery-select']")));
        //    IList<IWebElement> elementCount = oSelect.Options;
        //    elementCount.ElementAt(elementCount.Count - 1).Click();
        //}

        public void DeleteElementsCheckout(string descriptionint, int quantityToNotDelete = 0)
        {
            var count = ProductsInCheckout.Count;

            for (int i = 0; i < count + 1; i++)
            {
                if (count == quantityToNotDelete)
                {
                    break;
                }

                GF.ClickOn(DeleteItemFromCheckOutButton, "");
                count--;
                
                //GF.CheckElementNotAttachedToDom(ProductsInCheckout[i]);
                Thread.Sleep(1000);
                Driver.Navigate().Refresh();
            }
        }

        public void EnterCoupon(string coupon, string descriptionint = "")
        {
            GF.ClickOn(ShowCouponButton);
            EnterCouponField.SendKeys(coupon);
            GF.ClickOn(EnterCouponButton);
        }

        /// <summary>
        /// Добавляет со ВТОРОГО элемента (Первый всегда одинаков)
        /// </summary>
        /// <param name="dropDown"></param>
        /// <param name="descriptionint"></param>
        /// <returns></returns>
        public List<string> DropDown(By dropDown, string descriptionint = "")
        {
            List<string> str = new List<string>();
            SelectElement oSelect = new SelectElement(Driver.FindElement(dropDown));
            IList<IWebElement> elementCount = oSelect.Options;

            for (int i = 1; i < elementCount.Count; i++)
            {
                str.Add(elementCount.ElementAt(i).Text);
            }
            return str;
           
        }

        public bool PostIsDisplayed(int chooseElementAt = 2, string descriptionint = "")
        {
            GF.ClickOn(ChoosePostOfficeButton);
            string str;
            SelectElement oSelect = new SelectElement(Driver.FindElement(DropDownPost));
            IList<IWebElement> elementCount = oSelect.Options;
            try
            {

                str = elementCount.ElementAt(chooseElementAt).Text;
                elementCount.ElementAt(chooseElementAt).Click();
                GF.ClickOn(ChoosePostOfficeButton);
            }
            catch (ArgumentOutOfRangeException)
            {
                str = elementCount.ElementAt(10).Text;
                elementCount.ElementAt(10).Click();
                GF.ClickOn(ChoosePostOfficeButton);
            }
            return TextOfPost.Text.ToLower().Trim() == str.ToLower().Trim();
        }

        public bool ChangePayment(bool ClickOnFirst = true, string descriptionint = "")
        {
            if (ClickOnFirst == true)
            {
                GF.MoveToElement(CashPaymentUnactive);
                GF.ClickOn(CashPaymentUnactive);
                Thread.Sleep(1000);
                return CashPaymentActive.Displayed;
            }
            else
            {
                GF.MoveToElement(CardPaymentUnactive);
                GF.ClickOn(CardPaymentUnactive);
                Thread.Sleep(1000);
                return CardPaymentActive.Displayed;
            }
        }
    }
}
