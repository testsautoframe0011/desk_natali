﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TestFramework.General;

namespace TestFramework.Pages.NewFolder
{
    public class XMLfile
    {

        protected static string Localization { get => Config.Localization; }

        static XmlElement xRoot;

        public XMLfile()
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load("C://Dmytro//natali//Automation//FrameWork//TestFramework//natalyqarepo//TestFramework//Pages//XML_READER//XMLFile1.xml");
            xRoot = xDoc.DocumentElement;
        }

        private static  XmlElement XRoot
        {
            get
            {
                return xRoot;
            }
        }

        //походу из-за статического метода не работает
        public string GetLocator(string Elementx)
        {

            string ObjLocation = String.Format("NameEl[@name='{0}']/{1}", Elementx, Localization).Trim();

            XmlNode childnode = XRoot.SelectSingleNode(ObjLocation);

            return childnode.InnerXml;
        }

    }
}
