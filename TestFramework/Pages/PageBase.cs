﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using TestFramework.General;
using TestFramework.Pages.NewFolder;

namespace TestFramework.Pages
{
    public partial class PageBase
    {
        protected static IWebDriver Driver { get => Drivers.dr; }
        protected string BaseUrl { get => URLBuilder.BaseUrl; }

        public XMLfile XML = new XMLfile();

        protected static int timeLoadPage = 10;

        public string Url;
        public PageBase() { }


        protected static IWebElement WaitElement(By selector, int time = 10)
        {
            WaitPageFullLoaded();
            return GF.WaitElement(Driver, selector, time, new Condition(GF.ExistDisplayedEnabled));
        }

        protected static IWebElement WaitElement(By selector, int time, Condition cnd)
        {
            WaitPageFullLoaded();
            return GF.WaitElement(Driver, selector, time, cnd);
        }


        public static List<IWebElement> MyWaitElements(By locator, long timeToWait = 5)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeToWait));
            try
            {
                return wait.Until((x) =>
                {
                    List<IWebElement> list = Driver.FindElements(locator).ToList();
                    if (list.Count() == 0)
                    {
                        return null;
                    }
                    else
                    {
                        return list;
                    }
                });
            }
            catch (Exception ex)
            {
                if (ex is NoSuchElementException || ex is TimeoutException)
                {
                    return null;
                }
                else { return null; }
            }
        }

        /// <summary>
        /// Wait until a page is fully loaded
        /// </summary>
        /// <param name="time"></param>
        public static void WaitPageFullLoaded()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(timeLoadPage));

            wait.Until((x) =>
            {
                return ((IJavaScriptExecutor)Driver).ExecuteScript("return document.readyState").Equals("complete");
            });
        }

        //Sorts (використовується для перевірки чи правильно відсортований товар за ціною)
        /*        public bool SortPrice(IList<IWebElement> elements, bool sort)
                {
                    bool assert = false;
                    IWebElement firstElement = elements.ElementAt(0);
                    int numberFirst = GetNumberPrice(firstElement);
                    if (sort == true)
                    {
                        for (int i = 1; i < (elements.Count); i++)
                        {
                            int numberSecond = GetNumberPrice(elements.ElementAt(i));
                            if (numberFirst <= numberSecond) numberFirst = numberSecond;
                            else return assert;
                        }
                    }
                    else
                    {
                        for (int i = 1; i < (elements.Count); i++)
                        {
                            int numberSecond = GetNumberPrice(elements.ElementAt(i));
                            if (numberFirst >= numberSecond) numberFirst = numberSecond;
                            else return assert;
                        }
                    }
                    return assert = true;
                }*/

        public bool SortPrice(IList<IWebElement> elements, bool ubivaanie)
        {
            bool assert = false;
            IWebElement firstElement = elements.ElementAt(0);
            double numberFirst = GetNumberPrice(firstElement);
            if (ubivaanie == false)
            {
                for (int i = 1; i < (elements.Count); i++)
                {
                    //GF.MoveToElement(elements.ElementAt(i));
                    double numberSecond = GetNumberPrice(elements.ElementAt(i), true);
                    if (numberFirst <= numberSecond) numberFirst = numberSecond;
                    else return assert;
                }
            }
            else
            {
                for (int i = 1; i < (elements.Count); i++)
                {
                    double numberSecond = GetNumberPrice(elements.ElementAt(i));
                    if (numberFirst >= numberSecond) numberFirst = numberSecond;
                    else return assert;
                }
            }
            return assert = true;
        }



        public int GetNumberPriceINT(IWebElement elementNumber, string description = "")
        {
            GF.MoveToElement(elementNumber);
            String elem = elementNumber.Text;
            var split = elem.Split(" ");
            int number;
            if (split.Length < 3)
            {
                number = Int32.Parse(split[0]);
            }
            else
            {
                number = Int32.Parse(split[0] + split[1]);
            }
            return number;
        }

        public double GetNumberPrice(IWebElement elementNumber, bool move = false, string description = "")
        {
            if (move == true)
            {
                GF.MoveToElement(elementNumber);
            }
            String elem = elementNumber.Text;

            elem = elem.Replace(" ", "");
            var final = Regex.Match(elem, @"[+-]?\d*[\.\,]?\d+", RegexOptions.Singleline).Value;
            double d = double.Parse(final, CultureInfo.InvariantCulture);

            return d;
        }

        //Comparing Lists (порівняння двох листів)
        public bool ComparingTwoLists(List<string> el1, List<string> el2)
        {
            if (el1.Count != el2.Count)
            {
                return false;
            }

            for (int i = 0; i < el1.Count; i++)
            {
                if (el1.ElementAt(i) != (el2.ElementAt(i)))
                    return false;
            }
            return true;
        }


        //Comparing Lists (порівняння двох листів) SORRTED
        public bool ComparingTwoListsSorted(List<string> el1, List<string> el2)
        {
            if (el1.Count != el2.Count)
            {
                return false;
            }

            for (int i = 0; i < el1.Count; i++)
            {
                if (!el1.Contains(el2[i]))
                    return false;
            }
            return true;
        }

        public bool ComparingTwoListsBeElements(List<string> el1, List<string> el2)
        {
            if (el1.Count == 0 || el2.Count == 0)
            {
                return true;
            }

            if (el1.Count == el2.Count && el1[1] == el2[1])
            {
                return true;
            }

            for (int i = 0; i < el1.Count; i++)
            {


                if (el1.ElementAt(i) != (el2.ElementAt(i)))
                {
                    return false;
                }
            }
            return true;
        }

        public List<string> MakeNewList(List<IWebElement> l)
        {
            List<string> str = new List<string>();
            for (int i = 0; i < l.Count; i++) { str.Add(l.ElementAt(i).Text); }
            return str;
        }

        //проверка фильтра по цене
        public bool CheckPriceFilter(IList<IWebElement> elements, string min, string max)
        {
            var count = elements.Count;
            double minPrice = double.Parse(min);
            double maxPrice = double.Parse(max);
            int summ = 0;
            for (int i = 0; i < count; i++)
            {
                double numberFirst = GetNumberPrice(elements.ElementAt(i));

                if ((numberFirst >= minPrice) && (numberFirst <= maxPrice)) summ++;
                else return false; ;
            }
            if (summ == count) { return true; }
            else { return false; }
        }

        //проверка фильтра по цене
        public bool CheckPriceFilterINT(IList<IWebElement> elements, string min, string max)
        {
            var count = elements.Count;
            int minPrice = int.Parse(min);
            int maxPrice = int.Parse(max);
            int summ = 0;
            for (int i = 0; i < count; i++)
            {
                int numberFirst = GetNumberPriceINT(elements.ElementAt(i));
                if ((numberFirst >= minPrice) && (numberFirst <= maxPrice)) summ++;
                else summ = 0;
            }
            if (summ == count) { return true; }
            else { return false; }
        }

        //сума елементів в образі 
        public int SummOfPricesOfObrazItems(IList<IWebElement> elements)
        {
            int summ = 0;
            for (int i = 0; i < (elements.Count); i++)
            {
                int numberFirst = GetNumberPriceINT(elements.ElementAt(i));
                summ += numberFirst;
            }
            return summ;
        }
    }

    public partial class PageBase
    {
        //public PageBase() { Url = ""; }

        public readonly string PlatyaUrl = "catalog/platya/";
        public readonly string NovinkiUrl = "catalog/novye_postupleniya/";
        public readonly string BazoviyPlatyaUrl = "catalog/bazovyy_ofis/bazovye_platya/";
        //public readonly string BotinkiSumkiUrl = "/catalog/obuv/polubotinki/";
        public readonly string BotinkiSumkiUrl = "catalog/obuv/";
        public readonly string SaleUrl = "sale/";
        public readonly string BazJacketeUrl = "catalog/bazovyy_ofis/bazovye_zhakety_i_zhilety/";


        /*//Header//*/

        public IWebElement CartButton { get => WaitElement(By.XPath("//div[@class='small-basket__icon']")); }



        public IWebElement MainLogo { get => WaitElement(By.XPath("//img[contains(@src,'logo')]")); }
        public IWebElement PhoneNumber { get => WaitElement(By.XPath("//a[contains(@href,'tel')]")); }
        public IWebElement ProfileButton { get => WaitElement(By.XPath("//div[@class='header-bt']//a[contains(@class,'bt-1')]")); }

        //Search (пошук по сайту)
        public IWebElement GoSearchButton { get => WaitElement(By.XPath("//input[@class='search-bt']")); }
        public IWebElement SearchButton { get => WaitElement(By.XPath("//a[@class='bt-2']")); }
        public IWebElement SearchInput { get => WaitElement(By.XPath("//input[@id='title-search-input']")); }

        //NewArrivals Search
        public IWebElement NewArrivalsWishlistButton { get => WaitElement(By.XPath("//div[@class='def-like']")); }
        public IWebElement NewArrivalsImg { get => WaitElement(By.XPath("//div[@class='owl-item active']")); }



        public IWebElement FooterMenu { get => WaitElement(By.XPath("//div[@class='footer-menu']")); }


        /*//MainMenu//*/


        public IWebElement CatalogButton { get => WaitElement(By.XPath(XML.GetLocator("CatalogButton"))); }
        public IWebElement NovinkiButton { get => WaitElement(By.XPath(XML.GetLocator("NovinkiButton"))); }
        public IWebElement BazovyyOfisButton { get => WaitElement(By.XPath(XML.GetLocator("BazovyyOfisButton"))); }
        public IWebElement ObuvISumkiButton { get => WaitElement(By.XPath(XML.GetLocator("ObuvISumkiButton"))); }
        public IWebElement ObuvButton { get => WaitElement(By.XPath(XML.GetLocator("ObuvButton"))); }


        public IWebElement NashyTrendyButton { get => WaitElement(By.XPath(XML.GetLocator("NashyTrendyButton"))); }
        //public IWebElement NashyTrendyButton { get => WaitElement(By.XPath("//ul//a[@href='/nashy_trendy/']")); } //- dev version
        //public IWebElement SaleButton { get => WaitElement(By.XPath("//ul//a[contains(@href,'/sale/') and contains(text(),'Sale')]")); }
        public IWebElement SaleButton { get => WaitElement(By.XPath(XML.GetLocator("SaleButton"))); }

        public IWebElement OurShopsButton { get => WaitElement(By.XPath(XML.GetLocator("OurShopsButton"))); }
        public IWebElement BlogButton { get => WaitElement(By.XPath("//ul//a[@href='/blog/']")); }


        /*//Sub-menu// */

        //Catalog
        public IWebElement AksessuaryButton { get => WaitElement(By.XPath(XML.GetLocator("AksessuaryButton"))); }
        public IWebElement BluzyITunikiButton { get => WaitElement(By.XPath(XML.GetLocator("BluzyITunikiButton"))); }
        public IWebElement GolfySvitshotyButton { get => WaitElement(By.XPath(XML.GetLocator("GolfySvitshotyButton"))); }
        public IWebElement GolfyButton { get => WaitElement(By.XPath(XML.GetLocator("GolfyButton"))); }
        public IWebElement KostyumyButton { get => WaitElement(By.XPath(XML.GetLocator("KostyumyButton"))); }
        public IWebElement PlatyaButton { get => WaitElement(By.XPath(XML.GetLocator("PlatyaButton"))); }
        public IWebElement TopyButton { get => WaitElement(By.XPath(XML.GetLocator("TopyButton"))); }
        public IWebElement TrikotazhButton { get => WaitElement(By.XPath(XML.GetLocator("TrikotazhButton"))); }
        public IWebElement VerkhnyayaOdezhdaButton { get => WaitElement(By.XPath(XML.GetLocator("VerkhnyayaOdezhdaButton"))); }
        public IWebElement ZhaketyIZhiletyButton { get => WaitElement(By.XPath(XML.GetLocator("ZhaketyIZhiletyButton"))); }
        public IWebElement YubkiButton { get => WaitElement(By.XPath(XML.GetLocator("YubkiButton"))); }

        //BazovyiOffis

        public IWebElement BazovyeBluzyTopyButton { get => WaitElement(By.XPath(XML.GetLocator("BazovyeBluzyTopyButton"))); }
        public IWebElement BazovyeBryukiButton { get => WaitElement(By.XPath(XML.GetLocator("BazovyeBryukiButton"))); }
        public IWebElement BazovyePlatyaButton { get => WaitElement(By.XPath(XML.GetLocator("BazovyePlatyaButton"))); }
        public IWebElement BazovyeYubkiButton { get => WaitElement(By.XPath(XML.GetLocator("BazovyeYubkiButton"))); }

        //ObuvISumki
        public IWebElement PolubotinkiButton { get => WaitElement(By.XPath(XML.GetLocator("PolubotinkiButton"))); }
        public IWebElement TufliButton { get => WaitElement(By.XPath(XML.GetLocator("TufliButton"))); }
        public IWebElement SumkiButton { get => WaitElement(By.XPath(XML.GetLocator("SumkiButton"))); }
        public IWebElement BotinkiButton { get => WaitElement(By.XPath(XML.GetLocator("BotinkiButton"))); }

        //Sale
        public IWebElement DaysAyear365SaleButton { get => WaitElement(By.XPath(XML.GetLocator("DaysAyear365SaleButton"))); }
        public IWebElement KostyumySaleButton { get => WaitElement(By.XPath(XML.GetLocator("KostyumySaleButton"))); }
        public IWebElement ObuvSaleButton { get => WaitElement(By.XPath(XML.GetLocator("ObuvSaleButton"))); }


        public IWebElement OutletSaleButton { get => WaitElement(By.XPath(XML.GetLocator("OutletSaleButton"))); }
        public IWebElement PlatyaSaleButton { get => WaitElement(By.XPath(XML.GetLocator("PlatyaSaleButton"))); }
        public IWebElement TrikotazhSaleButton { get => WaitElement(By.XPath(XML.GetLocator("TrikotazhSaleButton"))); }
        public IWebElement YubkiSaleButton { get => WaitElement(By.XPath(XML.GetLocator("YubkiSaleButton"))); }
        public IWebElement ZhaketyZhiletySaleButton { get => WaitElement(By.XPath(XML.GetLocator("ZhaketyZhiletySaleButton"))); }

        //Nashi trend
        public IWebElement BlackWhiteOfficeButton { get => WaitElement(By.XPath(XML.GetLocator("BlackWhiteOfficeButton"))); }
        public IWebElement GidPoStilyuButton { get => WaitElement(By.XPath(XML.GetLocator("GidPoStilyuButton"))); }
        public IWebElement KapsulnyyGarderobButton { get => WaitElement(By.XPath(XML.GetLocator("KapsulnyyGarderobButton"))); }
        public IWebElement NashiTkaniButton { get => WaitElement(By.XPath(XML.GetLocator("NashiTkaniButton"))); }
        public IWebElement Vesna21Button { get => WaitElement(By.XPath(XML.GetLocator("Vesna21Button"))); }
        public IWebElement StayWarmButton { get => WaitElement(By.XPath(XML.GetLocator("StayWarmButton"))); }
        public IWebElement ThreeHundredDayPerYear { get => WaitElement(By.XPath(XML.GetLocator("ThreeHundredDayPerYear"))); }
        public IWebElement OfficeСollection { get => WaitElement(By.XPath(XML.GetLocator("OfficeСollection"))); }


        /*//Banners//*/

        public IWebElement FirstBigBannerButton { get => WaitElement(By.XPath("//div[contains(@class,'top-banner')]//*[text()='За новинками' || text()='перейти']")); }
        public IWebElement FirstItemBanner { get => WaitElement(By.XPath("//div[@class='owl-item active']")); }


        /*//CartPopup//*/

        public IWebElement PriceFirstItemMiniCart { get => WaitElement(By.XPath("(//div[@class='product-card-mini__bottom']//span[@class='def-price'])[1]")); }
        public IWebElement PriceSecondItemMiniCart { get => WaitElement(By.XPath("(//div[@class='product-card-mini__bottom']//span[@class='def-price'])[2]")); }
        public IWebElement GeneralPriceMiniCart { get => WaitElement(By.XPath("//div[@class='small-basket-inner__prices']//span[@class='def-price']")); }

        public IWebElement CartCloseButton { get => WaitElement(By.XPath("//span[@class='small-basket-inner__close']")); }

        public IWebElement TitleOfItemInMiniCart { get => WaitElement(By.XPath("//a[@class='product-card-mini__name'][last()]")); }

        public IWebElement ItemInCartPage { get => WaitElement(By.XPath("(//a[@class='basket-item-image-link'])[last()]")); }





        public IWebElement DeleteItemFromPopupCartButton { get => WaitElement(By.XPath("//span[@class='product-card-mini__delete']")); }
        public IWebElement FirstElementInCartPopup { get => WaitElement(By.XPath("//div[@class='product-card-mini']//img")); }
        public IWebElement GoToCartButton { get => WaitElement(By.XPath("//a[@class='def-button']")); }
        public IWebElement EmptyMiniCart { get => WaitElement(By.XPath("//div[@class='small-basket-inner'][count(div)<3]")); }


        public IWebElement OpenedCartPopUp { get => WaitElement(By.XPath("//div[@class='small-basket-inner']")); }
        public IWebElement CartAmount { get => WaitElement(By.XPath("//div[@id='basket_small_amount']/span")); }

        public By CartAmountSelector = By.XPath("//span[@class='small-basket__counter']");
        public By CartEmptySelector = By.XPath("//div[@class='small-basket__icon' and not(descendant::span)]");
        public List<IWebElement> CartAmountEmpty { get => MyWaitElements(By.XPath("//div[@class='small-basket__icon' and not(descendant::span)]")); }

        public bool GetQuantityCartPopup(string quantityMustBe)
        {
            try
            {
                return GF.CheckCorrectText(CartAmountSelector, quantityMustBe);
            }
            catch (NoSuchElementException)
            {
                IWebElement EmptyCart = Driver.FindElement(CartEmptySelector);
                return !EmptyCart.Displayed;
            }
        }
    }
}
