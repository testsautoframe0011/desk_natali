﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework.General;

namespace TestFramework.Pages
{
    public class PageProfile : PageBase
    {
        public PageProfile() { Url = "personal/"; }

        //Menu
        public IWebElement ProfileMenu { get => WaitElement(By.CssSelector("div[id='personal_menu']")); }
        public IWebElement ProfileMenuNoWait { get => Driver.FindElement(By.CssSelector("div[id='personal_menu']")); }

        public IWebElement CartProfileButton { get => WaitElement(By.CssSelector("li[data-id='1']")); }
        public IWebElement LogOutButton { get => WaitElement(By.CssSelector("a[id='auth_logout']")); }
        public IWebElement PersonalDataButton { get => WaitElement(By.CssSelector("li[data-id='2']")); }
        public IWebElement WishListButton { get => WaitElement(By.CssSelector("li[data-id='4']")); }

        //WishListProfile
        public IWebElement DeleteFromWishListButton { get => WaitElement(By.XPath("//div[@class='del-fav']")); }
        public IList<IWebElement> ItemsInWishList { get => Driver.FindElements(By.XPath("//div[@class='my-fav']//div[contains(@class,'one-cat-thum')]")); }
        public IWebElement FirstItemInWishList { get => WaitElement(By.XPath("//div[@class='my-fav']//div[@class='one-news'][1]/div[contains(@class,'one-cat-thum')]")); }
        public IWebElement TitleOfFirstItemWishList { get => WaitElement(By.XPath("//div[@class='my-fav']//div[@class='one-cat-tit']/a")); }
        public IWebElement EmptyWishListText { get => WaitElement(By.XPath("//div[@class='my-fav'][contains(text(),'Список желаний пуст')]")); }

        //CartProfile
        public IWebElement CartPrifileItem { get => WaitElement(By.XPath("//div[@class='basket-row']")); }
        public IWebElement CartPrifileTitleZero { get => WaitElement(By.XPath("//div[@class='basket-all-info']")); }
        public By ProfileMessageOfEmptyCart = By.XPath("//div[@class='basket-all-info']");
        public IWebElement DeleteFromCartProfileButton { get => WaitElement(By.XPath("//div[@class='basket-row']//div[@class='del-from']//a")); }
        public IWebElement MakeOrderButtonCartPageProfile { get => WaitElement(By.XPath("//input[@class='border-black']")); }
        public IWebElement TitleOfItemInCartPageProfile { get => WaitElement(By.XPath("(//div[@class='basket-item-tit'])[last()]/a[last()]")); }

        //PersonalInformation
        public IWebElement LastNameInput { get => WaitElement(By.XPath("//input[@name='PROFILE-LAST_NAME']")); }
        public IWebElement NameInput { get => WaitElement(By.XPath("//input[@name='PROFILE-NAME']")); }
        public IWebElement SavePersonalInfo { get => WaitElement(By.XPath("//button[@id='personal_save']")); }
        public IWebElement SuccessMessage { get => WaitElement(By.XPath("//span[@id='msg']")); }
        public IWebElement ProfileEmail { get => WaitElement(By.XPath("//input[@name='PROFILE-EMAIL']")); }

        //Незарегистрированный пользователь
        public IWebElement UnregistredUserMessage { get => WaitElement(By.XPath("//div[@class='lk-page']//p")); }


        public void DeleteAllItemsFromWishListAuth(string description)
        {
            if (!Driver.Url.Contains(URLBuilder.BaseUrl + Url))
            {
                Driver.Navigate().GoToUrl(URLBuilder.BaseUrl + Url);
            }

            WishListButton.Click();
            GF.CheckAction(WishListButton.GetAttribute("class") == "active");
            //Thread.Sleep(1000);

            if (ItemsInWishList != null)
            {
                foreach(var n in ItemsInWishList)
                {
                    DeleteFromWishListButton.Click();
                    Thread.Sleep(500);
                    Driver.Navigate().Refresh();
                }
            }
        }
    }
}
